#!/bin/bash
echo "<INICIO>"
for i in {1..2}
do
   export SYMFONY__DATABASE__NAME=sitio$i
   export SYMFONY__DIRECTORIO__NAME=sitio$i
   echo "------------------------------"
   echo "          | sitio$i |          "
   echo "------------------------------"
   echo ">>> dump-sql"
   php app/console doctrine:schema:update --dump-sql
   echo ">>> force"
   php app/console doctrine:schema:update --force
done
echo "<FIN>"
