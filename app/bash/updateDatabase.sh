#!/bin/bash
echo "<INICIO>"
for i in {1..2}
do
   export SYMFONY__DATABASE__NAME=sitio$i
   export SYMFONY__DIRECTORIO__NAME=sitio$i
   echo "------------------------------"
   echo "          | sitio$i |          "
   echo "------------------------------"
   echo ">>> dump-sql"
   php app/console doctrine:schema:update --dump-sql
   echo ">>> force"
   php app/console doctrine:schema:update --force
done
#ace
  export SYMFONY__DATABASE__NAME=ty000319_ace
  export SYMFONY__DIRECTORIO__NAME=ace
   echo "------------------------------"
   echo "           | ace |            "
   echo "------------------------------"
   echo ">>> dump-sql"
   php app/console doctrine:schema:update --dump-sql
   echo ">>> force"
   php app/console doctrine:schema:update --force
#cpce
  export SYMFONY__DATABASE__NAME=ty000319_cpceweb
  export SYMFONY__DIRECTORIO__NAME=cpceweb
   echo "------------------------------"
   echo "          | cpceweb |          "
   echo "------------------------------"
   echo ">>> dump-sql"
   php app/console doctrine:schema:update --dump-sql
   echo ">>> force"
   php app/console doctrine:schema:update --force
echo "<FIN>"