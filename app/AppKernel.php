<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel {

    public function registerBundles() {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            // TERCEROS
            new MWSimple\Bundle\CrudGeneratorBundle\MWSimpleCrudGeneratorBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new ADesigns\CalendarBundle\ADesignsCalendarBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new FOS\RestBundle\FOSRestBundle(),
            // new FOS\CommentBundle\FOSCommentBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle($this),
            new MWSimple\Bundle\AdminCrudBundle\MWSimpleAdminCrudBundle(),
            new SC\DatetimepickerBundle\SCDatetimepickerBundle(),
            new Oneup\UploaderBundle\OneupUploaderBundle(),
            new Tecspro\Bundle\ApiPostBundle\TecsproApiPostBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),
            // PROPIOS
            new CmsGa\BackBundle\CmsGaBackBundle(),
            new CmsGa\toolsBundle\CmsGatoolsBundle(),
            new CmsGa\CPCEBundle\CmsGaCPCEBundle(),
            new MWSimple\DynamicMenuBundle\MWSimpleDynamicMenuBundle(),
            new CmsGa\FrontBundle\CmsGaFrontBundle(),
            new MWSimple\Bundle\CountVisitBundle\MWSimpleCountVisitBundle(),
            new CmsGa\CalendarioBundle\CmsGaCalendarioBundle(),
            new CmsGa\RepositoryFilesBundle\MWSRFBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            // $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader) {
        $loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
    }

    public function getCacheDir() {
        $envParameters = $this->getEnvParameters();
        if (array_key_exists('cache', $envParameters)) {
            return $this->rootDir . '/cache/' . $envParameters['cache'] . '/' . $this->environment;
        }
        return $this->rootDir . '/cache/' . $this->environment;
    }

}
