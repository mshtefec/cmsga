<?php

namespace CmsGa\toolsBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class EntityToIdTransformer.
 *
 * @author Björn Fromme <mail@bjo3rn.com>
 */
class EntityToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var string
     */
    protected $class;

    /**
     * Constract.
     *
     * @param ObjectManager $objectManager Object Manager
     * @param mixed         $class         Class
     */
    public function __construct(ObjectManager $objectManager, $class)
    {
        $this->objectManager = $objectManager;
        $this->class = $class;
    }

    /**
     * Transform.
     *
     * @param mixed $entity Entity
     *
     * @return mixed
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return;
        }

        return $entity->getId();
    }

    /**
     * Reverse Transform.
     *
     * @param mixed $id Identifier
     *
     * @return mixed|null|object
     *
     * @throws \Symfony\Component\Form\Exception\TransformationFailedException
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return;
        }

       // die(var_dump('id '.$id));
        $entity = $this->objectManager
            ->getRepository($this->class)
            ->find($id);

        //die(var_dump($entity));
        if (null === $entity) {
            throw new TransformationFailedException();
        }

        return $entity;
    }
}
