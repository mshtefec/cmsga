<?php

namespace CmsGa\toolsBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Entity to JSON.
 *
 * @author  Gonzalo Alonso < gonzaloalonsod@gmail.com>
 */
class EntityToJsonTransformer implements DataTransformerInterface
{
    /**
     * Class para conectarse.
     */
    private $class;

    /**
     * ObjectManager.
     */
    private $om;

    /***
     * Constructor
     */
    public function __construct($dataConnect)
    {
        $this->class = $dataConnect['class'];
        $this->om = $dataConnect['om'];
        $this->multiple = $dataConnect['multiple'];
    }

    /**
     * {@inheritdoc}
     */
    public function transform($entities)
    {
        if (!$entities) {
            return;
        };
        if ($this->multiple) {
            $jsonResponse = array();
            $jsonResponse = $entities->map(function ($entity) {
                return array(
                    'id' => $entity->getId(),
                    'text' => $entity->__toString(),
                );
              })->toArray();

            return json_encode($jsonResponse);
        } else {
            return json_encode(array(
                'id' => $entities->getId(),
                'text' => $entities->__toString(),
                )
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($json)
    {
        if (!$json) {
            return;
        };

        $om = $this->om;
        $class = $this->class;
        if ($this->multiple) {
            $entitiesResponse = new ArrayCollection();
            if (!$json) {
                return $entitiesResponse;
            }
            $jEntities = json_decode($json, true);
            foreach ($jEntities as $j) {
                $entity = $om
                    ->getRepository($class)
                    ->findOneBy(array('id' => $j['id']));
                if (!$entitiesResponse->contains($entity)) {
                    $entitiesResponse->add($entity);
                }
            }

            return $entitiesResponse;
        } else {
            $jEntity = json_decode($json, true);
            $entity = (array_key_exists('id', $jEntity) ? $jEntity['id'] : null);
            $entityResponse = $om
                ->getRepository($class)
                ->findOneBy(array('id' => $entity));

            return $entityResponse;
        }
    }
}
