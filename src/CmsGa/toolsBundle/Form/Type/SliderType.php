<?php

namespace CmsGa\toolsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Slide Type.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class SliderType extends AbstractType
{
    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
            'choices' => array(
                'm' => 'Male',
                'f' => 'Female',
            )
        ));
    }*/

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'slider';
    }
}
