<?php

namespace CmsGa\toolsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use CmsGa\toolsBundle\Form\DataTransformer\BootstrapDateTimeTransformer;

class BootstrapDateTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new BootstrapDateTimeTransformer($options['widget_type']);
        $builder->addViewTransformer($transformer, true);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['options'] = $this->createDisplayOptions($options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'widget' => 'single_text',
            'format' => 'yyyy-MM-dd HH:mm',
            'widget_type' => 'date',
            'language' => 'es',
            'minute_step' => 5,
            'start_view' => 4,
            'today_highlight' => true,
            'days_of_week_disabled' => array(),
            'start_date' => '1900-01-01',
            'end_date' => '2020-01-01',
            'disabled_days' => array(),

        ));

        $resolver->setAllowedValues(
            array(
                'widget' => array('single_text'),
                'widget_type' => array('both', 'date', 'time', 'month'),
                'start_view' => array(1, 2, 3, 4),
            )
        );
    }

    public function getParent()
    {
        return 'datetime';
    }

    public function getName()
    {
        return 'bootstrapdatetime';
    }

    private function createDisplayOptions($options = array())
    {
        $displayOptions = array();
        $displayOptions['format'] = 'dd/mm/yyyy hh:ii';
        $displayOptions['autoclose'] = true;
        $displayOptions['startView'] = $options['start_view'];

        if (array_key_exists('days_of_week_disabled', $options)) {
            $displayOptions['daysOfWeekDisabled'] = $options['days_of_week_disabled'];
        }

        if (array_key_exists('disabled_days', $options)) {
            $displayOptions['disabledDays'] = $options['disabled_days'];
        }

        if (array_key_exists('start_date', $options)) {
            $displayOptions['startDate'] = $options['start_date'];
        }

        if (array_key_exists('today_highlight', $options)) {
            $displayOptions['todayHighlight'] = $options['today_highlight'];
        }

        if (array_key_exists('end_date', $options)) {
            $displayOptions['endDate'] = $options['end_date'];
        }

        if (array_key_exists('language', $options)) {
            $displayOptions['language'] = $options['language'];
        }

        if (array_key_exists('minute_step', $options)) {
            $displayOptions['minuteStep'] = (integer) $options['minute_step'];
        }

        if (array_key_exists('widget_type', $options)) {
            switch ($options['widget_type']) {
                case 'date':
                    $displayOptions['format'] = 'dd/mm/yyyy';
                    $displayOptions['minView'] = 2;
                    break;
                case 'month':
                    $displayOptions['format'] = 'mm/yyyy';
                    $displayOptions['minView'] = 2;
                    $displayOptions['minView'] = 3;
                    break;
                case 'time':
                    $displayOptions['format'] = 'hh:ii';
                    $displayOptions['startView'] = 1;
                    $displayOptions['maxView'] = 1;
                    break;
            }
        }

        return json_encode($displayOptions);
    }
}
