-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 25-08-2015 a las 13:16:27
-- Versión del servidor: 5.5.44-0+deb8u1
-- Versión de PHP: 5.6.9-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cpceweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_classes`
--

CREATE TABLE IF NOT EXISTS `acl_classes` (
`id` int(10) unsigned NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_entries`
--

CREATE TABLE IF NOT EXISTS `acl_entries` (
`id` int(10) unsigned NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_object_identities`
--

CREATE TABLE IF NOT EXISTS `acl_object_identities` (
`id` int(10) unsigned NOT NULL,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_object_identity_ancestors`
--

CREATE TABLE IF NOT EXISTS `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acl_security_identities`
--

CREATE TABLE IF NOT EXISTS `acl_security_identities` (
`id` int(10) unsigned NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Actividad`
--

CREATE TABLE IF NOT EXISTS `Actividad` (
  `id` int(11) NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Archivo`
--

CREATE TABLE IF NOT EXISTS `Archivo` (
`id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_upload_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Archivo`
--

INSERT INTO `Archivo` (`id`, `categoria_id`, `descripcion`, `file_path`, `file_upload_dir`) VALUES
(27, 3, 'MODELO CERTIFICACIÓN DE INFORMACIÓN PARA N.B.DEL CHACO S.A', 'ce920031f97537970f977dff424f3bb572359eab.doc', 'cpceweb'),
(28, 3, 'CERTIFICACIÓN CONTABLE DE SALDO DE CUENTA POR COBRAR', '3779de045debd4a3dfaa5a3b0043edaeeac627ed.docx', 'cpceweb'),
(29, 3, 'Estructura_de_un_informe_especial_de_acuerdo_con_el_Capitulo_VIIC_de_la_RT 37', 'c644412d1b4e06a64ec9a94c296c9c2ff32931eb.doc', 'cpceweb'),
(30, 3, 'CERTIFICACIÓN DE INGRESOS', '9198a21ed09019ed0c7eba822cd13902a7740521.docx', 'cpceweb'),
(31, 3, 'CERTIFICACIÓN DE INGRESOS CON DECLARACION JURADA DEL COMITENTE', '0bfc5487afd98fb5bcbf3119bf0e5e86115828aa.docx', 'cpceweb'),
(32, 3, 'Modelo_Certificacion_Manifestacion_Bienes_y_Deudas_segun_RTN37', 'ed4b3d64c9c17c2a14452f547cd067a3840dbf77.doc', 'cpceweb'),
(33, 3, 'Modelo_Certificacion_origen_fondos_para_compra_de_automotor_UIF_segun_RTN37', 'cd40a6ac89cb3d63573b9c2f48270db99a737adc.doc', 'cpceweb'),
(34, 3, 'Modelos_Informes_RT37_Auditoria', '15204ee567195dd47c36ccc5f50d6e28534b979c.doc', 'cpceweb'),
(35, 3, 'Modelos_Informes_RT37_Diversos', '297883541e8ad2ab872bb4fdba3aecf01d531a95.doc', 'cpceweb'),
(36, 3, 'CERTIFICACIÓN PARA GARANTIZAR SGR', '29c2628eca0d4d2c4913b4ccb95c3359c125a53c.docx', 'cpceweb'),
(37, 3, 'CERTIFICACIÓN CONTABLE. Formularios del REGISTRO DE CONSTRUCTORES', '78efce82729ffab515712511375636c16c89a5a4.doc', 'cpceweb'),
(38, 3, 'INF. ESPECIAL PARA SECRET. DE TPTE. DE LA NACIÓN', '9b47c77954c3601a273a4b84ac6a8edffb04eb67.docx', 'cpceweb'),
(39, 3, 'CERTIFICACIÓN DE INGRESOS PROYECTADOS I', '3ab0c90f6887b2096943a5da76f83cf3d7f642d1.docx', 'cpceweb'),
(40, 3, 'Informe Laboral según RT 3707-2015 SECRET. TPTE. DE LA NACIÓN', '4357bf7aed505aa65239b19fbc8315601991dfde.docx', 'cpceweb'),
(41, 3, 'CERTIF DE ORIGEN Y LICITUD DE FONDOS SIN DECLARACION JURADA', '2b561cb6cd33321f7fa3e61322cb2caee4fbbf96.doc', 'cpceweb'),
(48, 3, 'CNRT INFORME DE ASEGURAMIENTO SOBRE CUMPLIMIENTO DE PN MINIMO RT37', 'a3f0a830cba934eb9ace6c52b68780a062216aab.doc', 'cpceweb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Bloque`
--

CREATE TABLE IF NOT EXISTS `Bloque` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Calendario`
--

CREATE TABLE IF NOT EXISTS `Calendario` (
`id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `bg_color` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fg_color` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `css_class` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `all_day` tinyint(1) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `discr` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Categoria`
--

CREATE TABLE IF NOT EXISTS `Categoria` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Categoria`
--

INSERT INTO `Categoria` (`id`, `nombre`) VALUES
(3, 'cpce'),
(4, 'Normas de Auditoría rt 37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Comment`
--

CREATE TABLE IF NOT EXISTS `Comment` (
`id` int(11) NOT NULL,
  `thread_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ancestors` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `depth` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Configuracion`
--

CREATE TABLE IF NOT EXISTS `Configuracion` (
`id` int(11) NOT NULL,
  `clave` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Configuracion`
--

INSERT INTO `Configuracion` (`id`, `clave`, `valor`) VALUES
(1, 'PermisoNoticia', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
`id` int(11) NOT NULL,
  `nombre_apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Curso`
--

CREATE TABLE IF NOT EXISTS `Curso` (
  `id` int(11) NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `evento` (
  `id` int(11) NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Fecha`
--

CREATE TABLE IF NOT EXISTS `Fecha` (
`id` int(11) NOT NULL,
  `calendario_id` int(11) NOT NULL,
  `dia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
`id` int(11) NOT NULL,
  `seccion_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fos_user`
--

INSERT INTO `fos_user` (`id`, `seccion_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(1, NULL, 'admin', 'admin', 'admin@gmail.com', 'admin@gmail.com', 1, 'qz7qpwthpjk88cg80cscscos880kgso', '68D5iPO8Nwhy5Za1dS9CTUo4/rObcCjtwVbsnsvhoq14PeBeWTKh5nHCePAUpelvsH4YperKhUHQuxA8ogrkdQ==', '2015-08-25 08:16:03', 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:15:"ROLE_SUPER_ADMIN";i:1;s:10:"ROLE_ADMIN";}', 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

CREATE TABLE IF NOT EXISTS `imagen` (
`id` int(11) NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discriminador` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_upload_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `imagen`
--

INSERT INTO `imagen` (`id`, `file_path`, `discriminador`, `file_upload_dir`) VALUES
(10, '1b225c7558c24e5c13b8bcad58847cd4d93165d3.jpeg', 'imagen_noticia', 'cpceweb'),
(11, '0fd6536f35e6d5fe5c4a837c7d521e15eaf82eca.jpeg', 'imagen_noticia', 'cpceweb'),
(12, '795c83afefea0680185f38f7b3c66e505ecd26b4.png', 'imagen_noticia', 'cpceweb'),
(13, 'c8fbcab3b3a643729b4d97db0332115c80cbdb73.jpeg', 'imagen_noticia', 'cpceweb'),
(14, '4f854bda25d4f176a7d6c35b3ba7e1b8b1c03a3e.jpeg', 'imagen_noticia', 'cpceweb'),
(15, '61821dca3dc874c1efa8e8caa644f0cd8b00627f.png', 'imagen_noticia', 'cpceweb'),
(16, '0ad87c19ab4510f94d7c8cecd82c11fdc700f492.jpeg', 'imagen_noticia', 'cpceweb'),
(17, '74ee28455417073069824b3fb4c3a8f30b6aa37c.jpeg', 'imagen_noticia', 'cpceweb'),
(18, '79d073ea2770bbee11aef20d59d45c4d1b686c88.jpeg', 'imagen_noticia', 'cpceweb'),
(19, '5a364d14c8b42e2dcaa21412aa93be09503bebf0.jpeg', 'imagen_noticia', 'cpceweb'),
(20, '50ae30b7db50b3c8a2d9daf1069d2f6a27ca55d8.png', 'imagen_noticia', 'cpceweb'),
(21, '77d4301adfb9a35524c113b5c9207656a96d5590.png', 'imagen_noticia', 'cpceweb'),
(22, '22b66464f4d9d919679e55cc3ede4b46144b021e.jpeg', 'imagen_noticia', 'cpceweb'),
(23, '47abd5428d5e69c02fb91e6644c9d5aee04c2124.jpeg', 'imagen_noticia', 'cpceweb'),
(24, 'c0663aaf57dbbbaf9b913469dd987dd98f30e542.jpeg', 'imagen_noticia', 'cpceweb'),
(25, '2e48803c1b8621d2d8ed309b482b54158393a67d.jpeg', 'imagen_noticia', 'cpceweb'),
(26, '275fc87b1b7c259e7bdc56971c687cbcdf111a39.jpeg', 'imagen_noticia', 'cpceweb'),
(27, 'f6beb62e48bd0f9aa2f80bc5dcc9d8f32569cc8c.jpeg', 'imagen_noticia', 'cpceweb'),
(28, 'cc0c09eea48f50be6b61240148b2b30e6eedd92e.png', 'imagen_noticia', 'cpceweb'),
(29, '79fb09ae7b9574f4993a65115560ba21643559d9.png', 'imagen_noticia', 'cpceweb'),
(30, 'c9b0c26c92818379e6aeacc87f78793a28f67ed5.jpeg', 'imagen_noticia', 'cpceweb'),
(31, '249b762da658c0e85598f231449923e24e95ab7c.png', 'imagen_noticia', 'cpceweb'),
(32, 'cfc3d2912d8dc6a84188954c671731fe9e2e75a2.png', 'imagen_noticia', 'cpceweb'),
(33, '1414b06be506ab0098b03c6ca46a2e81d33581b3.jpeg', 'imagen_noticia', 'cpceweb'),
(34, 'd4f3a09c9c7e9478b0e1bdc885932a980115a139.jpeg', 'imagen_noticia', 'cpceweb'),
(35, '919cfc081c5322ce5484ad2448baee6c328ae825.jpeg', 'imagen_noticia', 'cpceweb'),
(36, 'b12a86bf2334e03c56d35d81f3f45483cbc9eea6.jpeg', 'imagen_noticia', 'cpceweb'),
(37, 'a9c493903a13350048a7801b66cf0163ad07d4a7.jpeg', 'imagen_noticia', 'cpceweb'),
(38, '6efcccdda3f9010b8edcf4ef04a94b25edbbae40.jpeg', 'imagen_noticia', 'cpceweb'),
(39, '27ab649b8765284d757f7c12e0758ddd23b7311c.jpeg', 'imagen_noticia', 'cpceweb'),
(45, '5d4e6adb64ce469e25bd06942b40c85f3beaa28d.jpeg', 'imagen_seccion', 'cpceweb'),
(46, '7b5936f8f60c5bf79590e4dddeb7eee6dd93fa82.png', 'imagen_seccion', 'cpceweb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_evento`
--

CREATE TABLE IF NOT EXISTS `imagen_evento` (
  `id` int(11) NOT NULL,
  `evento_id` int(11) DEFAULT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci,
  `actividad_id` int(11) DEFAULT NULL,
  `curso_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_noticia`
--

CREATE TABLE IF NOT EXISTS `imagen_noticia` (
  `id` int(11) NOT NULL,
  `noticia_id` int(11) DEFAULT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `imagen_noticia`
--

INSERT INTO `imagen_noticia` (`id`, `noticia_id`, `descripcion`) VALUES
(10, 54, NULL),
(11, 55, NULL),
(12, 56, NULL),
(13, 57, NULL),
(14, 58, NULL),
(15, 59, NULL),
(16, 60, NULL),
(17, 61, NULL),
(18, 37, NULL),
(19, 40, NULL),
(20, 39, NULL),
(21, 41, NULL),
(22, 43, NULL),
(23, 42, NULL),
(24, 44, NULL),
(25, 45, NULL),
(26, 46, NULL),
(27, 47, NULL),
(28, 48, NULL),
(29, 49, NULL),
(30, 15, NULL),
(31, 17, NULL),
(32, 18, NULL),
(33, 19, NULL),
(34, 14, NULL),
(35, 20, NULL),
(36, 12, NULL),
(37, 13, NULL),
(38, 16, NULL),
(39, 38, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_portada`
--

CREATE TABLE IF NOT EXISTS `imagen_portada` (
`id` int(11) NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci,
  `file_upload_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen_seccion`
--

CREATE TABLE IF NOT EXISTS `imagen_seccion` (
  `id` int(11) NOT NULL,
  `seccion_id` int(11) DEFAULT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `imagen_seccion`
--

INSERT INTO `imagen_seccion` (`id`, `seccion_id`, `descripcion`) VALUES
(45, 45, 'Pyme Jornadas Nacionales 2015'),
(46, 46, 'Sistema web');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Item`
--

CREATE TABLE IF NOT EXISTS `Item` (
`id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Item`
--

INSERT INTO `Item` (`id`, `menu_id`, `parent_id`, `name`, `name_slug`, `title`, `level`, `orden`) VALUES
(1, 1, 20, 'cpce', 'cpce', 'Noticias', 2, 1),
(2, 1, 24, 'SIPRES', 'sipres', 'Noticias', 2, 1),
(3, 1, NULL, 'SECRETARIA TÉCNICA', 'secretaria-t-cnica', 'SEC. TÉCNICA', 1, 3),
(4, 1, NULL, 'C.M.C.A', 'c-m-c-a', 'C.M.C.A', 1, 4),
(5, 1, NULL, 'Comisiones', 'comisiones', 'Comisiones', 1, 5),
(7, 1, 5, 'Jovenes Profesionales', 'jovenes-profesionales', 'Jovenes Profesionales', 2, 1),
(8, 1, 20, 'Autoridades', 'autoridades', 'Autoridades', 2, 1),
(10, 1, 24, 'beneficios Sipress', 'beneficios-sipress', 'beneficios', 2, 2),
(11, 1, 10, 'Descuento Comercio  sipres', 'descuento-comercio-sipres', 'Descuento Comercio', 2, 1),
(12, 1, 20, 'Historia', 'historia', 'Historia', 2, 2),
(13, 1, 20, 'delegaciones cpce', 'delegaciones-cpce', 'Delegaciones', 2, 3),
(14, 1, 20, 'tramites cpce', 'tramites-cpce', 'Tramites', 2, 4),
(15, 1, 14, 'TRÁMITE DE MATRICULACIÓN', 'tr-mite-de-matriculaci-n', 'trámite de matriculación', 3, 1),
(20, 1, NULL, 'cpce', 'cpce', 'cpce', 1, 0),
(24, 1, NULL, 'SIPRES', 'sipres', 'SIPRES', 1, 2),
(25, 1, 24, 'historia sipres', 'historia-sipres', 'Historia', 2, 3),
(26, 1, 24, 'Aurtoridades sipres', 'aurtoridades-sipres', 'Aurtoridades', 2, 4),
(27, 1, 10, 'descuentos sipres Hoteleria', 'descuentos-sipres-hoteleria', 'Descuentos Hoteleria', 3, 2),
(28, 1, 10, 'beneficios sipres salud', 'beneficios-sipres-salud', 'Servicios de salud', 3, 3),
(30, 1, 10, 'beneficios sipres Cuota ayuda personal', 'beneficios-sipres-cuota-ayuda-personal', 'Cuota ayuda personal', 3, 1),
(31, 1, 10, 'beneficios sipres  Ayuda Personales', 'beneficios-sipres-ayuda-personales', 'Ayuda Personales', 3, 5),
(32, 1, 3, 'NORMAS DE AUDITORÍA RT 37', 'normas-de-auditor-a-rt-37', 'NORMAS DE AUDITORÍA RT 37', 2, 1),
(33, 1, 3, 'NORMAS DE AUDITORÍA RT 7 (DEROGADA)', 'normas-de-auditor-a-rt-7-derogada', 'NORMAS DE AUDITORÍA RT 7 (DEROGADA)', 2, 2),
(34, 1, 20, 'normativas cpce', 'normativas-cpce', 'normativas', 2, 6),
(35, 1, 24, 'normativas sipres', 'normativas-sipres', 'normativas', 2, 5),
(37, 1, 4, 'normativas cmca', 'normativas-cmca', 'normativas', 2, 1),
(48, 2, NULL, 'Pyme Jornadas Nacionales 2015', 'pyme-jornadas-nacionales-2015', 'Pyme Jornadas Nacionales 2015', 1, 2),
(49, 2, NULL, 'Sistema web', 'sistema-web', 'Sistema web', 1, 1),
(50, 1, 5, 'Comisión de Capacitacion', 'comisi-n-de-capacitacion', 'Capacitación', 2, 2),
(51, 1, 5, 'Sector Público', 'sector-p-blico', 'Sector Público', 2, 3),
(52, 1, 5, 'Estudios Contables', 'estudios-contables', 'Estudios Contables', 2, 4),
(53, 1, 5, 'comision de Licenciados', 'comision-de-licenciados', 'Licenciados', 2, 5),
(54, 1, 5, 'comision de Cultura', 'comision-de-cultura', 'Cultura', 2, 6),
(55, 1, 5, 'comision de Deportes', 'comision-de-deportes', 'Deportes', 2, 7),
(56, 1, 5, 'comision de  Responsabilidad y  Balance Social', 'comision-de-responsabilidad-y-balance-social', 'Responsabilidad y  Balance Social', 2, 8),
(57, 1, 5, 'comision de Laboral y de la Seguridad Social', 'comision-de-laboral-y-de-la-seguridad-social', 'Laboral y de la Seguridad Social', 2, 9),
(58, 1, 5, 'comision de PYMES', 'comision-de-pymes', 'PYMES', 2, 10),
(59, 1, 5, 'comision de CEAT', 'comision-de-ceat', 'CEAT', 2, 11),
(60, 1, 5, 'comision de Mediación', 'comision-de-mediaci-n', 'Mediación', 2, 12),
(61, 1, 5, 'comision de  Actuación Judicial', 'comision-de-actuaci-n-judicial', 'Actuación Judicial', 2, 13),
(62, 1, 5, 'comision de Matriculación', 'comision-de-matriculaci-n', 'Matriculación', 2, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Menu`
--

CREATE TABLE IF NOT EXISTS `Menu` (
`id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Menu`
--

INSERT INTO `Menu` (`id`, `name`, `name_slug`) VALUES
(1, 'menu_principal', 'menu_principal'),
(2, 'enlaces', 'enlaces');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticia`
--

CREATE TABLE IF NOT EXISTS `noticia` (
`id` int(11) NOT NULL,
  `seccion_id` int(11) DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci,
  `publicado` tinyint(1) DEFAULT NULL,
  `portada` tinyint(1) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `imagenPortada_id` int(11) DEFAULT NULL,
  `archivo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `noticia`
--

INSERT INTO `noticia` (`id`, `seccion_id`, `titulo`, `descripcion`, `contenido`, `publicado`, `portada`, `fecha`, `updated_at`, `created_at`, `imagenPortada_id`, `archivo_id`) VALUES
(12, 11, 'Juguetería TATETI', 'Pellegrini 420', '<p>10% de descuento pago efectivo. 5% de descuento pago d&eacute;bito</p>', 1, 0, '2015-07-27 00:00:00', '2015-08-19 08:47:20', '2015-07-27 10:35:34', NULL, NULL),
(13, 11, 'Cuando Sea Grande', 'Pellegrini 138', '<p>Zapater&iacute;a para ni&ntilde;os, 15% de descuento pago efectivo</p>', 1, 0, '2015-07-27 00:00:00', '2015-08-19 08:47:42', '2015-07-27 10:36:49', NULL, NULL),
(14, 11, 'Gimnasio CROSS CENTER', 'Mitre 474', '<p>15% de descuento pago efectivo</p>', 1, 0, '2015-07-27 00:00:00', '2015-08-19 08:46:41', '2015-07-27 10:38:12', NULL, NULL),
(15, 11, 'Giro Didáctico', 'Santa Fe 300', '<p>10% de descuento pago efectivo. 5% de descuento pago d&eacute;bito</p>', 1, 0, '2015-08-10 00:00:00', '2015-08-19 08:45:06', '2015-08-10 08:14:28', NULL, NULL),
(16, 11, 'ROBACQ Living', 'Decoración & Diseño -Juan B. Justo 450', '<p>15% de descuento pago efectivo. 10% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta cr&eacute;dito</p>', 1, 0, '2015-08-10 00:00:00', '2015-08-19 08:48:01', '2015-08-10 08:18:06', NULL, NULL),
(17, 11, 'Alejandra Gubinelli', 'San MArtín 1150 Saenz Peña', '<p>Arte en vidrio, 15% de descuento pago efectivo. 10% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-10 00:00:00', '2015-08-19 08:45:31', '2015-08-10 08:26:25', NULL, NULL),
(18, 11, 'LA ROBLA', 'Viamonte 1615', '<p>Colega, cuando viaje a Capital Federal, en Viamonte 1615 el Restaurant &ldquo;LA ROBLA&rdquo; con solo la presentaci&oacute;n de su carnet del Consejo le otorga una&nbsp;bonificaci&oacute;n del 25% por pago contado.<br />&iexcl;&iexcl;BUEN PROVECHO!!</p>', 1, 0, '2015-08-10 00:00:00', '2015-08-19 08:45:56', '2015-08-10 08:27:54', NULL, NULL),
(19, 11, 'Mara', 'Arturo Illia 147', '<p>Zapater&iacute;a, 15% de descuento pago efectivo. 10% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-10 00:00:00', '2015-08-19 08:46:11', '2015-08-10 08:29:40', NULL, NULL),
(20, 11, 'Club Atlético Sarmiento', 'Juan D. Perón 1515', '<p>10% de descuento en la cuota social. Ver documento anexo de importes&nbsp;<a href="http://cpcechaco.org.ar/cpce/wp-content/uploads/2013/12/CLUB-SARMIENTO.docx">CLUB SARMIENTO</a></p><p style="font-weight: normal;">&nbsp;</p><p>&nbsp;</p>', 1, 0, '2015-08-10 00:00:00', '2015-08-19 08:46:59', '2015-08-10 09:48:55', NULL, NULL),
(37, 11, 'Prüne', 'Brown 183', '<p>15% de descuento pago efectivo. 10% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:38:21', '2015-08-11 11:20:11', NULL, NULL),
(38, 11, 'Identikids', 'Vedia y Julio A. Roca', '<p>30% de descuento pago efectivo. 15% de descuento pago d&eacute;bito. 10% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:50:21', '2015-08-11 11:21:24', NULL, NULL),
(39, 11, 'Gabardini  Casa Gabardini S.A.', 'Juan D. Perón 403', '<p>10% de descuento pago efectivo. 5% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:39:15', '2015-08-11 11:23:47', NULL, NULL),
(40, 11, 'Indy', 'Arturo Illia 164', '<p>10% de descuento pago efectivo. 5% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:38:47', '2015-08-11 11:24:55', NULL, NULL),
(41, 11, 'Indy', 'Av. Alberdi 245', '<p>15% de descuento pago efectivo. 10% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:40:30', '2015-08-11 11:26:14', NULL, NULL),
(42, 11, 'La Luminosa', 'La Ramada, Ruta Provincial N° 14 Km 175', '<p>10% Efectivo sobre las tarifas vigentes al momento de la reserva. Reserva: 20% por transferencia bancaria, 80% pago en efectivo al ingresar al complejo.</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:41:12', '2015-08-11 11:27:37', NULL, NULL),
(43, 11, 'Effe Uffici Oficinas y Neoconfort', 'Comuníquese con nuestra fábrica al Tel. 03442-428824/ e-mail:\r\neffeuffici@effeuffici.com, o con nuestro agente en Resistencia – ARQ. RICARDO DAMBORSKY, Tel. 0362-4421624 / cel. 0362-154766449 / e-mail:rodamborsky@hotmail.com', '<p>8% de descuento por compras de pago contado o con cheques diferidos, y 10% de descuento por compras de pago contado o con cheques diferidos, superiores a $40.000 sin IVA.</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:40:49', '2015-08-11 11:32:32', NULL, NULL),
(44, 11, 'La Segunda.', 'Ver la lista de productores más cercano a su domicilio aquí Agencias Chaco', '<p>Multiples beneficios con el Grupo Asegurador La Segunda. Ver el documento anexo&nbsp;<a href="http://cpcechaco.org.ar/cpce/wp-content/uploads/2013/12/CONSEJO-DE-CIENCIAS-ECONOMICAS-GRUPO-AFINIDAD.pdf">CONSEJO DE CIENCIAS ECONOMICAS &ndash; GRUPO AFINIDAD</a></p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:42:17', '2015-08-11 11:33:36', NULL, NULL),
(45, 11, 'Dormi sueño', 'Vedia 166', '<p>5% de descuento pago efectivo. 10% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:42:29', '2015-08-11 11:34:42', NULL, NULL),
(46, 11, 'Vinoteca “Las Rosas”', 'Av. Wilde 170', '<p>10% de descuento pago efectivo. 7% de descuento pago d&eacute;bito. 5% de descuento pago con tarjeta de cr&eacute;dito</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:43:28', '2015-08-11 11:35:33', NULL, NULL),
(47, 11, 'Óptica “Free Vision”', 'San Lorenzo 812 – San Lorenzo 812 – Tel. 0362 4431841 – opticafreevision@outlook.com', '<p>20% de descuento pago efectivo. 5% de descuento pago d&eacute;bito.&nbsp;De jueves a s&aacute;bados hasta 12 pagos sin inter&eacute;s con cualquier tarjeta de cr&eacute;dito. (promoci&oacute;n AHORA 12).&nbsp;Con las tarjetas del NBCH hasta 12 sin inter&eacute;s, o en 6 pagos con un 5% de reintegro; (visa &ndash; mastercad &ndash; cabal).&nbsp;Tarjeta TUYA hasta 12 sin inter&eacute;s y con un 5% de reintegro.</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:43:42', '2015-08-11 11:36:33', NULL, NULL),
(48, 11, 'S.I.S.E Argentina– Sistemas Integrales de Seguridad Electrónica', 'Av. 9 de julio 2514', '<p>15% de descuento pago en efectivo</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:44:10', '2015-08-11 11:37:35', NULL, NULL),
(49, 11, 'WeeNet– Internet donde estes', 'Av. 9 de julio 2514', '<p>15% de descuento pago en efectivo</p>', 1, 0, '2015-08-11 00:00:00', '2015-08-19 08:44:39', '2015-08-11 11:38:58', NULL, NULL),
(54, 28, 'SERVICIOS SOCIALES FACPCE', 'Fondo Solidario de Salud', '<h5 style="text-align: left;"><a href="http://www.facpce.org.ar/web2011/prestaciones.html">M&aacute;s Informaci&oacute;n</a></h5>', 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:30:49', '2015-08-14 10:10:58', NULL, NULL),
(55, 28, 'PREVENCIÓN SALUD', 'Contacto:\r\nNATALIA POGULANIK\r\nLOPEZ Y PLANES 140 1° Piso\r\nCel. 0362 154653235\r\nnatalia_pogulanik@hotmail.com', '<h5><a href="http://cpcechaco.org.ar/cpce/wp-content/uploads/2014/03/Convenio-SIPRES-Chaco.PREVENCION-SALUD.pdf">M&aacute;s Informaci&oacute;n</a></h5>', 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:33:04', '2015-08-14 10:13:24', NULL, NULL),
(56, 28, 'SWISS MEDICAL', 'PARA ASOCIADOS Y/O AFILIADOS', '<p><strong>::</strong>Swiss&nbsp;Medical Medicina Privada</p><p>Cristian Orlando Bravo</p><p>Asesor Comercial I Gerencia Comercial Interior</p><p><a href="mailto:cristianorlando.bravo@swissmedical.com.ar">cristianorlando.bravo@swissmedical.com.ar</a></p><p>Obligado 216 &ndash; Resistencia</p><p>Celular: 0362-154875674</p><p>TE. 0362-4437550 Int. 16066</p>', 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:34:26', '2015-08-14 10:14:53', NULL, NULL),
(57, 28, 'FEMECHACO', 'EN PROCESO DE CONVENIO', '<h5>&nbsp;</h5>', 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:34:41', '2015-08-14 10:16:17', NULL, NULL),
(58, 28, 'OMINT', 'EN PROCESO DE CONVENIO', NULL, 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:36:19', '2015-08-14 10:16:39', NULL, NULL),
(59, 28, 'ACASALUD', 'EN PROCESO DE CONVENIO', NULL, 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:36:40', '2015-08-14 10:16:59', NULL, NULL),
(60, 28, 'SanCor Salud', 'Afiliados y/o Asociados', '<p>Sucursal Resistencia<br /> Direcci&oacute;n: Santa fe 281<br /> Asesores comerciales comunicarse a los tel&eacute;fonos:<br /> &ndash; 0362 4762910<br /> &ndash; 0362 4762935</p>', 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:37:54', '2015-08-14 10:17:25', NULL, NULL),
(61, 28, 'FusionArte', 'Afiliados y/o Asociados', '<p style="text-align: left;">E-mail: <a href="mailto:fusionartechaco@gmail.com">fusionartechaco@gmail.com</a></p><p style="text-align: left;">Comercializaci&oacute;n de dise&ntilde;os exclusivos para ba&ntilde;os, bachas, y mesadas en vitrofusi&oacute;n y colocaci&oacute;n de mamparas.<br /> &ndash; 30% de descuento en compras de contado efectivo</p>', 1, 0, '2015-08-14 00:00:00', '2015-08-19 08:38:06', '2015-08-14 10:17:51', NULL, NULL),
(62, 32, 'MODELO CERTIFICACIÓN DE INFORMACIÓN PARA N.B.DEL CHACO S.A', 'nulo', NULL, 1, 0, '2015-08-18 00:00:00', '2015-08-25 08:33:47', '2015-08-18 11:07:07', NULL, 27),
(63, 32, 'Certificación contable de saldo de cuenta por cobrar', 'nulo', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:35:20', '2015-08-20 09:27:09', NULL, 28),
(64, 32, 'Estructura de un informe especial de acuerdo con el Capitulo VIIC de la RT 37', 'Estructura_de_un_informe_especial_de_acuerdo_con_el_Capitulo_VIIC_de_la_RT 37', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:36:09', '2015-08-20 10:58:03', NULL, 29),
(65, 32, 'Certificación de ingresos', 'CERTIFICACIÓN DE INGRESOS', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:36:57', '2015-08-20 11:00:01', NULL, 30),
(66, 32, 'Certificación de ingresos con declaración jurada del comitente', 'CERTIFICACIÓN DE INGRESOS CON DECLARACION JURADA DEL COMITENTE', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:37:36', '2015-08-20 11:10:09', NULL, 31),
(67, 32, 'Modelo Certificacion Manifestacion Bienes y Deudas segun RTN37', 'Modelo_Certificacion_Manifestacion_Bienes_y_Deudas_segun_RTN37', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:38:12', '2015-08-20 11:11:03', NULL, 32),
(68, 32, 'Modelo Certificación origen fondos para compa de automotor UIF según RTN37', 'Modelo_Certificacion_origen_fondos_para_compra_de_automotor_UIF_segun_RTN37', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:39:08', '2015-08-20 11:12:15', NULL, 33),
(69, 32, 'Modelos Informes RT37 Auditoria', 'Modelos_Informes_RT37_Auditoria', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:39:59', '2015-08-20 11:12:56', NULL, 34),
(71, 32, 'Modelos Informes RT37 Diversos', 'Modelos_Informes_RT37_Diversos', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:40:33', '2015-08-20 11:18:05', NULL, 35),
(72, 32, 'Certificación para garantizar SGR', 'CERTIFICACIÓN PARA GARANTIZAR SGR', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:41:59', '2015-08-20 11:18:50', NULL, 36),
(73, 32, 'Ceritificación Contable. Formularios del Registro de Constructores', 'CERTIFICACIÓN CONTABLE. Formularios del REGISTRO DE CONSTRUCTORES', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:42:26', '2015-08-20 11:20:23', NULL, 37),
(74, 32, 'INF. ESPECIAL PARA SECRET. DE TPTE. DE LA NACIÓN', 'INF. ESPECIAL PARA SECRET. DE TPTE. DE LA NACIÓN', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:44:48', '2015-08-20 11:20:53', NULL, 38),
(75, 32, 'Certificación de ingresos proyectados I', 'CERTIFICACIÓN DE INGRESOS PROYECTADOS I', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:45:19', '2015-08-20 11:21:47', NULL, 39),
(76, 32, 'Informe Laboral según RT 3707-2015 SECRET. TPTE. DE LA NACIÓN', 'Informe Laboral según RT 3707-2015 SECRET. TPTE. DE LA NACIÓN', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:46:19', '2015-08-20 11:22:17', NULL, 40),
(77, 32, 'Certif de origen y licitud de fondos sin declaración jurada', 'CERTIF DE ORIGEN Y LICITUD DE FONDOS SIN DECLARACION JURADA', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 08:46:55', '2015-08-20 11:23:04', NULL, 41),
(78, 32, 'CNRT informe de aseguramiento sobre cumplimiento de PN minimo RT37', 'CNRT INFORME DE ASEGURAMIENTO SOBRE CUMPLIMIENTO DE PN MINIMO RT37', NULL, 1, 0, '2015-08-20 00:00:00', '2015-08-25 13:13:10', '2015-08-20 11:23:59', NULL, 48);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pdf`
--

CREATE TABLE IF NOT EXISTS `pdf` (
`id` int(11) NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_upload_dir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `pdf`
--

INSERT INTO `pdf` (`id`, `file_path`, `file_upload_dir`) VALUES
(1, NULL, 'cpceweb'),
(2, NULL, 'cpceweb'),
(3, NULL, 'cpceweb'),
(4, NULL, 'cpceweb'),
(5, NULL, 'cpceweb'),
(6, NULL, 'cpceweb'),
(7, NULL, 'cpceweb'),
(8, NULL, NULL),
(9, NULL, NULL),
(10, NULL, NULL),
(12, NULL, 'cpceweb'),
(13, NULL, 'cpceweb'),
(14, NULL, 'cpceweb'),
(15, NULL, 'cpceweb'),
(18, NULL, 'cpceweb'),
(19, NULL, 'cpceweb'),
(20, NULL, 'cpceweb'),
(21, NULL, 'cpceweb'),
(22, NULL, 'cpceweb'),
(40, NULL, 'cpceweb'),
(41, NULL, 'cpceweb'),
(42, NULL, 'cpceweb'),
(43, NULL, 'cpceweb'),
(44, NULL, 'cpceweb'),
(46, NULL, 'cpceweb'),
(47, NULL, 'cpceweb'),
(48, NULL, 'cpceweb'),
(49, NULL, 'cpceweb'),
(50, NULL, 'cpceweb'),
(51, NULL, 'cpceweb'),
(52, NULL, 'cpceweb'),
(53, NULL, 'cpceweb'),
(58, NULL, 'cpceweb'),
(59, NULL, 'cpceweb'),
(60, NULL, 'cpceweb'),
(61, NULL, 'cpceweb'),
(62, NULL, 'cpceweb'),
(63, NULL, 'cpceweb'),
(64, NULL, 'cpceweb'),
(65, NULL, 'cpceweb'),
(69, NULL, 'cpceweb'),
(70, NULL, 'cpceweb'),
(71, NULL, 'cpceweb'),
(72, NULL, 'cpceweb'),
(73, NULL, 'cpceweb'),
(74, NULL, 'cpceweb'),
(75, NULL, 'cpceweb'),
(76, NULL, 'cpceweb'),
(77, NULL, 'cpceweb'),
(78, NULL, 'cpceweb'),
(79, NULL, 'cpceweb'),
(80, NULL, 'cpceweb'),
(81, NULL, 'cpceweb'),
(82, NULL, 'cpceweb'),
(83, NULL, 'cpceweb'),
(84, NULL, 'cpceweb'),
(85, NULL, 'cpceweb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Plantilla`
--

CREATE TABLE IF NOT EXISTS `Plantilla` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Plantilla`
--

INSERT INTO `Plantilla` (`id`, `nombre`, `activo`, `descripcion`) VALUES
(1, 'blog', 1, 'plantilla blog'),
(2, 'comun', 1, 'muestra solo el contenido de la seccion'),
(3, 'listado', 1, 'listado'),
(4, 'imagenes', 1, 'muestra imagenes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion`
--

CREATE TABLE IF NOT EXISTS `seccion` (
`id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `plantilla_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_externa` tinyint(1) DEFAULT NULL,
  `contenido` longtext COLLATE utf8_unicode_ci,
  `portada` tinyint(1) DEFAULT NULL,
  `archivo_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `seccion`
--

INSERT INTO `seccion` (`id`, `item_id`, `plantilla_id`, `url`, `url_externa`, `contenido`, `portada`, `archivo_id`) VALUES
(1, 1, 1, 'menu_principal_cpce_cpce', 0, NULL, 1, NULL),
(2, 2, 1, 'menu_principal_sipres_sipres', 0, NULL, 1, NULL),
(3, 3, 4, 'menu_principal_secretaria-t-cnica', 0, NULL, 1, NULL),
(4, 4, 1, 'menu_principal_c-m-c-a', 0, NULL, 1, NULL),
(5, 5, 2, 'menu_principal_comisiones', 0, '<h1 style="text-align: center;">Comisiones</h1><p style="text-align: center;">Seleccione una comision para ver sus noticias</p>', 0, NULL),
(7, 7, 1, 'menu_principal_comisiones_jovenes-profesionales', 0, '<h1 style="text-align: center;">Jovenes Profesionales</h1>', 1, NULL),
(8, 8, 2, 'menu_principal_cpce_autoridades', 0, '<h1 class="c11" style="text-align: center;"><strong><span class="c13">AUTORIDADES PER&Iacute;ODO 2013-2015</span></strong></h1><h2 class="c11" style="text-align: center;"><strong><span class="c0">CONSEJO DIRECTIVO</span></strong></h2><p><strong><span class="c0">&nbsp;</span></strong></p><p><a href="https://docs.google.com/document/d/1EP7eWVuHcwLlZAXn-MmUQIngnijzDSiO1zVlKARVCqo/pub?embedded=true" name="4a8982286d8c7d930656c60ff54e0fc02f4f6202"></a><a href="https://docs.google.com/document/d/1EP7eWVuHcwLlZAXn-MmUQIngnijzDSiO1zVlKARVCqo/pub?embedded=true" name="1"></a></p><table class="c7" style="height: 441px;" width="731" cellspacing="0" cellpadding="0"><tbody><tr class="c10"><td class="c6" colspan="1" rowspan="1"><h3 class="c2"><span class="c8">PRESIDENTE</span></h3></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CR. MARSALL, AN&Iacute;BAL OMAR</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 1807</span></p></td></tr><tr class="c10"><td class="c6" colspan="1" rowspan="1"><p class="c2"><span class="c8">VICEPRESIDENTE</span></p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CRA. SILWONIUK, SANDRA SYLVIA</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 1442</span></p></td></tr><tr class="c10"><td class="c6" colspan="1" rowspan="1"><p class="c2"><span class="c8">SECRETARIO</span></p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CR. GALARZA, MARIANO</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 932</span></p></td></tr><tr class="c10"><td class="c6" colspan="1" rowspan="1"><p class="c2"><span class="c8">PROSECRETARIO</span></p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CRA. CAZZANIGA, VIVIANA</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 1676</span></p></td></tr><tr class="c10"><td class="c6" colspan="1" rowspan="1"><p class="c2"><span class="c8">TESORERO</span></p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CRA. GIM&Eacute;NEZ ORT&Iacute;Z, SANDRA VERENA</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 1324</span></p></td></tr><tr class="c10"><td class="c6" colspan="1" rowspan="1"><p class="c2"><span class="c8">PROTESORERO</span></p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CR. ROFF&Eacute;, RICARDO ROBERTO</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 983</span></p></td></tr><tr class="c10"><td class="c6" colspan="1" rowspan="1"><p class="c2"><span class="c8">VOCALES SUPLENTES</span></p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CR. GUALTIERI, LUCAS GERARDO</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 2664</span></p></td></tr><tr class="c10"><td class="c15" colspan="1" rowspan="1"><p class="c2 c3">&nbsp;</p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CRA. PAVLICEK, GLADYS MABEL</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 1605</span></p></td></tr><tr class="c10"><td class="c15" colspan="1" rowspan="1"><p class="c2 c3">&nbsp;</p></td><td class="c1" colspan="1" rowspan="1"><p class="c5"><span class="c9">CR. GERARDO OJEDA RESCHINI</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c5"><span class="c8">Mat. N&ordm; 2724</span></p></td></tr></tbody></table>', 0, NULL),
(10, 10, 2, 'menu_principal_sipres_beneficios-sipress', 0, '<h1 style="text-align: center;">Beneficios Sipress</h1>', 0, NULL),
(11, 11, 4, 'menu_principal_secretaria-t-cnica_descuento-comercio-sipres', 0, NULL, 0, NULL),
(12, 12, 2, 'menu_principal_cpce_historia', 0, '<article id="post-141" class="post-single page-single post-141 page type-page status-publish hentry has_no_thumb"><h1 class="post-title">HISTORIA</h1><div class="post-content"><p><em><strong>Nace el Consejo Profesional de Ciencias Econ&oacute;micas</strong></em></p><p>Luego de concluida la Segunda Guerra Mundial nuestro pa&iacute;s se encontr&oacute; con un fuerte crecimiento econ&oacute;mico. Nuestra provincia no estuvo ajena a este fen&oacute;meno, con pleno empleo y altos salarios se consolido una demanda creciente de bienes y servicios, lo que exigi&oacute; una mayor presencia de los graduados en ciencias econ&oacute;micas para administraci&oacute;n, servicios contables, auditorias y gerenciamientos.</p><p><em><strong>Primeros pasos hacia la organizaci&oacute;n</strong></em></p><p>Los primeros graduados universitarios proven&iacute;an de distintas provincias, principalmente empleados por las grandes firmas acopiadoras de algod&oacute;n. El primer hecho documentado que se conoce, y que obra en el actual archivo del CPCE registrado a partir de un antiguo libro de actas no rubricado, es la constancia de lo actuado&nbsp; un domingo 3&nbsp;de diciembre de 1950, en el local fundacional de la Universidad Popular de la Universidad Popular de Resistencia.</p><p><em><strong>Aprobaci&oacute;n del Estatuto y arancelamiento</strong></em></p><p>El 20 de Septiembre de 1952 en Asamblea General Extraordinaria se aprueba un&aacute;nimente el estatuto, un proyecto de arancel y se pide al Poder Ejecutivo la aprobaci&oacute;n del arancel y la reglamentaci&oacute;n de la Ley 12.921.</p><p><em><strong>Primeras reglamentaciones internas del Consejo</strong></em></p><p>En la Asamblea del 16 de Octubre de 1955 se plantea como uno de los temas principalmente la cuesti&oacute;n de la antig&uuml;edad en el ejercicio de la profesi&oacute;n&hellip; el Consejo Profesional estar&aacute; constituido por 15 miembros inscritos en la matricula, cuya antig&uuml;edad en el titulo no sea menor de 5 a&ntilde;os&hellip;Un mes despu&eacute;s, el 5 de Noviembre, al no haber contadores suficientes con esa antig&uuml;edad, &eacute;sta se baja a 2 a&ntilde;os. El 28 de enero de 1956 se resuelve por unanimidad la reinscripci&oacute;n de la matricula y se establece los aranceles. El proyecto del decreto provincial para la regulaci&oacute;n final del CPCE fue elaborado por los profesionales del Consejo. Estaba encuadrado en el esp&iacute;ritu de la Ley 12.921 y fue aprobado por el Ejecutivo (Interventor Federal, Coronel Pedro Avila) constituy&eacute;ndose en la primera regulaci&oacute;n provincial. El texto estipulaba la solidaridad entre miembros, cuidaba que se cumpla los principios de &eacute;tica, amparaba a los profesionales individualmente y aplicaba &ldquo;<em>correcciones disciplinarias por violaci&oacute;n del c&oacute;digo de &eacute;tica y de los aranceles&rdquo;.&nbsp;</em>En su art&iacute;culo 44 determinaba taxativamente que SOLO PODR&Aacute;N EJERCER&nbsp; las personas matriculadas en el registro llevados por el CPCE del Chaco.</p><p><em><strong>Primer local del Consejo</strong></em></p><p>El 18 de Febrero de 1956 se expone en la reuni&oacute;n habitual del Consejo la necesidad de contar con un lugar f&iacute;sico propio, ya que se segu&iacute;a funcionando en las instalaciones de la Universidad Popular. Tambi&eacute;n eran necesarios muebles y &uacute;tiles de la oficina.</p><p><em><strong>Temprana inserci&oacute;n en el medio</strong></em></p><p>El Consejo fue prontamente reconocido. El primer antecedente fue cumplir con un pedido de la Municipalidad para el estudio del costo del Kw/hora que presentaba la empresa proveedora de energ&iacute;a para la zona. Se realizo a trav&eacute;s de una terna de contadores propuesta por la entidad.</p><p><em><strong>Defensa de los Intereses Profesionales</strong></em></p><p>Se comenz&oacute; con una campa&ntilde;a ante entidades para que se aceptasen y cumpliesen las determinaciones del Decreto 241/56 en el uso del sustantivo Contador.&nbsp; Sucedi&oacute; que algunas entidades bancarias impropiamente aplicaban esta denominaci&oacute;n a los responsables de tareas afines y aspectos parciales de la actividad profesional. Tambi&eacute;n en el &aacute;mbito de la justicia provincial para la aplicaci&oacute;n de las normas de la Ley de Quiebras en materia de Sindicatura.</p><p><em><strong>Recambio de directivos</strong></em></p><p>El decreto provincial 241/56 que reglamenta la ley 12.921 establec&iacute;a que deb&iacute;a convocarse a nuevas elecciones para fijar un nuevo elenco conductor de la entidad. La fecha quedo fijada para el 21 de junio de 1956 y llegado el momento se celebro la reuni&oacute;n para el logro de estos fines. Las elecciones fueron celebradas el 29 de junio y se elige la nueva Comisi&oacute;n Directiva presidida por el Cr. Mario Putallaz, dando cabal cumplimiento&nbsp;a todas las reglamentaciones nacionales y provinciales.&ldquo;<em>Lo que sigue hasta nuestros d&iacute;as, es la lucha cotidiana del desarrollo prestigiosa de este grupo profesional, que con muchos esfuerzos personales y dedicaci&oacute;n intelectual, con responsabilidad y probidad, viene aportando su cooperaci&oacute;n para el avance de nuestra sociedad chaque&ntilde;a nordestina&rdquo;.</em></p></div></article>', 0, NULL),
(13, 13, 2, 'menu_principal_cpce_delegaciones-cpce', 0, '<h2>Delegaci&oacute;n Pcia. Roque S&aacute;enz Pe&ntilde;a</h2><p>Saavedra y Rivadavia</p><p>Tel. 0364- 4422371</p><p>(C.P: 3700) P. R. S&aacute;enz Pe&ntilde;a &ndash; Chaco</p><p>E-mail:&nbsp;<a href="mailto:del.saenzpena@cpcechaco.org.ar">del.saenzpena@cpcechaco.org.ar</a></p><p>&nbsp;</p><h2>Delegaci&oacute;n Villa &Aacute;ngela</h2><p>Espa&ntilde;a N&ordm; 167</p><p>Tel. 03735-421388</p><p>(C.P: 3540) Villa &Aacute;ngela &ndash; Chaco</p><p>E-mail:&nbsp;<a href="mailto:del.villaangel@cpcechaco.org.ar">del.villaangela@cpcechaco.org.ar</a></p><p>&nbsp;</p><h2>Delegaci&oacute;n Las Bre&ntilde;as</h2><p>Lavalle y Rivadavia</p><p>Tel. 03731- 460785</p><p>(C. P: 3724) Las Bre&ntilde;as &ndash; Chaco</p><p>E-mail:&nbsp;<a href="mailto:del.sudoeste@cpcechaco.org.ar">del.sudoeste@cpcechaco.org.ar</a></p>', 0, NULL),
(14, 14, 2, 'menu_principal_cpce_tramites-cpce', 0, '<h1 style="text-align: center;">CPCE Tramites</h1>', 0, NULL),
(15, 15, 2, 'menu_principal_tramites-cpce_tr-mite-de-matriculaci-n', 0, '<div class="post-content"><p style="text-align: center;"><strong>REQUISITOS PARA MATRICULACI&Oacute;N C.P.C.E. DEL CHACO</strong></p><p align="center"><strong>&ndash; Tr&aacute;mite personal &ndash;</strong></p><p>&nbsp;</p><ul><li>Formulario &ldquo;Solicitud de Matr&iacute;cula&rdquo; (provee CPCE)</li></ul><p>Tener en cuenta en &ldquo;Domicilios&rdquo;:</p><p>&ndash; <span style="text-decoration: underline;">Domicilio real</span>: Debe coincidir con el Certificado de Domicilio.</p><p>&ndash; <span style="text-decoration: underline;">Domicilio Profesional</span>: Debe ser dentro de la Provincia del Chaco.</p><p>&ndash; <span style="text-decoration: underline;">Domicilio Legal</span>: Debe ser en la ciudad de Resistencia, Chaco.</p><ul><li>Original del T&iacute;tulo legalizado en la Direcci&oacute;n Nacional de Gesti&oacute;n Universitaria del Ministerio de Educaci&oacute;n, Ciencia y Tecnolog&iacute;a de la Naci&oacute;n.</li><li>Original del T&iacute;tulo legalizado en el Ministerio del Interior.</li><li>Original del T&iacute;tulo, sellado en la D.G.R. Pcia. del Chaco</li><li>2 (dos) fotocopias tama&ntilde;o oficio, ambos lados del t&iacute;tulo.</li><li>2 (d0s) fotocopias 1ra. y 2da. hojas del documento de identidad y hoja &uacute;ltimo cambio de domicilio</li><li>Certificado de domicilio</li><li>Certificado de Buena Conducta emitido por la Polic&iacute;a de la Provincia del Chaco.</li><li>3 (tres) fotos tipo carnet 3 x 3.</li><li>Si est&aacute; matriculado en otro CPCE: Constancia de Libre Deuda y Sanci&oacute;n.</li><li>Formulario &ldquo;Alta Fondos de Alta Complejidad&rdquo; (provee CPCE)</li></ul><p><span style="text-decoration: underline;">&nbsp;</span></p><p><span style="text-decoration: underline;">ADEM&Aacute;S</span>:</p><ul><li><span style="text-decoration: underline;">Si opta por la categor&iacute;a de Afiliado</span>:<ul><li>Ficha de &ldquo;Registro de Firmas&rdquo; (provee CPCE)</li><li>Formulario &ldquo;Alta al SIPRES&rdquo;&nbsp;&nbsp;&nbsp;&nbsp; (provee CPCE)</li></ul></li></ul><p>&nbsp;</p><ul><li><span style="text-decoration: underline;">Si opta por la categor&iacute;a de Asociado</span>: Nota -con car&aacute;cter de declaraci&oacute;n jurada- mediante la cual solicita la matr&iacute;cula en la categor&iacute;a de Asociado por no ejercer la profesi&oacute;n en forma independiente en el &aacute;mbito de la Provincia del Chaco.</li></ul><p>&nbsp;</p><p><strong><span style="text-decoration: underline;">Costos:</span></strong></p><p><strong><span style="text-decoration: underline;">Al momento de retirar la matricula:</span></strong></p><p><strong>&ndash; $ 30.-</strong>&nbsp; Por el pago de estampillas abonadas en el Registro P&uacute;blico de Comercio.</p><p><strong>&ndash; $ 30.-</strong>&nbsp; Por el pago de Tasas Retributivas de ATP.</p><p><strong>&ndash; $&nbsp;&nbsp; 5.-</strong>&nbsp; destinado al pago del Fondo de Alta Complejidad correspondiente al mes de matr&iacute;cula</p><p><strong>&ndash; $ 50.-</strong>&nbsp; Carnet.</p><p>Al mes siguiente:</p><p>&ndash; Asociado $ 5.- destinado al pago del Fondo de Alta Complejidad</p><p>&ndash; Afiliado&nbsp;&nbsp;&nbsp; $ 5.- &iacute;dem anterior m&aacute;s cuota aporte al SIPRES.-</p></div>', 0, NULL),
(20, 20, 1, 'menu_principal_cpce', 0, NULL, 1, NULL),
(24, 24, 1, 'menu_principal_sipres', 0, NULL, 1, NULL),
(25, 25, 2, 'menu_principal_sipres_historia-sipres', 0, '<h1 style="text-align: center;">HISTORIA</h1><div class="sharebox-wrap">&nbsp;</div><div class="post-content"><p><span style="color: #000000;">En la revisi&oacute;n de los hechos ocurridos durante el transcurso de los primeros 50 a&ntilde;os de vida del Consejo Profesional de Ciencias Econ&oacute;micas del Chaco, podemos mencionar como uno de los logros principales la creaci&oacute;n del Sistema Previsional y Social para Profesionales en Ciencias Econ&oacute;micas de la Provincia del Chaco (SI.PRE.S.); en otras palabras, la caja de jubilaci&oacute;n de los colegas que hacen ejercicio de la profesi&oacute;n en forma independiente en la provincia, que ha cumplido sus j&oacute;venes siete a&ntilde;os de vida, teniendo en cuenta que el mismo empez&oacute; a funcionar a partir del 1&ordm; de enero de 1994 con la sanci&oacute;n de la Ley N&ordm; 3978.</span></p><p><span style="color: #000000;">Como presidente del Directorio de Administrador desde su creaci&oacute;n, apelando a los recuerdos de las distintas etapas transitadas hasta lograr la concreci&oacute;n de &eacute;sta aspiraci&oacute;n largamente so&ntilde;ada por muchos colegas, voy a tratar de hacer un relato del camino recorrido hasta llegar a este presente. As&iacute; puedo mencionar que la actividad desarrollada por el Consejo en el campo de la solidaridad tuvo sus inicios a fines de los a&ntilde;os setenta y principio de los ochenta cuando funcionaba la Comisi&oacute;n de Servicios Sociales, integrada entre otros por los Cres. Isidro Blanco y Osvaldo G&oacute;mez, por medio de la cual se empezaron a brindar los primeros beneficios dentro del &aacute;rea social. Pero un paso importante se dio cuando en la Asamblea General Ordinaria del a&ntilde;o 1986, se decidi&oacute; incorporar en la cuota de Derecho de Ejercicio Profesional Anual, un importe con afectaci&oacute;n directa al Departamento de Servicios Sociales que fue creado en ese momento; en el tuve la suerte de trabajar junto a los Cres. Arnoldo J. Mir&oacute;n, Hilda Suligoy, Hugo Deville y Rub&eacute;n Meza.</span></p><p><span style="color: #000000;">En el Departamento se analizaron los servicios que se ven&iacute;an prestando y la posibilidad de incorporar nuevas prestaciones; concluida la evaluaci&oacute;n se redact&oacute; el reglamento general de prestaci&oacute;n de servicios, con el detalle de los requisitos necesarios para acceder a cada uno de ellos; podemos empezar a mencionar algunos de los que se empezaron a brindar; como ser subsidios (por casamiento, nacimiento o fallecimiento); las primeras l&iacute;neas de pr&eacute;stamos, financiamiento de libros y suscripciones; convenio con una obra social prepaga; reintegro por servicios m&eacute;dicos de emergencia. Adem&aacute;s, continuamente se realizaban consultas con los matriculados con la finalidad de conocer las necesidades de los colegas&nbsp;&nbsp;y su grupo familiar y se analizaban distintas propuestas con el objetivo de implementar nuevas prestaciones que solucionen aunque sea parcialmente los inconvenientes con mayor grado de prioridad. Se participaba en la mayor cantidad posible de reuniones en entidades o charlas de personas especialistas y dedicadas a las prestaciones sociales, siempre dentro del marco solidario.</span></p><p><span style="color: #000000;">La continua inquietud por todos estos temas hizo que a mediados de 1990 el Consejo fuera invitado a las reuniones que la Federaci&oacute;n Argentina de Consejos Profesionales realiz&oacute; para tratar de reflotar la Secretar&iacute;a de Servicios Sociales. En las mismas se propuso a la Junta de Gobierno de la Federaci&oacute;n la estructura que se entendi&oacute; que deber&iacute;a tener la Secretar&iacute;a (que es en l&iacute;neas generales la actual) solamente que inicialmente eran 6, luego 8 y hoy son 9 los Consejos integrantes. Lo que podemos mencionar como altamente positivo es que nuestro Consejo desde el inicio estuvo representado, pasando siempre por las elecciones para la renovaci&oacute;n de los Consejos integrantes. En este punto debo mencionar que particularmente tuve la enorme satisfacci&oacute;n y el honor de ser el Representante de nuestra Instituci&oacute;n (siendo el &uacute;nico Consejo, entre los que tuvieron su mandato renovado, que no reemplaz&oacute; a su representante) a lo que estoy eternamente agradecido por el hecho de haberme permitido participar del estudio y la implementaci&oacute;n de los que es hoy una de las concreciones mas importantes que ha logrado la profesi&oacute;n, a la que muchos quieren imitar o incorporarse, que es la creaci&oacute;n del Fondo de Accidentes de Tr&aacute;nsito y los Fondos Solidarios de Alta Complejidad y de Cirug&iacute;as Cardiovasculares y Oftalmol&oacute;gicas (estos dos &uacute;ltimos actualmente unificados por el Fondo Solidario) Estas relaciones brindan cobertura a los Consejos Profesionales de Ciencias Econ&oacute;micas y su grupo familiar primario (c&oacute;nyuge e hijos).</span></p><p><span style="color: #000000;">A mediados de 1993, advertidos de las modificaciones que se introducir&iacute;an en la Ley Nacional de Seguridad Social y tomando como ejemplo el Sistema Previsional del Consejo de Entre R&iacute;os, con el Cr. Daniel San Crist&oacute;bal elaboramos un proyecto tendiente a la creaci&oacute;n de nuestro Sistema Previsional y social que fue puesto a la consideraci&oacute;n de la Asamblea de ese a&ntilde;o , la que aprob&oacute; en general el mismo y design&oacute; una comisi&oacute;n para su estudio en particular, la que integramos, entre otros, con los Cres. Ovidio L&oacute;pez, Gabriel Ojeda y Diego Ballesta. Con el despacho de dicha comisi&oacute;n presentamos el proyecto en la Direcci&oacute;n General Impositiva, sede central y la Secretar&iacute;a de Seguridad Social de la Naci&oacute;n. En los primeros d&iacute;as de diciembre de 1993 nos informaron que no se pod&iacute;a asimilar al de Entre R&iacute;os, porque &eacute;ste Consejo en su ley de creaci&oacute;n contemplaba espec&iacute;ficamente esta posibilidad y en la nuestra no se daba. En esos d&iacute;as en la C&aacute;mara de Diputados de la Provincia se iba a tratar como punto espec&iacute;fico el Sistema Previsional Provincial. Con las modificaciones correspondientes y con el aval de los Diputados E. Moro A. S. Torresagasti, J. Sotelo, L. Glombosky y Burrueco Mansilla elevamos el proyecto de ley de creaci&oacute;n de nuestro sistema. Previamente a la sanci&oacute;n de la Ley, con los Cres. Soporsky y San Crist&oacute;bal tuvimos que explicar y fundamentar el proyecto ante la Comisi&oacute;n de Legislaci&oacute;n de la C&aacute;mara. Una vez sancionada, antes de su promulgaci&oacute;n tuvimos que realizar reuniones con el Ministro de Salud Dr. Laluf y el Secretario de Comunicaci&oacute;n Social Cr. J. Bittman, con la finalidad de evitar el veto de la misma. Como consecuencia de esto el Consejo tuvo que elaborar y poner en vigencia un nuevo Reglamento de Matriculaci&oacute;n que permite optar por la condici&oacute;n de matriculado o asociado. Finalmente, luego de realizar todos estos interminables y agotadores tr&aacute;mites, conseguimos que por medio del Decreto N&ordm; 241 de enero de 1994 la ley sea promulgada, con lo que nuestro Sistema Previsional y Social empez&oacute; a funcionar, que posteriormente fue reconocido como modelo de sistema previsional para los profesionales que hacen ejercicio independiente, sirviendo en todo el &aacute;mbito de la Rep&uacute;blica Argentina.</span></p><p><span style="color: #000000;">Al estar reconocido dentro de los par&aacute;metros de la Ley Nacional de Sistema Integrado de Jubilaciones y Pensiones est&aacute; autom&aacute;ticamente incluido en el R&eacute;gimen Nacional de Reciprocidad de Cajas de Previsi&oacute;n.</span></p><p><span style="color: #000000;">Contempla dentro del mismo cuatro fondos independientes y con destinos espec&iacute;ficos, adoptando las caracter&iacute;sticas de un Sistema de Capitalizaci&oacute;n, pero tambi&eacute;n con una parte solidaria o de reparto, sobre el que se realizan peri&oacute;dicamente c&aacute;lculos actuariales.</span></p><p><span style="color: #000000;">Permite realizar aportes extraordinarios para mejorar el futuro haber jubilatorio que se acreditan en su totalidad en la cuenta individual de capitalizaci&oacute;n del aportante.</span></p><p><span style="color: #000000;">El total del Fondo con que cuenta el Sistema se invierte teniendo en cuenta el men&uacute; de inversiones y los porcentajes establecidos en la Ley de creaci&oacute;n y distribuidos en varias entidades financieras. En este punto podemos mencionar como muy importante los rendimientos obtenidos con los fondos desde su creaci&oacute;n, con una tasa promedio cercana al 12 % anual.</span></p><p><span style="color: #000000;">La capitalizaci&oacute;n es anual y se acredita en la cuenta individual de cada aportante, una vez aprobados los Estados Contables por la Asamblea Anual Ordinaria.</span></p><p><span style="color: #000000;">Algo que me parece de primordial importancia, que todos debemos valorar siendo una caracter&iacute;stica sobresaliente de nuestra Instituci&oacute;n, es el hecho que todos los cargos electivos: Comisi&oacute;n Directiva, Comisi&oacute;n Fiscalizadora, Tribunal de Disciplina y Directorio Administrador del Sistema, se ejercen por vocaci&oacute;n y ad &ndash; honorem.</span></p><p><span style="color: #000000;">En esta breve s&iacute;ntesis he tratado de recordad el largo camino que hemos recorrido con muchos colegas (pido disculpas a los que involuntariamente no he mencionado) en el &aacute;rea de servicios sociales y previsionales que hicieron posible que hoy los Profesionales de Ciencias Econ&oacute;micas del Chaco tengamos nuestra propia caja de jubilaci&oacute;n, con las ventajas que esto otorga que est&aacute; dando sus primeros pero muy seguros pasos.</span></p><p><strong><span style="color: #000000;">Cr. Joaqu&iacute;n Mart&iacute;nez</span></strong></p></div>', 0, NULL),
(26, 26, 2, 'menu_principal_sipres_aurtoridades-sipres', 0, '<h1 class="c6" style="text-align: center;"><span class="c14">AUTORIDADES PER&Iacute;ODO 2013-2015</span></h1><h1 class="c6" style="text-align: center;"><span class="c9">DIRECTORIO ADMINISTRADOR SIPRES</span></h1><h1 style="text-align: center;"><a href="https://docs.google.com/document/d/18Xsa5KL4qG6LUlWa34_S5K6Dj6XhnjV8qyIeeGBSCBU/pub?embedded=true" name="93cdc93f7933f0879b1b6e02881eb544af0506d7"></a><a href="https://docs.google.com/document/d/18Xsa5KL4qG6LUlWa34_S5K6Dj6XhnjV8qyIeeGBSCBU/pub?embedded=true" name="0"></a></h1><p class="c6 c12">&nbsp;</p><p><a href="https://docs.google.com/document/d/18Xsa5KL4qG6LUlWa34_S5K6Dj6XhnjV8qyIeeGBSCBU/pub?embedded=true" name="4f47e687e3db80a2cc3dc702991b408535af36c8"></a><a href="https://docs.google.com/document/d/18Xsa5KL4qG6LUlWa34_S5K6Dj6XhnjV8qyIeeGBSCBU/pub?embedded=true" name="1"></a></p><table class="c11" style="height: 283px;" width="888" cellspacing="0" cellpadding="0"><tbody><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2"><span class="c0">PRESIDENTE</span></p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CR. RIBACK, ZELIG</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 166</span></p></td></tr><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2"><span class="c0">VICEPRESIDENTE</span></p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CRA. CAZZANIGA, VIVIANA</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 1676</span></p></td></tr><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2"><span class="c0">TESORERO</span></p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CR. VESCERA, FRANCISCO</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 609</span></p></td></tr><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2"><span class="c0">SECRETARIO</span></p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CR. PASCUAL, FLORENCIO</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 1378</span></p></td></tr><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2"><span class="c0">VOCALES TITULARES</span></p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">LE. NIEVAS, MARCELO</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 43</span></p></td></tr><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2 c12">&nbsp;</p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CRA. V&Aacute;ZQUEZ, PATRICIA</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 2091</span></p></td></tr><tr class="c4"><td class="c8" colspan="1" rowspan="1"><p class="c2"><span class="c0">VOCALES SUPLENTES</span></p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CR. RAVAROTTO, GUSTAVO</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 942</span></p></td></tr><tr class="c4"><td class="c13" colspan="1" rowspan="1"><p class="c2 c12">&nbsp;</p></td><td class="c7" colspan="1" rowspan="1"><p class="c1"><span class="c5">CR. REINOSO, JOS&Eacute;</span></p></td><td class="c3" colspan="1" rowspan="1"><p class="c1"><span class="c0">Mat. N&ordm; 1719</span></p></td></tr></tbody></table><p class="c6 c12">&nbsp;</p>', 0, NULL),
(27, 27, 2, 'menu_principal_beneficios-sipress_descuentos-sipres-hoteleria', 0, '<h1 class="c13" style="text-align: center;"><span class="c1 c6">DESCUENTOS EN HOTELER&Iacute;A</span></h1><h3 class="c11"><span class="c0">Beneficios de descuentos en hoteler&iacute;a para afiliados al SIPRES... </span></h3><p class="c4 c14">&nbsp;</p><p class="c4"><strong><span class="c0 c1 c2">Hotel Marconi</span></strong><span class="c0 c1">:</span><span class="c0">&nbsp;12% de descuento pago Contado.-</span></p><p class="c10"><strong><span class="c0 c1 c2">Hotel Colon:</span><span class="c0 c1">&nbsp;</span></strong><span class="c0">15% de descuento en los d&iacute;as Viernes, S&aacute;bados y Domingos sobre pago de Contado o Debito.-</span></p><p class="c10"><strong><span class="c0 c1 c2">Hotel Covadonga:</span></strong><span class="c0 c1">&nbsp;</span><span class="c0">10% de Descuento sobre pago de contado.-</span></p><p class="c10"><strong><span class="c0 c1 c2">Hotel Niyat:</span></strong><span class="c0">&nbsp;10% de Descuento Sobre Pago de Contado.-</span></p><p class="c10"><strong><span class="c0 c1 c2">Hotel Casa Mia</span></strong><span class="c0 c1">:</span><span class="c0">&nbsp;10% de Descuento sobre pago de contado.-</span></p><p class="c8"><strong><span class="c0 c1 c2">Hotel San Remo Viking</span></strong><span class="c0 c1">: </span><span class="c0">20% descuento abonando en efectivo. Av. Eolo 199 Pinamar. Buenos Aires. (02254) 482-330. </span><span class="c0 c3 c2"><a class="c5" href="http://www.google.com/url?q=http%3A%2F%2Fwww.sanremohoteles.com%2F&amp;sa=D&amp;sntz=1&amp;usg=AFQjCNGvlxEHhRdMMwIVJX-7IveXTdlPQQ">www.sanremohoteles.com</a></span></p><p class="c8"><span class="c0 c1 c2"><strong>Hotel Colonial</strong>:</span><span class="c0 c1">&nbsp;</span><span class="c0">15% descuento sobre tarifas de mostrador (desde el 01/01/2014 hasta el 31/03/2014). Salta (0387) 4211-740. </span><span class="c0 c3 c2"><a class="c5" href="http://www.google.com/url?q=http%3A%2F%2Fwww.saltahotelcolonial.com.ar%2F&amp;sa=D&amp;sntz=1&amp;usg=AFQjCNHL5Kw9rgMrXroHbvLeSC7QYRP8MA">www.saltahotelcolonial.com.ar</a></span><span class="c0 c3"> <br /></span></p>', 0, NULL),
(28, 28, 4, 'menu_principal_beneficios-sipress_beneficios-sipres-salud', 0, NULL, 0, NULL);
INSERT INTO `seccion` (`id`, `item_id`, `plantilla_id`, `url`, `url_externa`, `contenido`, `portada`, `archivo_id`) VALUES
(30, 30, 2, 'menu_principal_beneficios-sipress_beneficios-sipres-cuota-ayuda-personal', 0, '<h1 style="text-align: center;">SISTEMA DE AYUDA PERSONAL: IMPORTE DE CUOTAS</h1><p><strong>PARA AFILIADOS</strong></p><table style="height: 729px;" border="0" width="816" cellspacing="0"><colgroup span="6" width="85"></colgroup><tbody><tr><td style="border: 1px solid #000000;" align="center" height="32"><span style="font-family: Liberation Serif;">MONTO/CUOTAS</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">12</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">18</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">24</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">30</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">36</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">15000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.485</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.068</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 862</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 741</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 662</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">16000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.583</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.139</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 919</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 790</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 706</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">17000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.682</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.210</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 977</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 840</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 750</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">18000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.781</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.281</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.034</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 889</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 794</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">19000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.880</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.352</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.092</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 938</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 838</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">20000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.979</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.424</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.149</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 988</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 882</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">21000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.078</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.495</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.207</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.037</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 926</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">22000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.177</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.566</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.264</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.086</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 970</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">23000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.276</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.637</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.322</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.136</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.015</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">24000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.375</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.708</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.379</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.185</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.059</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">25000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.474</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.780</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.437</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.235</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.103</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">26000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.573</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.851</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.494</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.284</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.147</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">27000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.672</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.922</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.552</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.333</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.191</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">28000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.771</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.993</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.609</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.383</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.235</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">29000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.870</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.064</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.667</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.432</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.279</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">30000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.969</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.135</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.724</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.482</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.323</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">31000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.068</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.207</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.782</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.521</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.367</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">32000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.167</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.278</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.839</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.580</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.412</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">33000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.266</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.349</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.896</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.630</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.456</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">34000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.365</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.420</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.954</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.679</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.500</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">35000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.464</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.491</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.011</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.728</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.544</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">36000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.563</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.563</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.069</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.778</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.588</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">37000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.662</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.634</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.126</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.827</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.632</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">38000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.761</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.705</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.184</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.877</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.676</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">39000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.860</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.776</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.241</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.926</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.720</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">40000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.959</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.847</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.299</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.975</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.764</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">41000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.058</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.918</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.356</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.025</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.809</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">42000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.157</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.990</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.414</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.074</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.853</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">43000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.256</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.061</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.471</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.124</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.456</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">44000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.355</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.132</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.529</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.173</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.941</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">45000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.454</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.203</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.586</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.222</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.985</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">46000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.553</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.274</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.644</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.272</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.029</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">47000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.652</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.345</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.701</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.321</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.073</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">48000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.750</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.417</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.758</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.370</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.117</span></td></tr></tbody></table><p>&nbsp;</p><p><strong>PARA ASOCIADOS</strong></p><p>&nbsp;</p><table style="height: 611px;" border="0" width="817" cellspacing="0"><colgroup span="6" width="85"></colgroup><tbody><tr><td style="border: 1px solid #000000;" align="center" height="32"><span style="font-family: Liberation Serif;">MONTO/CUOTAS</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">12</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">18</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">24</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">30</span></td><td style="border: 1px solid #000000;" align="center"><span style="font-family: Liberation Serif;">36</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">15000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.514</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.098</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 894</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 774</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 696</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">16000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.615</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.172</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 953</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 825</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 742</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">17000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.716</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.245</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.013</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 877</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 788</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">18000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.817</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.318</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.072</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 928</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 835</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">19000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.918</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.391</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.132</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 980</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 881</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">20000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.019</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.464</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.192</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.031</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 928</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">21000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.120</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.538</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.251</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.083</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 974</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">22000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.221</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.611</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.311</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.135</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.020</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">23000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.322</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.684</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.370</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.186</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.067</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">24000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.423</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.757</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.430</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.238</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.113</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">25000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.524</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.831</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.489</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.289</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.159</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">26000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.625</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.904</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.549</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.341</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.206</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">27000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.726</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.977</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.609</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.392</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.252</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">28000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.827</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.050</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.668</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.444</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.299</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">29000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.928</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.123</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.728</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.496</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.345</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">30000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.029</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.197</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.787</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.547</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.391</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">31000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.130</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.270</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.847</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.599</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.438</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">32000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.231</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.343</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.907</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.650</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.484</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">33000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.332</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.416</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.966</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.702</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.530</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">34000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.433</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.490</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.026</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.882</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.577</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">35000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.534</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.563</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.085</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.805</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.623</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">36000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.635</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.636</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.145</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.857</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.670</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">37000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.736</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.709</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.204</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.908</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.716</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">38000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.837</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.782</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.264</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.960</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.762</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">39000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.938</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.856</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.324</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.011</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.809</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">40000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.038</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.929</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.383</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.063</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.855</span></td></tr><tr><td style="border: 1px solid #000000;" align="center" height="17"><span style="font-family: Liberation Serif;">41000</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 4.139</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 3.002</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.443</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 2.114</span></td><td style="border: 1px solid #000000;" align="left"><span style="font-family: Liberation Serif;">$ 1.901</span></td></tr></tbody></table>', 0, NULL),
(31, 31, 2, 'menu_principal_beneficios-sipress_beneficios-sipres-ayuda-personales', 0, '<h1 style="text-align: center;">AYUDAS PERSONALES</h1><p><br />REQUISITOS PARA AYUDAS PERSONALES (Tanto para solicitante y garante)<br /><br />&nbsp;&nbsp;&nbsp; Nota formal solicitando&nbsp; pr&eacute;stamos, m&aacute;s el formulario &ldquo;A&rdquo; expresando datos personales, monto y cantidad de cuotas.<br /><br /><a href="../../../uploads/cpceweb/archivos/e2bdc628883996cf5f7284aa9c0c86a8a33985b0.pdf"><strong>DESCARGAR EL FORMULARIO A AQU&Iacute;. IMPRIMIRLO POR DUPLICADO.</strong></a><br /><br />&nbsp;&nbsp;&nbsp; Justificar Ingresos mediante: 6 &uacute;ltimas ddjj de ATP, Recibos de sueldo o Certificaci&oacute;n de Ingresos, emitida por CP.<br />&nbsp;&nbsp;&nbsp; Garante Afiliado.<br />&nbsp;&nbsp;&nbsp; No poseer deuda por ning&uacute;n concepto<br />&nbsp;&nbsp;&nbsp; Tasas: Afiliado el 33% y Asociados el 37%.<br />&nbsp;&nbsp;&nbsp; Montos 10 SMVM. Plazos de 12 hasta 36 cuotas.<br /><br />Se trata en Directorio administrador, en funci&oacute;n de los antecedentes se aprueba o no.<br /><br /><strong>Aprobado:</strong><br /><br />&nbsp;&nbsp;&nbsp; Pagar&eacute; firmado, por solicitante y garante, ambos certificado por escribano y sellado en rentas.<br />&nbsp;&nbsp;&nbsp; Deducci&oacute;n de gastos administrativos para afiliados del 2% y asociados del 3%.<br /><br /><strong>VER IMPORTE Y MONTO DE CUOTAS</strong></p>', 0, NULL),
(32, 32, 3, 'menu_principal_secretaria-t-cnica_normas-de-auditor-a-rt-37', 0, NULL, 0, NULL),
(33, 33, 3, 'menu_principal_secretaria-t-cnica_normas-de-auditor-a-rt-7-derogada', 0, NULL, 0, NULL),
(34, 34, 3, 'menu_principal_cpce_normativas-cpce', 0, NULL, 0, NULL),
(35, 35, 3, 'menu_principal_sipres_normativas-sipres', 0, NULL, 0, NULL),
(37, 37, 3, 'menu_principal_c-m-c-a_normativas-cmca', 0, NULL, 0, NULL),
(45, 48, 2, 'http://cpcechaco.org.ar/?page_id=8206', 1, NULL, 0, NULL),
(46, 49, 2, 'http://www.gestion.cpcechaco.org.ar/', 1, NULL, 0, NULL),
(47, 50, 1, 'menu_principal_comisiones_comisi-n-de-capacitacion', 0, NULL, 1, NULL),
(48, 51, 1, 'menu_principal_comisiones_sector-p-blico', 0, NULL, 1, NULL),
(49, 52, 1, 'menu_principal_comisiones_estudios-contables', 0, NULL, 1, NULL),
(50, 53, 1, 'menu_principal_comisiones_comision-de-licenciados', 0, NULL, 1, NULL),
(51, 54, 1, 'menu_principal_comisiones_comision-de-cultura', 0, NULL, 1, NULL),
(52, 55, 1, 'menu_principal_comisiones_comision-de-deportes', 0, NULL, 1, NULL),
(53, 56, 1, 'menu_principal_comisiones_comision-de-responsabilidad-y-balance-social', 0, NULL, 1, NULL),
(54, 57, 1, 'menu_principal_comisiones_comision-de-laboral-y-de-la-seguridad-social', 0, NULL, 1, NULL),
(55, 58, 1, 'menu_principal_comisiones_comision-de-pymes', 0, NULL, 1, NULL),
(56, 59, 1, 'menu_principal_comisiones_comision-de-ceat', 0, NULL, 1, NULL),
(57, 60, 1, 'menu_principal_comisiones_comision-de-mediaci-n', 0, NULL, 1, NULL),
(58, 61, 1, 'menu_principal_comisiones_comision-de-actuaci-n-judicial', 0, NULL, 1, NULL),
(59, 62, 1, 'menu_principal_comisiones_comision-de-matriculaci-n', 0, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Thread`
--

CREATE TABLE IF NOT EXISTS `Thread` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_commentable` tinyint(1) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `last_comment_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video_evento`
--

CREATE TABLE IF NOT EXISTS `video_evento` (
`id` int(11) NOT NULL,
  `evento_id` int(11) DEFAULT NULL,
  `codigoIframe` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  `curso_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video_noticia`
--

CREATE TABLE IF NOT EXISTS `video_noticia` (
`id` int(11) NOT NULL,
  `noticia_id` int(11) DEFAULT NULL,
  `codigoIframe` longtext COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acl_classes`
--
ALTER TABLE `acl_classes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indices de la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`), ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`), ADD KEY `IDX_46C8B806EA000B10` (`class_id`), ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`), ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indices de la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`), ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indices de la tabla `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
 ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`), ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`), ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indices de la tabla `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indices de la tabla `Actividad`
--
ALTER TABLE `Actividad`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Archivo`
--
ALTER TABLE `Archivo`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_FA948D1E3397707A` (`categoria_id`);

--
-- Indices de la tabla `Bloque`
--
ALTER TABLE `Bloque`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Calendario`
--
ALTER TABLE `Calendario`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Categoria`
--
ALTER TABLE `Categoria`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Comment`
--
ALTER TABLE `Comment`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_5BC96BF0E2904019` (`thread_id`), ADD KEY `IDX_5BC96BF0F675F31B` (`author_id`);

--
-- Indices de la tabla `Configuracion`
--
ALTER TABLE `Configuracion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Curso`
--
ALTER TABLE `Curso`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Fecha`
--
ALTER TABLE `Fecha`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_C06998DDA7F6EA19` (`calendario_id`);

--
-- Indices de la tabla `fos_user`
--
ALTER TABLE `fos_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`), ADD UNIQUE KEY `UNIQ_957A64797A5A413A` (`seccion_id`);

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagen_evento`
--
ALTER TABLE `imagen_evento`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_62C2E8B987A5F842` (`evento_id`), ADD KEY `IDX_62C2E8B96014FACA` (`actividad_id`), ADD KEY `IDX_62C2E8B987CB4A1F` (`curso_id`);

--
-- Indices de la tabla `imagen_noticia`
--
ALTER TABLE `imagen_noticia`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_F3D2E4D299926010` (`noticia_id`);

--
-- Indices de la tabla `imagen_portada`
--
ALTER TABLE `imagen_portada`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagen_seccion`
--
ALTER TABLE `imagen_seccion`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_224FAE8D7A5A413A` (`seccion_id`);

--
-- Indices de la tabla `Item`
--
ALTER TABLE `Item`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_BF298A20CCD7E912` (`menu_id`), ADD KEY `IDX_BF298A20727ACA70` (`parent_id`);

--
-- Indices de la tabla `Menu`
--
ALTER TABLE `Menu`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticia`
--
ALTER TABLE `noticia`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_31205F961FACC042` (`imagenPortada_id`), ADD UNIQUE KEY `UNIQ_31205F9646EBF93B` (`archivo_id`), ADD KEY `IDX_31205F967A5A413A` (`seccion_id`);

--
-- Indices de la tabla `pdf`
--
ALTER TABLE `pdf`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Plantilla`
--
ALTER TABLE `Plantilla`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion`
--
ALTER TABLE `seccion`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_E0BD15C9F47645AE` (`url`), ADD UNIQUE KEY `UNIQ_E0BD15C9126F525E` (`item_id`), ADD UNIQUE KEY `UNIQ_E0BD15C946EBF93B` (`archivo_id`), ADD KEY `IDX_E0BD15C9A08F3969` (`plantilla_id`);

--
-- Indices de la tabla `Thread`
--
ALTER TABLE `Thread`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `video_evento`
--
ALTER TABLE `video_evento`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_7FD1ABC887A5F842` (`evento_id`), ADD KEY `IDX_7FD1ABC86014FACA` (`actividad_id`), ADD KEY `IDX_7FD1ABC887CB4A1F` (`curso_id`);

--
-- Indices de la tabla `video_noticia`
--
ALTER TABLE `video_noticia`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_D4CDB63B99926010` (`noticia_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acl_classes`
--
ALTER TABLE `acl_classes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Archivo`
--
ALTER TABLE `Archivo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de la tabla `Bloque`
--
ALTER TABLE `Bloque`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Calendario`
--
ALTER TABLE `Calendario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Categoria`
--
ALTER TABLE `Categoria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `Comment`
--
ALTER TABLE `Comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Configuracion`
--
ALTER TABLE `Configuracion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Fecha`
--
ALTER TABLE `Fecha`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fos_user`
--
ALTER TABLE `fos_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `imagen_portada`
--
ALTER TABLE `imagen_portada`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT de la tabla `Item`
--
ALTER TABLE `Item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `Menu`
--
ALTER TABLE `Menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `noticia`
--
ALTER TABLE `noticia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT de la tabla `pdf`
--
ALTER TABLE `pdf`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT de la tabla `Plantilla`
--
ALTER TABLE `Plantilla`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `seccion`
--
ALTER TABLE `seccion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `video_evento`
--
ALTER TABLE `video_evento`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `video_noticia`
--
ALTER TABLE `video_noticia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acl_entries`
--
ALTER TABLE `acl_entries`
ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Filtros para la tabla `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Actividad`
--
ALTER TABLE `Actividad`
ADD CONSTRAINT `FK_F033FA5BF396750` FOREIGN KEY (`id`) REFERENCES `Calendario` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Archivo`
--
ALTER TABLE `Archivo`
ADD CONSTRAINT `FK_FA948D1E3397707A` FOREIGN KEY (`categoria_id`) REFERENCES `Categoria` (`id`);

--
-- Filtros para la tabla `Comment`
--
ALTER TABLE `Comment`
ADD CONSTRAINT `FK_5BC96BF0E2904019` FOREIGN KEY (`thread_id`) REFERENCES `Thread` (`id`),
ADD CONSTRAINT `FK_5BC96BF0F675F31B` FOREIGN KEY (`author_id`) REFERENCES `fos_user` (`id`);

--
-- Filtros para la tabla `Curso`
--
ALTER TABLE `Curso`
ADD CONSTRAINT `FK_BFA6FE8BF396750` FOREIGN KEY (`id`) REFERENCES `Calendario` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `evento`
--
ALTER TABLE `evento`
ADD CONSTRAINT `FK_47860B05BF396750` FOREIGN KEY (`id`) REFERENCES `Calendario` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Fecha`
--
ALTER TABLE `Fecha`
ADD CONSTRAINT `FK_C06998DDA7F6EA19` FOREIGN KEY (`calendario_id`) REFERENCES `Calendario` (`id`);

--
-- Filtros para la tabla `fos_user`
--
ALTER TABLE `fos_user`
ADD CONSTRAINT `FK_957A64797A5A413A` FOREIGN KEY (`seccion_id`) REFERENCES `seccion` (`id`);

--
-- Filtros para la tabla `imagen_evento`
--
ALTER TABLE `imagen_evento`
ADD CONSTRAINT `FK_62C2E8B96014FACA` FOREIGN KEY (`actividad_id`) REFERENCES `Actividad` (`id`),
ADD CONSTRAINT `FK_62C2E8B987A5F842` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
ADD CONSTRAINT `FK_62C2E8B987CB4A1F` FOREIGN KEY (`curso_id`) REFERENCES `Curso` (`id`),
ADD CONSTRAINT `FK_62C2E8B9BF396750` FOREIGN KEY (`id`) REFERENCES `imagen` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `imagen_noticia`
--
ALTER TABLE `imagen_noticia`
ADD CONSTRAINT `FK_F3D2E4D299926010` FOREIGN KEY (`noticia_id`) REFERENCES `noticia` (`id`),
ADD CONSTRAINT `FK_F3D2E4D2BF396750` FOREIGN KEY (`id`) REFERENCES `imagen` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `imagen_seccion`
--
ALTER TABLE `imagen_seccion`
ADD CONSTRAINT `FK_224FAE8D7A5A413A` FOREIGN KEY (`seccion_id`) REFERENCES `seccion` (`id`),
ADD CONSTRAINT `FK_224FAE8DBF396750` FOREIGN KEY (`id`) REFERENCES `imagen` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `Item`
--
ALTER TABLE `Item`
ADD CONSTRAINT `FK_BF298A20727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `Item` (`id`),
ADD CONSTRAINT `FK_BF298A20CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `Menu` (`id`);

--
-- Filtros para la tabla `noticia`
--
ALTER TABLE `noticia`
ADD CONSTRAINT `FK_31205F961FACC042` FOREIGN KEY (`imagenPortada_id`) REFERENCES `imagen_portada` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `FK_31205F9646EBF93B` FOREIGN KEY (`archivo_id`) REFERENCES `Archivo` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `FK_31205F967A5A413A` FOREIGN KEY (`seccion_id`) REFERENCES `seccion` (`id`);

--
-- Filtros para la tabla `seccion`
--
ALTER TABLE `seccion`
ADD CONSTRAINT `FK_E0BD15C9126F525E` FOREIGN KEY (`item_id`) REFERENCES `Item` (`id`),
ADD CONSTRAINT `FK_E0BD15C946EBF93B` FOREIGN KEY (`archivo_id`) REFERENCES `Archivo` (`id`) ON DELETE SET NULL,
ADD CONSTRAINT `FK_E0BD15C9A08F3969` FOREIGN KEY (`plantilla_id`) REFERENCES `Plantilla` (`id`);

--
-- Filtros para la tabla `video_evento`
--
ALTER TABLE `video_evento`
ADD CONSTRAINT `FK_7FD1ABC86014FACA` FOREIGN KEY (`actividad_id`) REFERENCES `Actividad` (`id`),
ADD CONSTRAINT `FK_7FD1ABC887A5F842` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`id`),
ADD CONSTRAINT `FK_7FD1ABC887CB4A1F` FOREIGN KEY (`curso_id`) REFERENCES `Curso` (`id`);

--
-- Filtros para la tabla `video_noticia`
--
ALTER TABLE `video_noticia`
ADD CONSTRAINT `FK_D4CDB63B99926010` FOREIGN KEY (`noticia_id`) REFERENCES `noticia` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
