<?php

namespace CmsGa\RepositoryFilesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CategoriaType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class CategoriaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre')
                ->add('secciones', 'select2', array(
                    'class' => 'CmsGa\BackBundle\Entity\Seccion',
                    'url' => 'categoria_autocomplete_seccion',
                    'configs' => array(
                        'multiple' => true, //required true or false
                        'width' => 'off',
                    ),
                    'attr' => array(
                        'class' => "col-lg-12 col-md-12 col-sm-12 col-xs-12",
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\RepositoryFilesBundle\Entity\Categoria',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mwsimple_bundle_repositoryfilesbundle_categoria';
    }

}
