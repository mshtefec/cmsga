<?php

namespace CmsGa\RepositoryFilesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\RepositoryFilesBundle\Entity\Categoria;
use CmsGa\RepositoryFilesBundle\Form\CategoriaType;
use CmsGa\RepositoryFilesBundle\Form\CategoriaFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Categoria controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/categoria")
 */
class CategoriaController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/RepositoryFilesBundle/Resources/config/Categoria.yml',
    );

    /**
     * Lists all Categoria entities.
     *
     * @Route("/", name="admin_categoria")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new CategoriaFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Categoria entity.
     *
     * @Route("/", name="admin_categoria_create")
     * @Method("POST")
     * @Template("MWSRFBundle:Categoria:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new CategoriaType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Categoria entity.
     *
     * @Route("/new", name="admin_categoria_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new CategoriaType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Categoria entity.
     *
     * @Route("/{id}", name="admin_categoria_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Categoria entity.
     *
     * @Route("/{id}/edit", name="admin_categoria_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new CategoriaType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Categoria entity.
     *
     * @Route("/{id}", name="admin_categoria_update")
     * @Method("PUT")
     * @Template("MWSRFBundle:Categoria:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new CategoriaType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Categoria entity.
     *
     * @Route("/{id}", name="admin_categoria_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Categoria.
     *
     * @Route("/exporter/{format}", name="admin_categoria_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Categoria entity.
     *
     * @Route("/autocomplete-forms/get-archivos", name="Categoria_autocomplete_archivos")
     */
    public function getAutocompleteArchivo() {
        $options = array(
            'repository' => "CmsGaRepositoryFilesBundle:Archivo",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Categoria.
     *
     * @Route("/get-table/", name="admin_categoria_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Autocomplete a Menus entity.
     *
     * @Route("/autocomplete-forms/get-seccion", name="categoria_autocomplete_seccion")
     */
    public function getAutocompleteSeccion() {
        $options = array(
            'repository' => "CmsGaBackBundle:Seccion",
            'field' => "id",
        );
        $request = $this->getRequest();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository($options['repository'])->createQueryBuilder('a');
        $qb
                ->select('a, i')
                ->leftJoin('a.item', 'i')
                ->where('a.' . $options['field'] . ' LIKE ?1 or i.title LIKE ?1')
                ->orderBy('a.' . $options['field'] , ' ASC')
                ->setParameter(1, '%' . $term . '%')
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
