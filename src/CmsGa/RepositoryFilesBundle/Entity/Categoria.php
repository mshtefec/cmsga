<?php

namespace CmsGa\RepositoryFilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria.
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="CmsGa\RepositoryFilesBundle\Entity\CategoriaRepository")
 */
class Categoria {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Archivo", mappedBy="categoria", cascade={"persist"}, orphanRemoval=true)
     */
    private $archivos;

    /**
     * @ORM\ManyToMany(targetEntity="CmsGa\BackBundle\Entity\Seccion", mappedBy="categorias", cascade={"all"})
     * */
    private $secciones;

    public function __toString() {
        return $this->getNombre();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Categoria
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Constructor.
     */
    public function __construct() {
        $this->archivos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add archivos.
     *
     * @param \MWSimple\Bundle\RepositoryFilesBundle\Entity\Archivo $archivos
     *
     * @return Categoria
     */
    public function addArchivo(\CmsGa\RepositoryFilesBundle\Entity\Archivo $archivos) {
        $this->archivos[] = $archivos;

        return $this;
    }

    /**
     * Remove archivos.
     *
     * @param \CmsGa\RepositoryFilesBundle\Entity\Archivo $archivos
     */
    public function removeArchivo(\CmsGa\RepositoryFilesBundle\Entity\Archivo $archivos) {
        $this->archivos->removeElement($archivos);
    }

    /**
     * Get archivos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArchivos() {
        return $this->archivos;
    }

    /**
     * Add secciones
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $secciones
     * @return Categoria
     */
    public function addSeccione(\CmsGa\BackBundle\Entity\Seccion $secciones) {
        $secciones->addCategoria($this);
        $this->secciones[] = $secciones;

        return $this;
    }

    /**
     * Remove secciones
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $secciones
     */
    public function removeSeccione(\CmsGa\BackBundle\Entity\Seccion $secciones) {
        $secciones->removeCategoria($this);
        $this->secciones->removeElement($secciones);
    }

    /**
     * Get secciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSecciones() {
        return $this->secciones;
    }

}
