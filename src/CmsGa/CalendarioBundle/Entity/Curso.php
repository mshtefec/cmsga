<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Curso.
 *
 * @ORM\Table(name="curso")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\CursoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Curso extends Calendario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    protected $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=true)
     */
    protected $contenido;

    /**
     * @var int
     *
     * @ORM\OneTomany(targetEntity="ImagenEvento",mappedBy="curso", cascade={"all"}, orphanRemoval=true)  
     */
    protected $imagenes;

    /**
     * @var int
     *
     * @ORM\OneTomany(targetEntity="VideoEvento",mappedBy="curso", cascade={"all"}, orphanRemoval=true)  
     */
    protected $videos;

    /**
     * @var bool
     *
     * @ORM\Column(name="mp_active", type="boolean", options={"default" : 0})
     */
    protected $mpActive = false;

    /**
     * @ORM\OneToMany(targetEntity="CursoMpLink", mappedBy="curso", cascade={"persist", "remove"})
     */
    protected $mpLinks;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->imagenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->videos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cuentas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mpLinks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Curso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set contenido.
     *
     * @param string $contenido
     *
     * @return Curso
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido.
     *
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Add imagenes.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes
     *
     * @return Curso
     */
    public function addImagene(\CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes)
    {
        $imagenes->setCurso($this);
        $this->imagenes[] = $imagenes;

        return $this;
    }

    /**
     * Remove imagenes.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes
     */
    public function removeImagene(\CmsGa\CalendarioBundle\Entity\ImagenEvento $imagenes)
    {
        $this->imagenes->removeElement($imagenes);
    }

    /**
     * Get imagenes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Add videos.
     *
     * @param \CmsGa\CalendarioBundle\Entity\VideoEvento $videos
     *
     * @return Curso
     */
    public function addVideo(\CmsGa\CalendarioBundle\Entity\VideoEvento $videos)
    {
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos.
     *
     * @param \CmsGa\CalendarioBundle\Entity\VideoEvento $videos
     */
    public function removeVideo(\CmsGa\CalendarioBundle\Entity\VideoEvento $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    public function setMpActive($mpActive = false)
    {
        $this->mpActive = (boolean) $mpActive;
    }

    public function getMpActive()
    {
        return $this->mpActive;
    }

    /**
     * Add mpLinks.
     *
     * @param \CmsGa\CalendarioBundle\Entity\CursoMpLink $mpLinks
     *
     * @return Curso
     */
    public function addMpLink(\CmsGa\CalendarioBundle\Entity\CursoMpLink $mpLinks)
    {
        $mpLinks->setCurso($this);
        $this->mpLinks[] = $mpLinks;

        return $this;
    }

    /**
     * Remove mpLinks.
     *
     * @param \CmsGa\CalendarioBundle\Entity\CursoMpLink $mpLinks
     */
    public function removeMpLink(\CmsGa\CalendarioBundle\Entity\CursoMpLink $mpLinks)
    {
        $this->mpLinks->removeElement($mpLinks);
    }

    /**
     * Get mpLinks.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMpLinks()
    {
        return $this->mpLinks;
    }
}
