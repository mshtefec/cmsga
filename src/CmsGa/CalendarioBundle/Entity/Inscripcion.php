<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Inscripcion
 *
 * @ORM\Table(name="inscripcion")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\InscripcionRepository")
 */
class Inscripcion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $estado;

    /**
     * @ORM\ManyToOne(targetEntity="Calendario", inversedBy="inscripcions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calendario_id", referencedColumnName="id", nullable=false)
     * })
     * @Assert\NotNull()
     */
    private $calendario;

    // DATOS

    /**
     * @var string
     *
     * @ORM\Column(name="afi_tipdoc", type="string", length=3)
     * @Assert\NotBlank()
     */
    private $tipdoc;

    /**
     * @var integer
     *
     * @ORM\Column(name="afi_nrodoc", type="integer", length=8)
     * @Assert\NotBlank()
     */
    private $nrodoc;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_nombre", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_titulo", type="string", length=2, nullable=true)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="afi_matricula", type="integer", nullable=true)
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=255)
     * @Assert\Email(
     *     message = "'{{ value }}' no es un correo valido.",
     * )
     */
    private $correo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Inscripcion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Inscripcion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Inscripcion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set calendario
     *
     * @param \CmsGa\CalendarioBundle\Entity\Calendario $calendario
     * @return Inscripcion
     */
    public function setCalendario(\CmsGa\CalendarioBundle\Entity\Calendario $calendario)
    {
        $this->calendario = $calendario;

        return $this;
    }

    /**
     * Get calendario
     *
     * @return \CmsGa\CalendarioBundle\Entity\Calendario 
     */
    public function getCalendario()
    {
        return $this->calendario;
    }

    /**
     * Set tipdoc
     *
     * @param string $tipdoc
     * @return Inscripcion
     */
    public function setTipdoc($tipdoc)
    {
        $this->tipdoc = $tipdoc;

        return $this;
    }

    /**
     * Get tipdoc
     *
     * @return string 
     */
    public function getTipdoc()
    {
        return $this->tipdoc;
    }

    /**
     * Set nrodoc
     *
     * @param integer $nrodoc
     * @return Inscripcion
     */
    public function setNrodoc($nrodoc)
    {
        $this->nrodoc = $nrodoc;

        return $this;
    }

    /**
     * Get nrodoc
     *
     * @return integer 
     */
    public function getNrodoc()
    {
        return $this->nrodoc;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Inscripcion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Inscripcion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set matricula
     *
     * @param integer $matricula
     * @return Inscripcion
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula
     *
     * @return integer 
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Inscripcion
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }
}
