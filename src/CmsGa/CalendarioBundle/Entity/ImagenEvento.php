<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CmsGa\BackBundle\Entity\Imagen;

/**
 * ImagenEvento.
 *
 * @ORM\Table(name="imagen_evento")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ImagenEvento extends Imagen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Evento", inversedBy="imagenes")
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id")
     */
    private $evento;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Actividad", inversedBy="imagenes")
     * @ORM\JoinColumn(name="actividad_id", referencedColumnName="id")
     */
    private $actividad;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="imagenes")
     * @ORM\JoinColumn(name="curso_id", referencedColumnName="id")
     */
    private $curso;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Upload directory.
     *
     * @return string
     */
    public function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.

        /* if ($this->dirname) {
          $this->uploadDir = 'uploads/' . $this->dirname . '/imagen_evento';
          $res = 'uploads/' . $this->dirname . '/imagen_evento';
          } elseif ($this->id) {
          $res = 'uploads/' . $this->uploadDir . '/imagen_evento';
          } else {
          $res = 'uploads/imagen_evento';
          } */

        return 'uploads/'.$this->uploadDir.'/imagen_evento';
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return ImagenNoticia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set evento.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Evento $evento
     *
     * @return ImagenNoticia
     */
    public function setEvento(\CmsGa\CalendarioBundle\Entity\Evento $evento = null)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get evento.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * Set actividad.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Actividad $actividad
     *
     * @return ImagenEvento
     */
    public function setActividad(\CmsGa\CalendarioBundle\Entity\Actividad $actividad = null)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Actividad
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Set curso.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Curso $curso
     *
     * @return ImagenEvento
     */
    public function setCurso(\CmsGa\CalendarioBundle\Entity\Curso $curso = null)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * Get curso.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Curso
     */
    public function getCurso()
    {
        return $this->curso;
    }
}
