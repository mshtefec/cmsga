<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Max Shtefec <max.shtefec@gmail.com>
 * CalendarioCuenta
 *
 * @ORM\Table(name="calendario_cuenta")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\CalendarioCuentaRepository")
 */
class CalendarioCuenta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ControlCuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="control_cuenta_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $controlCuenta;

    /**
     * @var integer
     *
     * @ORM\Column(name="importe", type="integer", nullable=true)
     */
    private $importe;

    /**
     * @ORM\ManyToOne(targetEntity="Calendario", inversedBy="cuentas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calendario_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $calendario;

    /**
     * Constructor.
     */
    public function __construct() { }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Set controlCuenta.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ControlCuenta $controlCuenta
     *
     * @return CalendarioCuenta
     */
    public function setControlCuenta(\CmsGa\CalendarioBundle\Entity\ControlCuenta $controlCuenta)
    {
        $this->controlCuenta = $controlCuenta;

        return $this;
    }

    /**
     * Get controlCuenta.
     *
     * @return \CmsGa\CalendarioBundle\Entity\ControlCuenta
     */
    public function getControlCuenta()
    {
        return $this->controlCuenta;
    }

    /**
     * Set importe.
     *
     * @param integer $importe
     *
     * @return CalendarioCuenta
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe.
     *
     * @return integer
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set calendario.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Calendario $calendario
     *
     * @return CalendarioCuenta
     */
    public function setCalendario(\CmsGa\CalendarioBundle\Entity\Calendario $calendario)
    {
        $this->calendario = $calendario;

        return $this;
    }

    /**
     * Get calendario.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Calendario
     */
    public function getCalendario()
    {
        return $this->calendario;
    }
}
