<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Max Shtefec <max.shtefec@gmail.com>
 * CursoMpLink
 *
 * @ORM\Table(name="curso_mplink")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\CursoMpLinkRepository")
 */
class CursoMpLink
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="importe", type="integer", nullable=true)
     */
    private $importe;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_ref", type="text")
     */
    private $codigoRef;

    /**
     * @var string
     *
     * @ORM\Column(name="dia", type="string", length=50)
     */
    private $dia;

    /**
     * @var string
     *
     * @ORM\Column(name="hora_exp", type="string", length=50)
     */
    private $horaExp;

    /**
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="mpLinks")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="curso_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $curso;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo.
     *
     * @param string $titulo
     *
     * @return CursoMpLink
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo.
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set importe.
     *
     * @param integer $importe
     *
     * @return CursoMpLink
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe.
     *
     * @return integer
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set codigoRef.
     *
     * @param string $codigoRef
     *
     * @return CursoMpLink
     */
    public function setCodigoRef($codigoRef)
    {
        $this->codigoRef = $codigoRef;

        return $this;
    }

    /**
     * Get codigoRef.
     *
     * @return string
     */
    public function getCodigoRef()
    {
        return $this->codigoRef;
    }

    /**
     * Set dia.
     *
     * @param string $dia
     *
     * @return CursoMpLink
     */
    public function setDia($dia)
    {
        $this->dia = $dia;

        return $this;
    }

    /**
     * Get dia.
     *
     * @return string
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Set horaExp.
     *
     * @param string $horaExp
     *
     * @return CursoMpLink
     */
    public function setHoraExp($horaExp)
    {
        $this->horaExp = $horaExp;

        return $this;
    }

    /**
     * Get horaExp.
     *
     * @return string
     */
    public function getHoraExp()
    {
        return $this->horaExp;
    }

    /**
     * Set curso.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Curso $curso
     *
     * @return CursoMpLink
     */
    public function setCurso(\CmsGa\CalendarioBundle\Entity\Curso $curso)
    {
        $this->curso = $curso;

        return $this;
    }

    /**
     * Get curso.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Curso
     */
    public function getCurso()
    {
        return $this->curso;
    }
}
