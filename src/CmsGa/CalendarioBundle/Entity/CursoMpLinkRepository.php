<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * @author Max Shtefec <max.shtefec@gmail.com>
 * CursoMpLinkRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CursoMpLinkRepository extends EntityRepository
{
}
