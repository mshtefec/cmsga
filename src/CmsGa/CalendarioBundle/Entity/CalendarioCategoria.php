<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Max Shtefec <max.shtefec@gmail.com>
 * CalendarioCategoria
 *
 * @ORM\Table(name="calendario_categoria")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\CalendarioCategoriaRepository")
 */
class CalendarioCategoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ControlCategoria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="control_categoria_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $controlCategoria;

    /**
     * @ORM\ManyToOne(targetEntity="Calendario", inversedBy="cuentas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calendario_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $calendario;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set controlCategoria.
     *
     * @param \CmsGa\CalendarioBundle\Entity\ControlCategoria $controlCategoria
     *
     * @return CalendarioCategoria
     */
    public function setControlCategoria(\CmsGa\CalendarioBundle\Entity\ControlCategoria $controlCategoria)
    {
        $this->controlCategoria = $controlCategoria;

        return $this;
    }

    /**
     * Get controlCategoria.
     *
     * @return \CmsGa\CalendarioBundle\Entity\ControlCategoria
     */
    public function getControlCategoria()
    {
        return $this->controlCategoria;
    }

    /**
     * Set calendario.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Calendario $calendario
     *
     * @return CalendarioCategoria
     */
    public function setCalendario(\CmsGa\CalendarioBundle\Entity\Calendario $calendario)
    {
        $this->calendario = $calendario;

        return $this;
    }

    /**
     * Get calendario.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Calendario
     */
    public function getCalendario()
    {
        return $this->calendario;
    }
}
