<?php

namespace CmsGa\CalendarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 * Calendario
 * @ORM\Table(name="calendario")
 * @ORM\Entity(repositoryClass="CmsGa\CalendarioBundle\Entity\CalendarioRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"evento" = "Evento", "curso"="Curso", "actividad"="Actividad"})
 */
class Calendario
{
    /**
     * mixed Unique identifier of this event (optional).
     */

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * string Title/label of the calendar event.
     */

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=200)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=200, nullable=true)
     */
    protected $category;

    /**
     * @var string
     *
     * @ORM\Column(name="disertante", type="string", length=200, nullable=true)
     */
    protected $disertante;

    /**
     * string URL Relative to current path.
     */

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=200)
     */
    protected $url;

    /**
     * @var string HTML color code for the bg color of the event label.
     */

    /**
     * string.
     *
     * @ORM\Column(name="bg_color", type="string", length=50)
     */
    protected $bgColor;

    /**
     * string HTML color code for the foregorund color of the event label.
     */

    /**
     * @var string
     *
     * @ORM\Column(name="fg_color", type="string", length=50)
     */
    protected $fgColor;

    /**
     * string css class for the event label.
     */

    /**
     * @var string
     *
     * @ORM\Column(name="css_class", type="string", length=250, nullable=true)
     */
    protected $cssClass;

    /**
     * \DateTime DateTime object of the event start date/time.
     */

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    protected $startDatetime;

    /**
     * \DateTime DateTime object of the event end date/time.
     */

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     */
    protected $endDatetime;

    /**
     * boolean Is this an all day event?
     */

    /**
     * @var bool
     *
     * @ORM\Column(name="all_day", type="boolean", nullable=true)
     */
    protected $allDay = false;

    /**
     * @ORM\OneToMany(targetEntity="CalendarioCuenta", mappedBy="calendario", cascade={"persist", "remove"})
     */
    protected $cuentas;

    /**
     * @ORM\OneToMany(targetEntity="CalendarioCategoria", mappedBy="calendario", cascade={"persist", "remove"})
     */
    protected $categorias;

    /**
     * @ORM\OneToMany(targetEntity="Fecha", mappedBy="calendario", cascade={"persist", "remove"})
     */
    protected $fechas;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    protected $activo;

    /**
     * @ORM\OneToOne(targetEntity="CmsGa\BackBundle\Entity\ImagenPortada", inversedBy="calendario",cascade={"all"})
     * @ORM\JoinColumn(name="imagenPortada_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * */
    protected $imagenPortada;

    /**
     * @var bool
     *
     * @ORM\Column(name="permite_inscripcion", type="boolean", options={"default" : 0})
     */
    protected $permiteInscripcion = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="sin_control", type="boolean", options={"default" : 0})
     */
    protected $sinControl = false;

    /**
     * @ORM\OneToMany(targetEntity="Inscripcion", mappedBy="calendario", cascade={"persist"})
     */
    protected $inscripcions;

    // public function __construct($title, \DateTime $startDatetime, \DateTime $endDatetime = null, $allDay = false)
    // {
    //     $this->title = $title;
    //     $this->startDatetime = $startDatetime;
    //     $this->setAllDay($allDay);
    //     if ($endDatetime === null && $this->allDay === false) {
    //         throw new \InvalidArgumentException("Must specify an event End DateTime if not an all day event.");
    //     }
    //     $this->endDatetime = $endDatetime;
    // }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->cuentas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inscripcions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->startDatetime = new \DateTime('now');
        $this->endDatetime = new \DateTime('now');
    }

    /**
     * Convert calendar event details to an array.
     * 
     * @return array $event 
     */
    public function toArray()
    {
        $event = array();

        if ($this->id !== null) {
            $event['id'] = $this->id;
        }

        $event['title'] = $this->title;
        $event['start'] = $this->startDatetime->format("Y-m-d\TH:i:sP");

        if ($this->url !== null) {
            $event['url'] = $this->url;
        }

        if ($this->bgColor !== null) {
            $event['backgroundColor'] = $this->bgColor;
            $event['borderColor'] = $this->bgColor;
        }

        if ($this->fgColor !== null) {
            $event['textColor'] = $this->fgColor;
        }

        if ($this->cssClass !== null) {
            $event['className'] = $this->cssClass;
        }

        if ($this->endDatetime !== null) {
            $event['end'] = $this->endDatetime->format("Y-m-d\TH:i:sP");
        }

        $event['allDay'] = $this->allDay;

        return $event;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set disertante.
     *
     * @param string $disertante
     *
     * @return Calendario
     */
    public function setDisertante($disertante)
    {
        $this->disertante = $disertante;

        return $this;
    }

    /**
     * Get disertante.
     *
     * @return string
     */
    public function getDisertante()
    {
        return $this->disertante;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setBgColor($color)
    {
        $this->bgColor = $color;
    }

    public function getBgColor()
    {
        return $this->bgColor;
    }

    public function setFgColor($color)
    {
        $this->fgColor = $color;
    }

    public function getFgColor()
    {
        return $this->fgColor;
    }

    public function setCssClass($class)
    {
        $this->cssClass = $class;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setStartDatetime(\DateTime $start)
    {
        $this->startDatetime = $start;
    }

    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    public function setEndDatetime(\DateTime $end)
    {
        $this->endDatetime = $end;
    }

    public function getEndDatetime()
    {
        return $this->endDatetime;
    }

    public function setAllDay($allDay = false)
    {
        $this->allDay = (boolean) $allDay;
    }

    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * Add cuentas.
     *
     * @param \CmsGa\CalendarioBundle\Entity\CalendarioCuenta $cuentas
     *
     * @return Calendario
     */
    public function addCuenta(\CmsGa\CalendarioBundle\Entity\CalendarioCuenta $cuentas)
    {
        $cuentas->setCalendario($this);
        $this->cuentas[] = $cuentas;

        return $this;
    }

    /**
     * Remove cuentas.
     *
     * @param \CmsGa\CalendarioBundle\Entity\CalendarioCuenta $cuentas
     */
    public function removeCuenta(\CmsGa\CalendarioBundle\Entity\CalendarioCuenta $cuentas)
    {
        $this->cuentas->removeElement($cuentas);
    }

    /**
     * Get cuentas.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCuentas()
    {
        return $this->cuentas;
    }

    /**
     * Add categorias.
     *
     * @param \CmsGa\CalendarioBundle\Entity\CalendarioCategoria $categorias
     *
     * @return Calendario
     */
    public function addCategoria(\CmsGa\CalendarioBundle\Entity\CalendarioCategoria $categorias)
    {
        $categorias->setCalendario($this);
        $this->categorias[] = $categorias;

        return $this;
    }

    /**
     * Remove categorias.
     *
     * @param \CmsGa\CalendarioBundle\Entity\CalendarioCategoria $categorias
     */
    public function removeCategoria(\CmsGa\CalendarioBundle\Entity\CalendarioCategoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Add fechas.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Fecha $fechas
     *
     * @return Calendario
     */
    public function addFecha(\CmsGa\CalendarioBundle\Entity\Fecha $fechas)
    {
        $fechas->setCalendario($this);
        $this->fechas[] = $fechas;

        return $this;
    }

    /**
     * Remove fechas.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Fecha $fechas
     */
    public function removeFecha(\CmsGa\CalendarioBundle\Entity\Fecha $fechas)
    {
        $this->fechas->removeElement($fechas);
    }

    /**
     * Get fechas.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFechas()
    {
        return $this->fechas;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Calendario
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Calendario
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set activo.
     *
     * @param bool $activo
     *
     * @return Calendario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo.
     *
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set imagenPortada.
     *
     * @param \CmsGa\BackBundle\Entity\ImagenPortada $imagenPortada
     *
     * @return Calendario
     */
    public function setImagenPortada(\CmsGa\BackBundle\Entity\ImagenPortada $imagenPortada = null)
    {
        $this->imagenPortada = $imagenPortada;

        return $this;
    }

    /**
     * Get imagenPortada.
     *
     * @return \CmsGa\BackBundle\Entity\ImagenPortada
     */
    public function getImagenPortada()
    {
        return $this->imagenPortada;
    }

    /**
     * Set permiteInscripcion.
     *
     * @param bool $permiteInscripcion
     *
     * @return Calendario
     */
    public function setPermiteInscripcion($permiteInscripcion)
    {
        $this->permiteInscripcion = $permiteInscripcion;

        return $this;
    }

    /**
     * Get permiteInscripcion.
     *
     * @return bool
     */
    public function getPermiteInscripcion()
    {
        return $this->permiteInscripcion;
    }

    /**
     * Set sinControl.
     *
     * @param bool $sinControl
     *
     * @return Calendario
     */
    public function setSinControl($sinControl)
    {
        $this->sinControl = $sinControl;

        return $this;
    }

    /**
     * Get sinControl.
     *
     * @return bool
     */
    public function getSinControl()
    {
        return $this->sinControl;
    }

    /**
     * Add inscripcions
     *
     * @param \CmsGa\CalendarioBundle\Entity\Inscripcion $inscripcions
     * @return Calendario
     */
    public function addInscripcion(\CmsGa\CalendarioBundle\Entity\Inscripcion $inscripcions)
    {
        $this->inscripcions[] = $inscripcions;

        return $this;
    }

    /**
     * Remove inscripcions
     *
     * @param \CmsGa\CalendarioBundle\Entity\Inscripcion $inscripcions
     */
    public function removeInscripcion(\CmsGa\CalendarioBundle\Entity\Inscripcion $inscripcions)
    {
        $this->inscripcions->removeElement($inscripcions);
    }

    /**
     * Get inscripcions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInscripcions()
    {
        return $this->inscripcions;
    }
}
