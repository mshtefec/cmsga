<?php

namespace CmsGa\CalendarioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\CalendarioBundle\Entity\Curso;
use CmsGa\CalendarioBundle\Form\CursoType;
use CmsGa\CalendarioBundle\Form\CursoFilterType;
use Doctrine\Common\Collections\ArrayCollection;
use CmsGa\CalendarioBundle\Entity\Cuenta;
use CmsGa\CalendarioBundle\Entity\CuentaControl;
use CmsGa\CalendarioBundle\Entity\Categoria;
use CmsGa\CalendarioBundle\Entity\Fecha;

/**
 * Curso controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/curso")
 */
class CursoController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/CalendarioBundle/Resources/config/Curso.yml',
    );

    /**
     * Lists all Curso entities.
     *
     * @Route("/", name="admin_curso")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new CursoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Curso entity.
     *
     * @Route("/", name="admin_curso_create")
     * @Method("POST")
     * @Template("CmsGaCalendarioBundle:Curso:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new CursoType(null, $this->getDoctrine()->getManager());
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            //imagen portada
            if (is_null($entity->getImagenPortada()->getFile())) {
                $entity->setImagenPortada(null);
            }

            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            // Obtengo fecha inicio y fin y seteo
            $startEndDatetime = $form->get('startEndDatetime')->getData();
            $startEndDatetime = str_replace(' hasta ', '-', $startEndDatetime);
            $startEndDatetime = explode('-', $startEndDatetime);
            $entity->setStartDatetime(new \DateTime($startEndDatetime[0]));
            $entity->setEndDatetime(new \DateTime($startEndDatetime[1]));
            // Fin obtengo fecha inicio y fin y seteo
            $entity->setUrl($this->createUrl($entity));
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Curso entity.
     *
     * @Route("/new", name="admin_curso_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new CursoType(null, $this->getDoctrine()->getManager());
        $config = $this->getConfig();
        $entity = new $config['entity']();
        $entity->setStartDatetime(new \DateTime('today'));
        $entity->setEndDatetime(new \DateTime('today'));
        //$cuenta = new Cuenta();
        //$categoria = new ArrayCollection();
        //$cuenta->setCuenta('00000000');
        //$cuenta->setImporte(0);
        //$entity->addCuenta($cuenta);
        //$entity->addCategoria($categoria);
        $fecha = new Fecha();
        $dia = $entity->getStartDatetime()->format('D'); //obtengo dia segun fecha
        $fecha->setDia($dia); //seteo el dia en la fecha
        $entity->addFecha($fecha); //agrego fecha
        $form = $this->createCreateForm($config, $entity);

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Curso entity.
     *
     * @Route("/{id}", name="admin_curso_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Curso entity.
     *
     * @Route("/{id}/edit", name="admin_curso_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new CursoType(null, $this->getDoctrine()->getManager());
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }

        $startEndDatetime = $entity->getStartDatetime()->format('m/d/Y') .
                ' hasta ' . $entity->getEndDatetime()->format('m/d/Y');
        $config['editType'] = new CursoType($startEndDatetime, $this->getDoctrine()->getManager());
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Curso entity.
     *
     * @Route("/{id}", name="admin_curso_update")
     * @Method("PUT")
     * @Template("CmsGaCalendarioBundle:Curso:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new CursoType(null, $this->getDoctrine()->getManager());
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        
        $originalTags = new ArrayCollection();
        $originalCuentas = new ArrayCollection();
        $originalCategorias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($entity->getFechas() as $tag) {
            $originalTags->add($tag);
        }

        foreach ($entity->getCuentas() as $cuenta) {
            $originalCuentas->add($cuenta);
        }

        foreach ($entity->getCategorias() as $categoria) {
            $originalCategorias->add($categoria);
        }

        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $values = $request->request->all();
            //imagen portada 
            if (isset($values['CmsGa_CalendarioBundle_curso']['imagenPortada_eliminar'])) {
                $existImagenPortada = $editForm->get('imagenPortada_eliminar')->getData();
            } else {
                $existImagenPortada = null;
            }
            if (!is_null($existImagenPortada)) {
                if ($existImagenPortada) {
                    $em->remove($entity->getImagenPortada());
                }
            }

            if (is_null($entity->getImagenPortada()->getFile()) == true && is_null($entity->getImagenPortada()->getFilePath()) == true) {
                $entity->setImagenPortada(null);
            } elseif (is_null($entity->getImagenPortada()->getFilePath())) {
                $acedirname = $this->container->get('imagen.Dirname');
                $acedirname->setDirname($entity);
            }
            if (count($entity->getImagenes()) > 0) {
                $acedirname = $this->container->get('imagen.Dirname');
                $acedirname->setDirname($entity);
            }
            // remove the relationship between the tag and the Task
            foreach ($originalTags as $tag) {
                if (false === $entity->getFechas()->contains($tag)) {
                    // remove the Task from the Tag
                    // $tag->getCalendario()->removeElement($entity);
                    // if it was a many-to-one relationship, remove the relationship like this
                    // $tag->setTask(null);
                    // $em->persist($tag);
                    // if you wanted to delete the Tag entirely, you can also do that
                    $em->remove($tag);
                }
            }

            foreach ($originalCuentas as $cuenta) {
                if (false === $entity->getCuentas()->contains($cuenta)) {
                    $em->remove($cuenta);
                }
            }

            foreach ($originalCategorias as $categoria) {
                if (false === $entity->getCategorias()->contains($categoria)) {
                    $em->remove($categoria);
                }
            }
            // Obtengo fecha inicio y fin y seteo
            $startEndDatetime = $editForm->get('startEndDatetime')->getData();
            $startEndDatetime = str_replace(' hasta ', '-', $startEndDatetime);
            $startEndDatetime = explode('-', $startEndDatetime);
            $entity->setStartDatetime(new \DateTime($startEndDatetime[0]));
            $entity->setEndDatetime(new \DateTime($startEndDatetime[1]));
            // Fin obtengo fecha inicio y fin y seteo
            $entity->setUrl($this->createUrl($entity));
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                    $this->generateUrl($config['new']) :
                    $this->generateUrl($config['show'], array('id' => $id))
            ;

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Curso entity.
     *
     * @Route("/{id}", name="admin_curso_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Curso.
     *
     * @Route("/exporter/{format}", name="admin_curso_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Exporter Inscripcion.
     *
     * @Route("/exportar/inscriptos/{id}", name="admin_curso_inscripcion_export", options={"expose"=true})
     */
    public function getExporterXLS($id)
    {
        $format = 'xls';
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('CmsGaCalendarioBundle:Inscripcion')->findAllById($id);

        $response = parent::exportCsvAction($format, $query);

        return $response;
    }

    /**
     * Autocomplete a Curso entity.
     *
     * @Route("/autocomplete-forms/get-imagenes", name="Curso_autocomplete_imagenes")
     */
    public function getAutocompleteImagenEvento() {
        $options = array(
            'repository' => "CmsGaCalendarioBundle:ImagenEvento",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Curso entity.
     *
     * @Route("/autocomplete-forms/get-videos", name="Curso_autocomplete_videos")
     */
    public function getAutocompleteVideoEvento() {
        $options = array(
            'repository' => "CmsGaCalendarioBundle:VideoEvento",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Curso entity.
     *
     * @Route("/autocomplete-forms/get-fechas", name="Curso_autocomplete_fechas")
     */
    public function getAutocompleteFecha() {
        $options = array(
            'repository' => "CmsGaCalendarioBundle:Fecha",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Curso entity.
     *
     * @Route("/autocomplete-forms/get-cuentas", name="Curso_autocomplete_cuentas")
     */
    public function getAutocompleteCuenta() {
        
        //$cpceEm = $this->get('doctrine')->getManager('cpce');
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository('CmsGaCalendarioBundle:ControlCuenta')->createQueryBuilder('a');
        $qb
            ->add('where', 'a.cuentaMadre = 13010000 OR a.cuentaMadre = 31040000 OR a.cuentaMadre = 13040000')
            ->add('orderBy', 'a.id ASC')
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getNombre(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Curso entity.
     *
     * @Route("/autocomplete-forms/get-categorias", name="Curso_autocomplete_categorias")
     */
    public function getAutocompleteCategoria() {
        
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository('CmsGaCalendarioBundle:ControlCategoria')->createQueryBuilder('a');
        $qb
            ->add('orderBy', 'a.id ASC')
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getNombre(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Autocomplete a Curso entity.
     *
     * @Route("/autocomplete-forms/get-mplinks", name="Curso_autocomplete_mplinks")
     */
    public function getAutocompleteMpLink() {
        
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository('CmsGaCalendarioBundle:ControlMp')->createQueryBuilder('a');
        $qb
            ->add('orderBy', 'a.id ASC')
        ;
        $entities = $qb->getQuery()->getResult();

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->getNombre(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Datatable Curso.
     *
     * @Route("/get-table/", name="admin_curso_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    private function createUrl($entity) {
        $title = Slug::slugify($entity->getTitle());

        return 'curso/' . $title . '-' . $entity->getStartDatetime()->format('dMy');
    }

}
