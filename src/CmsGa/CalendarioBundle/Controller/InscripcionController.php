<?php

namespace CmsGa\CalendarioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\CalendarioBundle\Entity\Inscripcion;
use CmsGa\CalendarioBundle\Form\InscripcionType;
use CmsGa\CalendarioBundle\Form\InscripcionFilterType;
use Exporter\Source\DoctrineORMQuerySourceIterator;
use Exporter\Source\ArraySourceIterator;
use Exporter\Handler;

/**
 * Inscripcion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/cpce/inscripcion")
 */
class InscripcionController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/CalendarioBundle/Resources/config/Inscripcion.yml',
    );

    /**
     * Lists all Inscripcion entities.
     *
     * @Route("/", name="admin_cpce_inscripcion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new InscripcionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Lists all Inscripcion entities.
     *
     * @Route("/curso-{id}", name="admin_cpce_inscripcion_filtro")
     * @Method("GET")
     * @Template()
     */
    public function indexFiltroAction($id) {
        
        $config = $this->getConfig();

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CmsGaCalendarioBundle:Inscripcion')->findByCalendario($id);

        return array(
            'entities' => $entities,
            'idCalendario' => $id,
            'config' => $config,
        );
    }

    /**
     * Create query.
     *
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQueryFilter($repositorio, $filter = null) {

        $config = $this->getConfig();
        $alias = $config['alias'];
        $select = array();
        $join = array();
        $tipoArray = array();
        $concat = '';
        $groupBy = false;
        $oneToMany = false;
        $concatBoolean = false;
        $contador = 0;
        foreach ($config['fieldsindex'] as $columnas) {
            //select
            if ($columnas['type'] == 'MANY_TO_ONE' || $columnas['type'] == 'ONE_TO_ONE' || $columnas['type'] == 'ONE_TO_MANY') {
                foreach ($columnas['campos'] as $key => $campos) {
                    if ($key == '0') {
                        $concat = $columnas['alias'] . '.' . $campos;
                    } else {
                        $concat = $concat . ",' '," . $columnas['alias'] . '.' . $campos;
                    }
                }
                if ($columnas['type'] == 'ONE_TO_MANY') {
                    $concat = 'GROUP_CONCAT(' . $concat . " SEPARATOR ',')";
                    $groupBy = true;
                    $oneToMany = true;
                } else {
                    //si concat tiene varios campos lo concatena
                    if (substr_count($concat, ',') > 0) {
                        $concat = 'concat(' . $concat . ')';
                        $concatBoolean = true;
                    }
                }

                $select[] = $concat;
                $join[] = array(
                    'alias' => $columnas['alias'],
                    'join' => $columnas['join'],
                );
                $columnas['type'] = 'string';
            } else {
                $select[] = $alias . '.' . $columnas['name'];
            }
            //formato
            if (isset($columnas['date']) || isset($columnas['datetime']) || isset($columnas['datetimetz'])) {
                $formato = $columnas['date'];
            } else {
                $formato = '';
            }
            $tipoArray[] = array(
                'column' => $contador,
                'type' => $columnas['type'],
                'format' => $formato,
            );
            ++$contador;
        }
        
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb
            ->select($select)
            ->from($config['repository'], 'a')
            ->where('calendario.id = :filter')
            ->setParameter('filter', $filter)
        ;

        if (count($join) > 0) {
            foreach ($join as $j) {
                if ($j['join'] == 'INNER') {
                    $qb->join('a.' . $j['alias'], $j['alias']);
                } else {
                    $qb->leftJoin('a.' . $j['alias'], $j['alias']);
                }
            }
            if ($groupBy) {
                $qb->groupBy('a.id');
            }
        }

        return array(
            'query' => $qb,
            'oneToMany' => $oneToMany,
            'concatBoolean' => $concatBoolean,
            'tipoArray' => $tipoArray,
        );
    }

    /**
     * Creates a new Inscripcion entity.
     *
     * @Route("/", name="admin_cpce_inscripcion_create")
     * @Method("POST")
     * @Template("CmsGaCalendarioBundle:Inscripcion:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new InscripcionType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Inscripcion entity.
     *
     * @Route("/new", name="admin_cpce_inscripcion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new InscripcionType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Inscripcion entity.
     *
     * @Route("/{id}", name="admin_cpce_inscripcion_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Inscripcion entity.
     *
     * @Route("/{id}/edit", name="admin_cpce_inscripcion_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new InscripcionType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Inscripcion entity.
     *
     * @Route("/{id}", name="admin_cpce_inscripcion_update")
     * @Method("PUT")
     * @Template("CmsGaCalendarioBundle:Inscripcion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new InscripcionType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Inscripcion entity.
     *
     * @Route("/{id}", name="admin_cpce_inscripcion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Inscripcion.
     *
     * @Route("/exporter/{format}", name="admin_cpce_inscripcion_export")
     */
    public function getExporter($format)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('CmsGaCalendarioBundle:Inscripcion')->findAllOrder();

        $response = parent::exportCsvAction($format, $query);

        return $response;
    }

    /**
     * Exporter Inscripcion.
     *
     * @Route("/exporter/{format}/filter/{filter}", name="admin_cpce_inscripcion_export_filter")
     */
    public function getExporterFilter($format, $filter)
    {
        $config = $this->getConfig();
        $queryBuilder = $this->createQueryFilter($config['repository'], $filter);

        $campos = array();
        //array
        foreach ($config['fieldsindex'] as $key => $value) {
            //if is defined and true
            if (!empty($value['export']) && $value['export']) {
                if ($value['type'] == 'ONE_TO_ONE' || $value['type'] == 'ONE_TO_ONE' || $value['type'] == 'MANY_TO_ONE') {
                    $campos[] = $value['alias'] . '.' . $value['name'];
                } elseif ($value['type'] == 'datetime' || $value['type'] == 'datetimetz') {
                    $campos[] = "DATE_FORMAT(" . $key . ", '%d/%m/%Y %H:%s')";
                } elseif ($value['type'] == 'date') {
                    $campos[] = "DATE_FORMAT(" . $key . ", '%d/%m/%Y')";
                } elseif ($value['type'] == 'time') {
                    $campos[] = "DATE_FORMAT(" . $key . ", '%H:%s')";
                } else {
                    $campos[] = $key;
                }
            }
        }

        $queryBuilder['query']->select($campos);

        $array = $queryBuilder['query']->getQuery()->getArrayResult();

        // Pick a format to export to
        //$format = 'csv';
        // Set Content-Type
        switch ($format) {
            case 'xls':
                $content_type = 'application/vnd.ms-excel';
                break;
            case 'json':
                $content_type = 'application/json';
                break;
            case 'csv':
                $content_type = 'text/csv';
                break;
            default:
                $content_type = 'text/csv';
                break;
        }
        // Location to Export this to
        $export_to = 'php://output';
        // Data to export
        //$exporter_source = new DoctrineORMQuerySourceIterator($query, $campos, 'Y-m-d H:i:s');      
        $exporter_source = new ArraySourceIterator($array);
        // Get an Instance of the Writer
        $exporter_writer = '\Exporter\Writer\\' . ucfirst($format) . 'Writer';
        $exporter_writer = new $exporter_writer($export_to);
        // Generate response
        $response = new Response();
        // Set headers
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Content-type', $content_type);
        $response->headers->set('Expires', 0);
        //$response->headers->set('Content-length', filesize($filename));
        $response->headers->set('Pragma', 'public');
        // Send headers before outputting anything
        $response->sendHeaders();
        // Export to the format
        Handler::create($exporter_source, $exporter_writer)->export();

        return $response;
    }

    /**
     * Autocomplete a Inscripcion entity.
     *
     * @Route("/autocomplete-forms/get-calendario", name="Inscripcion_autocomplete_calendario")
     */
    public function getAutocompleteCalendario()
    {
        $options = array(
            'repository' => "CmsGaCalendarioBundle:Calendario",
            'field'      => "title",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Inscripcion.
     *
     * @Route("/get-table/", name="admin_cpce_inscripcion_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }

    /**
     * change an existing Inscripcion state.
     *
     * @Route("/{id}/edit/state/{state}", name="admin_cpce_estado_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function estadoAction($id, $state)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CmsGaCalendarioBundle:Inscripcion')->find($id);
        $entity->setEstado($state);

        $em->persist($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'La inscripción se actualizo correctamente!');

        return $this->redirect(
            $this->generateUrl('admin_cpce_inscripcion'), 301
        );
    }
 
}