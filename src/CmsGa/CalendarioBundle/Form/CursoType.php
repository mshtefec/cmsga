<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * EventoType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class CursoType extends CalendarioType {

    public function __construct($startEndDatetime = null, $manager) {
        $this->startEndDatetime = $startEndDatetime;
        parent::__construct($manager);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);

        if (is_null($this->startEndDatetime)) {
            $builder->add('startEndDatetime', 'text', array(
                'mapped' => false,
                'label' => 'Fecha inicio - fin',
                'attr' => array(
                    // 'readonly' => 'readonly',
                    'data-format' => 'MM/dd/yyyy',
                ),
            ));
        } else {
            $builder->add('startEndDatetime', 'text', array(
                'mapped' => false,
                'label' => 'Fecha inicio - fin',
                'attr' => array(
                    // 'readonly' => 'readonly',
                    'data-format' => 'MM/dd/yyyy',
                    'value' => $this->startEndDatetime,
                ),
            ));
        }
        $builder
        ->add('mpActive', null, array(
            'label' => 'Cobro con Mercado Pago',
        ))
        ->add('mpLinks', 'collection', array(
            'label' => ' ',
            'type' => new MpLinkType(),
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Curso',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'CmsGa_CalendarioBundle_curso';
    }

}
