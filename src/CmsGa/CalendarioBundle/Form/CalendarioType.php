<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use CmsGa\BackBundle\Form\ImagenPortadaType;

/**
 * EventoType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class CalendarioType extends AbstractType {

    private $manager;

    public function __construct($manager) {
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {
            $isNew = true;
        } else {
            $isNew = false;
        }
        if (is_null($builder->getData()->getImagenPortada())) {
            $existImagenPortada = false;
        } else {
            if (is_null($builder->getData()->getImagenPortada()->getFilePath())) {
                $existImagenPortada = false;
            } else {
                $existImagenPortada = true;
            }
        }
        $builder
            ->add('idPage', 'hidden', array(
                'mapped' => false,
                'required' => false,
                'attr' => array(
                    'class' => 'selectPage',
                ),
            ))
            ->add('title', null, array(
                'label' => 'Título',
            ))
            ->add('disertante', null, array(
                'label' => 'Disertante',
            ))
            ->add('descripcion', null, array(
                'label' => 'Breve Descripción',
            ))
            ->add('category', null, array(
                'label' => 'Categoria',
            ))
            ->add('cuentas', 'collection', array(
                'label' => ' ',
                'type' => new CuentaType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('categorias', 'collection', array(
                'label' => ' ',
                'type' => new CategoriaType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('fechas', 'collection', array(
                'label' => ' ',
                'type' => new FechaType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('activo')
            ->add('allDay', null, array(
                'label' => 'Todo el dia',
            ))
            ->add('permiteInscripcion', null, array(
                'label' => 'Inscripciones',
            ))
            ->add('sinControl', null, array(
                'label' => 'Control de Matriculas',
            ))
            ->add('contenido', 'text', array(
                'required' => false,
                'attr' => array(
                    'class' => 'contenido',
                ),
            ))
            ->add('imagenes', 'collection', array(
                'type' => new ImagenEventoType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ))
            ->add('videos', 'collection', array(
                'type' => new VideoEventoType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ))
            ->add('imagenPortada', new ImagenPortadaType($this->manager, $existImagenPortada))
        ;

        if ($isNew) {
            $builder
                ->add('bgColor', null, array(
                    'label' => 'Color de fondo',
                    'attr' => array(
                        'readonly' => 'readonly',
                        'class' => 'pick-a-color',
                        'value' => 'ffffff'
                    ),
                ))
                ->add('fgColor', null, array(
                    'label' => 'Color de letra',
                    'attr' => array(
                        'readonly' => 'readonly',
                        'class' => 'pick-a-color',
                        'value' => 'ffffff'
                    ),
                ))
            ;
        } else {
            $builder
                ->add('bgColor', null, array(
                    'label' => 'Color de fondo',
                    'attr' => array(
                        'readonly' => 'readonly',
                        'class' => 'pick-a-color',
                    ),
                ))
                ->add('fgColor', null, array(
                    'label' => 'Color de letra',
                    'attr' => array(
                        'readonly' => 'readonly',
                        'class' => 'pick-a-color',
                    ),
                ))
            ;
        }
        if (!$isNew && $existImagenPortada) {
            $builder->add('imagenPortada_eliminar', 'checkbox', array(
                'mapped' => false,
                'label' => 'Eliminar',
                'required' => false,
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Evento',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'CmsGa_CalendarioBundle_calendario';
    }

}
