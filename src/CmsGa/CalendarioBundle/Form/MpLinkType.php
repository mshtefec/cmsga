<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CuentaType form.
 *
 * @author Max Shtefec <max.shtefec@gmail.com>
 */
class MpLinkType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('titulo', 'text')
            ->add('importe')
            ->add('codigoRef', 'text', array(
                'label' => 'Cód. de Referencia'
            ))
            ->add('dia', 'choice', array(
                'label' => 'Fecha de Expiración de Pago',
                'choices' => $this->getDias(),
            ))
            ->add('horaExp', 'text', array(
                'label' => 'Hora de Finalización',
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\CursoMpLink',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'CmsGa_CalendarioBundle_mplinks';
    }

    private function getDias() {
        return array(
            'Mon' => 'Lunes',
            'Tue' => 'Martes',
            'Wed' => 'Miercoles',
            'Thu' => 'Jueves',
            'Fri' => 'Viernes',
            'Sat' => 'Sabado',
            'Sun' => 'Domingo',
        );
    }

}
