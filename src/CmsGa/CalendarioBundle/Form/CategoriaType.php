<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CuentaType form.
 *
 * @author Max Shtefec <max.shtefec@gmail.com>
 */
class CategoriaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('controlCategoria', 'select2', array(
                'class' => 'CmsGa\CalendarioBundle\Entity\ControlCategoria',
                'url'   => 'Curso_autocomplete_categorias',
                'configs' => array(
                    'multiple' => false, //es requerido true o false
                    'width' => '100%'
                ),
                'label' => false,
                'attr' => array(
                    'col' => "col-lg-12 col-md-12 col-sm-12",
                    'class' => "col-lg-12 col-md-12 col-sm-12",
                )
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\CalendarioCategoria',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'CmsGa_CalendarioBundle_categorias';
    }

    private function getCategorias() {
        return array(
            '10' => 'ACTIVOS EN GENERAL',
            '11' => 'ACTIVOS MAS DE TRES AÑOS',
            '12' => 'ACTIVOS MENOS DE TRES AÑOS',
            '17' => 'ACTIVOS JUBILADOS',
            '20' => 'NO ACTIVOS EN GENERAL',
            '21' => 'ASOCIADOS',
            '22' => 'DADOS DE BAJA',
        );
    }

}
