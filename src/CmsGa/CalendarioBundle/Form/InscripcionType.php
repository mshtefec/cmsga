<?php

namespace CmsGa\CalendarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * InscripcionType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class InscripcionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipdoc', 'choice', array(
                'label' => 'Tipo Documento',
                'choices' => array(
                    1 => 'DNI',
                    2 => 'LC',
                    3 => 'LE',
                ),
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('nrodoc', null, array(
                'label' => 'Nro Documento',
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('nombre', null, array(
                'label' => 'Apellido y Nombre',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                )
            ))
            ->add('titulo', 'choice', array(
                'label' => 'Tipo Matricula',
                'choices'  => array(
                    'Contador Público' => "CP",
                    'Licenciado en Administración' => 'LA',
                    'Licenciado en Economía' => 'LE',
                    'Personal' => 'PE',
                    'Sin Matricula' => 'S/M',
                ),
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('matricula', null, array(
                'label' => 'Nro Matricula',
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('correo', null, array(
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                )
            ))
            ->add('calendario', 'select2', array(
                'class' => 'CmsGa\CalendarioBundle\Entity\Calendario',
                'url'   => 'Inscripcion_autocomplete_calendario',
                'configs' => array(
                    'multiple' => false, //es requerido true o false
                    'width' => '100%'
                ),
                'label_attr' => array(
                    'col' => "col-lg-12 col-md-12 col-sm-12",
                    'class' => "col-lg-12 col-md-12 col-sm-12",
                ),
                'attr' => array(
                    'col' => "col-lg-12 col-md-12 col-sm-12",
                    'class' => "col-lg-12 col-md-12 col-sm-12",
                )
            ))
            ->add('estado', 'choice', array(
                'label' => 'Estado',
                'choices' => array(
                    'En Proceso' => 'En Proceso',
                    'Inscripto' => 'Inscripto',
                    'Abonado' => 'Abonado',
                ),
                'attr' => array(
                    'col' => 'col-lg-12 col-md-12 col-sm-12',
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Inscripcion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_calendariobundle_inscripcion';
    }
}
