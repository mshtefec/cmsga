<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Bloque;
use CmsGa\BackBundle\Form\BloqueType;
use CmsGa\BackBundle\Form\BloqueFilterType;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Bloque controller.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 *
 * @Route("/admin/bloque")
 */
class BloqueController extends Controller
{
    /**
     * Lists all Bloque entities.
     *
     * @Route("/", name="admin_bloque")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        list($filterForm, $queryBuilder) = $this->filter();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            10
        );

        return array(
            'entities' => $pagination,
            'filterForm' => $filterForm->createView(),
        );
    }

    /**
     * Process filter request.
     *
     * @return array
     */
    protected function filter()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $filterForm = $this->createFilterForm();
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('CmsGaBackBundle:Bloque')
            ->createQueryBuilder('a')
            ->orderBy('a.id', 'DESC')
        ;
        // Bind values from the request
        $filterForm->handleRequest($request);
        // Reset filter
        if ($filterForm->get('reset')->isClicked()) {
            $session->remove('BloqueControllerFilter');
            $filterForm = $this->createFilterForm();
        }

        // Filter action
        if ($filterForm->get('filter')->isClicked()) {
            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('BloqueControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('BloqueControllerFilter')) {
                $filterData = $session->get('BloqueControllerFilter');
                $filterForm = $this->createFilterForm($filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }
    /**
     * Create filter form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFilterForm($filterData = null)
    {
        $form = $this->createForm(new BloqueFilterType(), $filterData, array(
            'action' => $this->generateUrl('admin_bloque'),
            'method' => 'GET',
        ));

        $form
            ->add('filter', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.index.filter',
                'attr' => array('class' => 'btn btn-success'),
            ))
            ->add('reset', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.index.reset',
                'attr' => array('class' => 'btn btn-danger'),
            ))
        ;

        return $form;
    }

    /**
     * Finds and displays a Bloque entity.
     *
     * @Route("/{id}", name="admin_bloque_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsGaBackBundle:Bloque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bloque entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to edit an existing Bloque entity.
     *
     * @Route("/{id}/edit", name="admin_bloque_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsGaBackBundle:Bloque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bloque entity.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Bloque entity.
     *
     * @param Bloque $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Bloque $entity)
    {
        $form = $this->createForm(new BloqueType(), $entity, array(
            'action' => $this->generateUrl('admin_bloque_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form
            ->add(
                'save', 'submit', array(
                'translation_domain' => 'MWSimpleAdminCrudBundle',
                'label' => 'views.new.save',
                'attr' => array('class' => 'btn btn-success'),
                )
            )
        ;

        return $form;
    }
    /**
     * Edits an existing Bloque entity.
     *
     * @Route("/{id}", name="admin_bloque_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Bloque:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CmsGaBackBundle:Bloque')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bloque entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $this->generateUrl('admin_bloque_show', array('id' => $id));

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('error', 'flash.update.error');

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }
}
