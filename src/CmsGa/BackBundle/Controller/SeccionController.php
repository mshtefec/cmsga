<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Seccion;
use CmsGa\BackBundle\Form\SeccionType;
use CmsGa\BackBundle\Form\SeccionFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Seccion controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/seccion")
 */
class SeccionController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/Seccion.yml',
    );

    /**
     * Lists all Seccion entities.
     *
     * @Route("/", name="admin_seccion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new SeccionFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Seccion entity.
     *
     * @Route("/", name="admin_seccion_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:Seccion:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new SeccionType($this->get('security.authorization_checker'));
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);

            if (!is_null($entity->getArchivo())) {
                if (is_null($entity->getArchivo()->getFile())) {
                    $entity->setArchivo(null);
                }
            }
            $em = $this->getDoctrine()->getManager();
            $entity->getItem()->setLevel($this->generateNivel($entity));
            $this->slugName($entity);
            if (!$entity->getUrlExterna()) {
                //si url externa no esta check entra.
                $entity->setUrl($this->createUrl($entity));
            }
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));
            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Create query.
     * @return Doctrine\ORM\QueryBuilder $queryBuilder
     */
    protected function createQuery() {
        $em = $this->getDoctrine()->getManager();
        $config = $this->getConfig();
        $qb = $em->createQueryBuilder();
        $qb
                ->select('a.id', 'm.name', 'i.title', 'a.url', 'a.publicado','i.orden','a.ordenListado','a.portada','a.permiteNoticias')
                ->from($config['repository'], 'a')
                ->leftJoin('a.item', 'i')
                ->leftJoin('i.menu', 'm')
        ;

        $array = array(
            'query' => $qb,
        );
        return $array;
    }

    /**
     * Displays a form to create a new Seccion entity.
     *
     * @Route("/new", name="admin_seccion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new SeccionType($this->get('security.authorization_checker'));
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Seccion entity.
     *
     * @Route("/{id}", name="admin_seccion_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Seccion entity.
     *
     * @Route("/{id}/edit", name="admin_seccion_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new SeccionType($this->get('security.authorization_checker'));
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $acedirname = $this->container->get('imagen.Dirname');
        $acedirname->setDirname($entity);
        $this->useACL($entity, 'edit');
        $editForm = $this->createEditForm($config, $entity);
        $deleteForm = $this->createDeleteForm($config, $id);

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Seccion entity.
     *
     * @Route("/{id}", name="admin_seccion_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Seccion:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new SeccionType($this->get('security.authorization_checker'));
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            $values = $request->request->all();
            if (isset($values['cmsga_backbundle_seccion']['archivo_eliminar'])) {
                $existArchivo = $editForm->get('archivo_eliminar')->getData();
            } else {
                $existArchivo = null;
            }
            if (!is_null($existArchivo)) {
                if ($existArchivo) {
                    $em->remove($entity->getArchivo());
                }
            }
            if (!is_null($entity->getArchivo())) {
                if (is_null($entity->getArchivo()->getFile()) && is_null($entity->getArchivo()->getFilePath()) == true) {
                    $entity->setArchivo(null);
                }
            }


            $entity->getItem()->setLevel($this->generateNivel($entity));
            $this->slugName($entity);
            if (!$entity->getUrlExterna()) {
                //si url externa no esta check entra.
                $entity->setUrl($this->createUrl($entity));
            }
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $id));
            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Seccion entity.
     *
     * @Route("/{id}", name="admin_seccion_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $config = $this->getConfig();
        $request = $this->getRequest();
        $form = $this->createDeleteForm($config, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($config['repository'])->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
            }
            $acedirname = $this->container->get('imagen.Dirname');
            $acedirname->setDirname($entity);
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.delete.success');
        }

        return $this->redirect($this->generateUrl($config['index']));
    }

    /**
     * Exporter Seccion.
     *
     * @Route("/exporter/{format}", name="admin_seccion_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a Seccion entity.
     *
     * @Route("/autocomplete-forms/get-imagenes", name="Seccion_autocomplete_imagenes")
     */
    public function getAutocompleteImagenSeccion() {
        $options = array(
            'repository' => "CmsGaBackBundle:ImagenSeccion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Seccion entity.
     *
     * @Route("/autocomplete-forms/get-noticias", name="Seccion_autocomplete_noticias")
     */
    public function getAutocompleteNoticia() {
        $options = array(
            'repository' => "CmsGaBackBundle:Noticia",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Seccion entity.
     *
     * @Route("/autocomplete-forms/get-item", name="Seccion_autocomplete_item")
     */
    public function getAutocompleteItem() {
        $options = array(
            'repository' => "MWSimpleDynamicMenuBundle:Item",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Seccion entity.
     *
     * @Route("/autocomplete-forms/get-usuarios", name="Seccion_autocomplete_usuarios")
     */
    public function getAutocompleteUser() {
        $options = array(
            'repository' => "CmsGaBackBundle:User",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Seccion entity.
     *
     * @Route("/autocomplete-forms/get-plantilla", name="Seccion_autocomplete_plantilla")
     */
    public function getAutocompletePlantilla() {
        $options = array(
            'repository' => "CmsGaBackBundle:Plantilla",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Autocomplete a Seccion entity.
     *
     * @Route("/autocomplete-forms/get-archivo", name="Seccion_autocomplete_archivo")
     */
    public function getAutocompleteArchivo() {
        $options = array(
            'repository' => "CmsGaRepositoryFilesBundle:Archivo",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable Seccion.
     *
     * @Route("/get-table/", name="admin_seccion_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * Validar nombre seccion.
     *
     * @Route("/validar-url", name="validar_nombre_url", options={"expose"=true})
     *
     * @return ajax
     */
    public function validarNombreSeccionAction(Request $request) {
        $url = $request->get('urlSeccion');
        $em = $this->getDoctrine()->getManager();
        $qb = $em
                ->getRepository('CmsGaBackBundle:Seccion')
                ->createQueryBuilder('s')
                ->where('s.url LIKE :url')
                ->setParameter('url', '%' . $url . '%');

        $result = $qb->getQuery()->getResult();
        if (!$result) {
            $content = 'ok';
        } else {
            $content = 'existe';
        }
        $response = new Response(
                $content, 200, array(
            'content-type' => 'text/html',
                )
        );

        return $response;
    }

    private function slugName($entity) {
        //creo slug de name del item.
        $name = Slug::slugify($entity->getItem()->getName());
        //seteo el slug en nameSlug.
        $entity->getItem()->setNameSlug($name);
    }

    private function createUrl($entity) {
        $name = $entity->getItem()->getNameSlug();
        //obtengo el menu.
        $menu = $entity->getItem()->getMenu();
        //obtengo el parent del item.
        $parent = $entity->getItem()->getParent();
        //si existe parent entra y utiliza el nameSlug del parent para la url.
        if (!empty($parent)) {
            $parent = $parent->getNameSlug();
            $name = $parent . '_' . $name;
        }
        //debe pertenecer a un menu
        $menu = $menu->getNameSlug();
        $name = $menu . '_' . $name;

        return $name;
    }

    private function generateNivel($entity) {
        $level = 1;
        $parent = $entity->getItem()->getParent();
        while ($parent != null) {
            ++$level;
            $parent = $parent->getParent();
        }

        return $level;
    }

    /**
     * @Route("/autocomplete-forms/get-seccion", name="autocomplete_get_seccion")
     */
    public function getSeccionAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CmsGaBackBundle:Seccion')->findSeccion($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
