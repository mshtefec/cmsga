<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    public function removeTrailingSlashAction()
    {
        $request = $this->getRequest();
        $pathInfo = $request->getPathInfo();
        $requestUri = $request->getRequestUri();

        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

        return $this->redirect($url, 301);
    }

    /**
     * @Route("/admin/", name="index_admin")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    /**
     * @Route("/admin/connect/facebook", name="connect_facebook")
     * @Template()
     */
    public function connectFacebookAction()
    {
        $user       = $this->getUser();
        $paginas    = $user->getPages();
        if (isset($paginas[0])) {
            $paginaFace = $paginas[0]->getPageFace();

            $config = array();
            $config['appId']  = $paginaFace->getConfigFace()->getAppId();
            $config['secret'] = $paginaFace->getConfigFace()->getSecret();
            $config['url']    = $this->generateUrl('index_admin', array(), true);

            $url = $this->get('tecspro_facebook_api')->connectFace($config, $user);
        } else {
            $url = null;
        }

        return array('url' => $url);
    }
}
