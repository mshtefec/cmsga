<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\ConfigFace;
use CmsGa\BackBundle\Form\ConfigFaceType;
use CmsGa\BackBundle\Form\ConfigFaceFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * ConfigFace controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/configface")
 */
class ConfigFaceController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/ConfigFace.yml',
    );

    /**
     * Lists all ConfigFace entities.
     *
     * @Route("/", name="admin_configface")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new ConfigFaceFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new ConfigFace entity.
     *
     * @Route("/", name="admin_configface_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:ConfigFace:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new ConfigFaceType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new ConfigFace entity.
     *
     * @Route("/new", name="admin_configface_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new ConfigFaceType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a ConfigFace entity.
     *
     * @Route("/{id}", name="admin_configface_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing ConfigFace entity.
     *
     * @Route("/{id}/edit", name="admin_configface_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new ConfigFaceType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing ConfigFace entity.
     *
     * @Route("/{id}", name="admin_configface_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:ConfigFace:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new ConfigFaceType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a ConfigFace entity.
     *
     * @Route("/{id}", name="admin_configface_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter ConfigFace.
     *
     * @Route("/exporter/{format}", name="admin_configface_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a ConfigFace entity.
     *
     * @Route("/autocomplete-forms/get-pages", name="ConfigFace_autocomplete_pages", options={"expose"=true})
     */
    public function getAutocompletePageFace() {
        $request = $this->getRequest();
        $term = $request->query->get('q', null);
        $limitarUsuario = true;

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_CONTROL')) {
            $limitarUsuario = false;
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CmsGaBackBundle:PageFace')->findPages($term, $limitarUsuario);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

    /**
     * Datatable ConfigFace.
     *
     * @Route("/get-table/", name="admin_configface_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

}
