<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\User;
use CmsGa\BackBundle\Form\UserType;
use CmsGa\BackBundle\Form\UserFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use CmsGa\BackBundle\Entity\UserSeccion;

/**
 * User controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/user")
 */
class UserController extends Controller {

    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/User.yml',
    );

    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $this->config['filterType'] = new UserFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="admin_user_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:User:new.html.twig")
     */
    public function createAction() {
        $this->config['newType'] = new UserType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $entity = new $config['entity']();
        $form = $this->createCreateForm($config, $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //relaciono las secciones
            $secciones = array();
            foreach ($entity->getSecciones() as $seccion) {
                $idSeccion = $seccion->getSeccion()->getId();
                $exite = array_key_exists($idSeccion, $secciones);
                if (is_bool($exite) == true && $exite == false) {
                    $this->agregarSubsecciones($seccion->getSeccion(), $secciones, $entity);
                }
            }

            foreach ($secciones as $seccion) {
                $userSeccion = new UserSeccion();
                $userSeccion->setSeccion($seccion);
                $entity->addSeccione($userSeccion);
            }

            $em = $this->getDoctrine()->getManager();

            $this->get('fos_user.user_manager')
                    ->updateCanonicalFields($entity);
            $this->get('fos_user.user_manager')
                    ->updatePassword($entity);
            $em->persist($entity);
            $em->flush();
            $this->useACL($entity, 'create');

            $this->get('session')->getFlashBag()->add('success', 'flash.create.success');

            $nextAction = $form->get('saveAndAdd')->isClicked() ? $this->generateUrl($config['new']) : $this->generateUrl($config['show'], array('id' => $entity->getId()));

            return $this->redirect($nextAction);
        }
        $this->get('session')->getFlashBag()->add('danger', 'flash.create.error');

        // remove the form to return to the view
        unset($config['newType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="admin_user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $this->config['newType'] = new UserType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="admin_user_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $config = $this->getConfig();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->findUser($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'show');
        $deleteForm = $this->createDeleteForm($config, $id);

        return array(
            'config' => $config,
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_user_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $this->config['editType'] = new UserType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="admin_user_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:User:edit.html.twig")
     */
    public function updateAction($id) {
        $this->config['editType'] = new UserType();
        $config = $this->getConfig();
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($config['repository'])->find($id);
        $backSecciones = array();
        foreach ($entity->getSecciones() as $seccion) {
            $backSecciones[] = $seccion->getSeccion()->getId();
        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ' . $config['entityName'] . ' entity.');
        }
        $this->useACL($entity, 'update');
        $deleteForm = $this->createDeleteForm($config, $id);
        $editForm = $this->createEditForm($config, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //relaciono las secciones
            $secciones = array();
            foreach ($entity->getSecciones() as $seccion) {
                $idSeccion = $seccion->getSeccion()->getId();
                $exite = array_search($idSeccion, $backSecciones);
                if (is_bool($exite) == true && $exite == false) {
                    $exite = array_key_exists($idSeccion, $secciones);
                    if (is_bool($exite) == true && $exite == false) {
                        $this->agregarSubsecciones($seccion->getSeccion(), $secciones, $entity, $backSecciones);
                    }
                }
            }

            foreach ($secciones as $seccion) {
                $userSeccion = new UserSeccion();
                $userSeccion->setSeccion($seccion);
                $entity->addSeccione($userSeccion);
            }

            $this->get('fos_user.user_manager')
                    ->updateCanonicalFields($entity);
            $this->get('fos_user.user_manager')
                    ->updatePassword($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'flash.update.success');

            $nextAction = $editForm->get('saveAndAdd')->isClicked() ?
                    $this->generateUrl($config['new']) :
                    $this->generateUrl($config['show'], array('id' => $id))
            ;

            return $this->redirect($nextAction);
        }

        $this->get('session')->getFlashBag()->add('danger', 'flash.update.error');

        // remove the form to return to the view
        unset($config['editType']);

        return array(
            'config' => $config,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    function agregarSubsecciones($seccion, &$resultado, $entity, $backSecciones = null) {
        foreach ($seccion->getItem()->getChildren() as $child) {
            $idSeccion = $child->getSeccion()->getId();
            if (!is_null($backSecciones)) {
                $exite = array_search($idSeccion, $backSecciones);
                if (is_bool($exite) == true && $exite == false) {
                    $resultado[$idSeccion] = $child->getSeccion();
                }
            } else {
                $resultado[$idSeccion] = $child->getSeccion();
            }
            $childrenEmpty = $child->getChildren();
            if (!empty($childrenEmpty)) {
                $this->agregarSubsecciones($child->getSeccion(), $resultado, $entity, $backSecciones);
            }
        }
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="admin_user_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter User.
     *
     * @Route("/exporter/{format}", name="admin_user_export")
     */
    public function getExporter($format) {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Autocomplete a User entity.
     *
     * @Route("/autocomplete-forms/get-secciones", name="User_autocomplete_secciones")
     */
    public function getAutocompleteSeccion() {
        $options = array(
            'repository' => "CmsGaBackBundle:Seccion",
            'field' => "id",
        );
        $response = parent::getAutocompleteFormsMwsAction($options);

        return $response;
    }

    /**
     * Datatable User.
     *
     * @Route("/get-table/", name="admin_user_table")
     */
    public function getDatatable() {
        $response = parent::getTable();

        return $response;
    }

    /**
     * @Route("/autocomplete-forms/get-pages", name="autocomplete_get_pages")
     */
    public function getSeccionAction(Request $request) {
        //$this->get('security_role')->controlRolesUser();
        $term = $request->query->get('q', null);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CmsGaBackBundle:PageFace')->findPages($term);

        $array = array();

        foreach ($entities as $entity) {
            $array[] = array(
                'id' => $entity->getId(),
                'text' => $entity->__toString(),
            );
        }

        $response = new JsonResponse();
        $response->setData($array);

        return $response;
    }

}
