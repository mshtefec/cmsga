<?php

namespace CmsGa\BackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MWSimple\Bundle\AdminCrudBundle\Controller\DefaultController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Entity\Correo;
use CmsGa\BackBundle\Form\CorreoType;
use CmsGa\BackBundle\Form\CorreoFilterType;

/**
 * Correo controller.
 * @author Nombre Apellido <name@gmail.com>
 *
 * @Route("/admin/correo")
 */
class CorreoController extends Controller
{
    /**
     * Configuration file.
     */
    protected $config = array(
        'yml' => 'CmsGa/BackBundle/Resources/config/Correo.yml',
    );

    /**
     * Lists all Correo entities.
     *
     * @Route("/", name="admin_correo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $this->config['filterType'] = new CorreoFilterType();
        $response = parent::indexAction();

        return $response;
    }

    /**
     * Creates a new Correo entity.
     *
     * @Route("/", name="admin_correo_create")
     * @Method("POST")
     * @Template("CmsGaBackBundle:Correo:new.html.twig")
     */
    public function createAction()
    {
        $this->config['newType'] = new CorreoType();
        $response = parent::createAction();

        return $response;
    }

    /**
     * Displays a form to create a new Correo entity.
     *
     * @Route("/new", name="admin_correo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->config['newType'] = new CorreoType();
        $response = parent::newAction();

        return $response;
    }

    /**
     * Finds and displays a Correo entity.
     *
     * @Route("/{id}", name="admin_correo_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $response = parent::showAction($id);

        return $response;
    }

    /**
     * Displays a form to edit an existing Correo entity.
     *
     * @Route("/{id}/edit", name="admin_correo_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->config['editType'] = new CorreoType();
        $response = parent::editAction($id);

        return $response;
    }

    /**
     * Edits an existing Correo entity.
     *
     * @Route("/{id}", name="admin_correo_update")
     * @Method("PUT")
     * @Template("CmsGaBackBundle:Correo:edit.html.twig")
     */
    public function updateAction($id)
    {
        $this->config['editType'] = new CorreoType();
        $response = parent::updateAction($id);

        return $response;
    }

    /**
     * Deletes a Correo entity.
     *
     * @Route("/{id}", name="admin_correo_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $response = parent::deleteAction($id);

        return $response;
    }

    /**
     * Exporter Correo.
     *
     * @Route("/exporter/{format}", name="admin_correo_export")
     */
    public function getExporter($format)
    {
        $response = parent::exportCsvAction($format);

        return $response;
    }

    /**
     * Datatable Correo.
     *
     * @Route("/get-table/", name="admin_correo_table")
     */
    public function getDatatable()
    {
        $response = parent::getTable();

        return $response;
    }
 
}