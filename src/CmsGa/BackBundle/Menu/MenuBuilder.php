<?php

namespace CmsGa\BackBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use JMS\SecurityExtraBundle\Metadata\Driver\AnnotationDriver;

/**
 * MenuBuilder.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class MenuBuilder {

    /** @var ContainerInterface */
    private $container;

    /** @var Router */
    private $router;

    /**
     * @var SecurityContext
     */
    private $securityContext;

    /**
     * @var \JMS\SecurityExtraBundle\Metadata\Driver\AnnotationDriver
     */
    private $metadataReader;
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, ContainerInterface $container) {
        $this->factory = $factory;
        $this->container = $container;
        $this->router = $this->container->get('router');
        $this->securityContext = $this->container->get('security.context');
        $this->metadataReader = new AnnotationDriver(new \Doctrine\Common\Annotations\AnnotationReader());
    }

    /**
     * @param $class
     *
     * @return \JMS\SecurityExtraBundle\Metadata\ClassMetadata
     */
    public function getMetadata($class) {
        return $this->metadataReader->loadMetadataForClass(new \ReflectionClass($class));
    }

    public function hasRouteAccess($routeName) {
        $token = $this->securityContext->getToken();
        if ($token->isAuthenticated()) {
            $route = $this->router->getRouteCollection()->get($routeName);
            $controller = $route->getDefault('_controller');
            list($class, $method) = explode('::', $controller, 2);

            $metadata = $this->getMetadata($class);
            if (!isset($metadata->methodMetadata[$method])) {
                return false;
            }

            foreach ($metadata->methodMetadata[$method]->roles as $role) {
                if ($this->securityContext->isGranted($role)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function filterMenu($menu) {
        foreach ($menu->getChildren() as $child) {
            /* @var \Knp\Menu\MenuItem $child */
            $routes = $child->getExtra('routes');
            if ($routes !== null) {
                $route = current(current($routes));

                if ($route && !$this->hasRouteAccess($route)) {
                    $menu->removeChild($child);
                }
            }
            $this->filterMenu($child);
        }

        return $menu;
    }

    /**
     * Create Main Menu.
     *
     * @param  Request $request request
     *                           
     * @return $menu
     */
    public function createMainMenu(Request $request) {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav pull-left');
        $menu->addChild('Menu', array(
            'route' => 'admin_menu',
        ));
        $menu->addChild('Item', array(
            'route' => 'admin_item',
        ));
        $menu->addChild('Sección', array(
            'route' => 'admin_seccion',
        ));
        $menu->addChild('Noticia', array(
            'route' => 'noticia',
        ));
        $menu->addChild('Portada', array(
            'route' => 'admin_imagenportada',
        ));
        $menu->addChild('Eventos', array(
            'route' => 'admin_evento',
        ));
        $menu->addChild('Cursos', array(
            'route' => 'admin_curso',
        ));
        $menu->addChild('Actividades', array(
            'route' => 'admin_actividad',
        ));
        $menu->addChild('Archivos', array(
            'route' => 'admin_archivo',
        ));
        $menu->addChild('Bloques', array(
            'route' => 'admin_bloque',
        ));
        $menu->addChild('Usuarios', array(
            'route' => 'admin_user',
        ));
        $menu->addChild('Contacto', array(
            'route' => 'admin_contacto',
        ));
        $menu->addChild('Correo', array(
            'route' => 'admin_correo',
        ));
        $menu->addChild('Configuracion', array(
            'route' => 'admin_configuracion',
        ));
        $menu->addChild('Plantilla', array(
            'route' => 'admin_plantilla',
        ));     

        $this->filterMenu($menu);

        return $menu;
    }

    /**
     * Create Log Out Menu.
     *
     * @return $menu
     */
    public function createLogOut(Request $request) {
        $menu = $this->factory->createItem('right');
        $menu->setChildrenAttribute('class', 'nav pull-right');
        $menu->addChild('Salir', array(
            'route' => 'fos_user_security_logout',
        ));

        return $menu;
    }

}
