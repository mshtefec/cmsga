<?php

// namespace CmsGa\BackBundle\DataFixtures\ORM;

// use Doctrine\Common\DataFixtures\AbstractFixture;
// use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
// use Doctrine\Common\Persistence\ObjectManager;
// use Symfony\Component\DependencyInjection\ContainerAwareInterface;
// use Symfony\Component\DependencyInjection\ContainerInterface;

// use CmsGa\BackBundle\Entity\Categoria;

// class LoadCategoria extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
// {
//     /**
//      * @var ContainerInterface
//      */
//     private $container;

//     /**
//      * {@inheritDoc}
//      */
//     public function setContainer(ContainerInterface $container = null)
//     {
//         $this->container = $container;
//     }

//     /**
//      * {@inheritDoc}
//      */
//     public function load(ObjectManager $manager)
//     {
//         $categorias =
//         array('noticias', 'academicas', 'investigaciones', 'becas');

//         foreach ($categorias as $key => $value) {
//             $categoria = new Categoria();
//             $categoria->setTipo($value);
//             $manager->persist($categoria);
//         }

//         $manager->flush();
//     }

//     /**
//      * {@inheritDoc}
//      */
//     public function getOrder()
//     {
//         return 4; // the order in which fixtures will be loaded
//     }
// }

