<?php

namespace CmsGa\BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CmsGa\BackBundle\Entity\User;

class LoadUser extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        //Creando Admin !!
        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@admin.com');
        $user->setEnabled(true);
        $user->setPassword('admin');
        $user->setRoles(array('ROLE_ADMIN'));
        // Completar las propiedades que el usuario no rellena en el formulario
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $passwordCodificado = $encoder->encodePassword(
            $user->getPassword(),
            $user->getSalt()
        );
        $user->setPassword($passwordCodificado);
        $manager->persist($user);
        $user = new User();
        $user->setUsername('seccion');
        $user->setEmail('seccion@admin.com');
        $user->setEnabled(true);
        $user->setPassword('admin');
        $user->setRoles(array('ROLE_SECCION'));
        // Completar las propiedades que el usuario no rellena en el formulario
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $passwordCodificado = $encoder->encodePassword(
            $user->getPassword(),
            $user->getSalt()
        );
        $user->setPassword($passwordCodificado);
        // Guardar el nuevo usuario en la base de datos
        $manager->persist($user);
        $manager->flush();

        $this->addReference('seccion', $user);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
