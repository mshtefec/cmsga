<?php

namespace CmsGa\BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MWSimple\DynamicMenuBundle\Entity\Menu;

class LoadMenu extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $institucional =
        array(
            0 => array(
                'name' => 'Menu',
                'nameSlug' => 'menu',
            ),
            // 1 => array(
            //     'name' => 'Direcciones',
            //     'nameSlug' => 'direcciones',
            // ),
            // 2 => array(
            //     'name' => 'Enlaces',
            //     'nameSlug' => 'enlaces',
            // ),
        );

        foreach ($institucional as $key => $value) {
            $menu = new Menu();
            $menu->setName($value['name']);
            $menu->setNameSlug($value['nameSlug']);
            $manager->persist($menu);
            $this->addReference($value['nameSlug'], $menu);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
