<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImagenPortadaType extends AbstractType {

    private $manager;
    private $id;

    public function __construct($manager, $id = false) {
        $this->manager = $manager;
        $this->id = $id;
    }

    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $formatoFecha = array();
        $formatoFecha["formato"] = 'dd/mm/yyyy hh:ii';

        if (((is_null($builder->getData())) || is_null($builder->getData()->getId()))) {
            $isNew = true;
            $formatoFecha["minView"] = 2;
        } else {
            $isNew = false;
            $formatoFecha["minView"] = 0;
        }

        if ($this->id) {
            $isNew = false;
        }
        $builder
                ->add(
                        'file', 'mws_field_file', array(
                    'required' => false,
                    'file_path' => 'webPath',
                    'label' => 'Imagen',
                    'show_path' => true
                        )
                )
                ->add(
                        'descripcion', null, array(
                    'label' => 'Descripción',
                        )
                )
                ->add('publicado', null, array(
                    'label' => 'Publicado',
                    'required' => false,
                    'attr' => array(
                        'class' => 'btnPublicado',
                    ),
                ))
        /* ->add('finPublicacion', 'collot_datetime', array(
          'pickerOptions' => array(
          'format' => $formatoFecha["formato"],
          'minView' => $formatoFecha["minView"]
          ),
          'required' => false,
          'label' => 'Fecha Fin de Publicación',
          'attr' => array(
          'readonly' => 'readonly',
          ),
          'block_name' => 'finPublicacion',
          )) */
        ;

        if ($isNew) {
            $data = 1;
            $obtenerProximoOrden = current($this->manager->getRepository('CmsGaBackBundle:ImagenPortada')->findProximoNumeroOrden());
            if (!is_null($obtenerProximoOrden))
                $data = (int) $obtenerProximoOrden + 1;

            $builder
                    ->add('orden', null, array(
                        'label' => 'Orden',
                        'required' => false,
                        'data' => $data
                    ))
                    ->add('finPublicacion', 'date', array(
                        'data' => new \DateTime('now'),
                        'empty_value' => array('year' => '', 'month' => '', 'day' => ''),
                        'required' => false,
                        'format' => 'ddMMMyyyy',
                        'attr' => array(
                            'class' => 'txtFinPublicacion'
                        )
                    ))
            ;
        } else {
            $builder
                    ->add('orden', null, array(
                        'label' => 'Orden',
                        'required' => false,
                    ))
                    ->add('finPublicacion', 'date', array(
                        'empty_value' => array('year' => '', 'month' => '', 'day' => ''),
                        'required' => false,
                        'format' => 'ddMMMyyyy',
                        'attr' => array(
                            'class' => 'txtFinPublicacion'
                        )
                    ))
            ;
        }
    }

    /**
     * Set Default Options.
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\ImagenPortada',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_imagenportada';
    }

}
