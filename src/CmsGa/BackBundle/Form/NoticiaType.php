<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use CmsGa\RepositoryFilesBundle\Form\ArchivoType;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * NoticiaType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class NoticiaType extends AbstractType {

    private $ROLE_ADMIN = "ROLE_ADMIN";
    private $ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
    private $ROLE_CONTROL = "ROLE_CONTROL";
    private $PERMITIR_NOTICIA = "NO";
    private $OPCION_PERMITIR_NOTICIA = "PermisoNoticia";
    protected $authorizationChecker;
    private $configuraciones = array();
    private $limitar;
    private $manager;
    private $tieneImagenPortada;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, $configuraciones, $manager) {
        $this->authorizationChecker = $authorizationChecker;
        $this->configuraciones = $configuraciones;
        $this->manager = $manager;
    }

    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $formatoFecha = array();
        $formatoFecha["formato"] = 'yyyy-mm-dd hh:ii:ss';

        //tiene imagenportada
        if (!is_null($builder->getData()->getImagenPortada())) {
            $this->tieneImagenPortada = true;
        } else {
            $this->tieneImagenPortada = false;
        }

        //isnew
        if (is_null($builder->getData()->getId())) {
            $isNew = true;
            $formatoFecha["minView"] = 2;
        } else {
            $isNew = false;
            $formatoFecha["minView"] = 0;
        }

        //archivo
        if (is_null($builder->getData()->getArchivo())) {
            $existArchivo = false;
        } else {
            if (is_null($builder->getData()->getArchivo()->getFilePath())) {
                $existArchivo = false;
            } else {
                $existArchivo = true;
            }
        }

        //tiene dato imagenportada
        if (is_null($builder->getData()->getImagenPortada())) {
            $existImagenPortada = false;
        } else {
            if (is_null($builder->getData()->getImagenPortada()->getFilePath())) {
                $existImagenPortada = false;
            } else {
                $existImagenPortada = true;
            }
        }

        if ($this->authorizationChecker->isGranted($this->ROLE_CONTROL)) {
            $this->limitar = false;
        } else {
            $this->limitar = true;
        }

        $builder
                ->add('seccion', 'select2', array(
                    'label' => 'Sección',
                    'class' => 'CmsGaBackBundle:Seccion',
                    'url' => 'autocomplete_get_seccion_noticia',
                    'required' => true,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%'
                    ),
                ))
                ->add('fecha', 'datetime', array(
                    'date_format' => 'dd-MMM-yyyy',
                ))
                ->add('fechaFin', 'datetime', array(
                    'date_format' => 'dd-MMM-yyyy',
                ))
                ->add('portada')
                ->add('titulo', null, array(
                    'label' => 'Título',
                ))
                ->add('descripcion', null, array(
                    'label' => 'Descripción',
                ))
                ->add('contenido', 'text', array(
                    'required' => false,
                    'attr' => array(
                        'class' => 'contenido',
                    ),
                ))
                ->add('archivo', new ArchivoType($this->authorizationChecker), array(
                    'label' => false,
                    'required' => false,
                ))
                ->add('idPage', 'hidden', array(
                    'mapped' => false,
                    'required' => false,
                    'attr' => array(
                        'class' => 'selectPage',
                    ),
                ))
        ;

        if ((isset($this->configuraciones[$this->OPCION_PERMITIR_NOTICIA]) && $this->configuraciones[$this->OPCION_PERMITIR_NOTICIA] == $this->PERMITIR_NOTICIA) || ($this->limitar == FALSE)) {
            $builder
                    ->add('publicado')
                    ->add('destacada')
            ;
        }

        //es edit
        if (!$isNew && $existArchivo) {
            $builder->add('archivo_eliminar', 'checkbox', array(
                'mapped' => false,
                'label' => 'Eliminar Archivo',
                'required' => false,
            ));
        }
        if (!$isNew && $existImagenPortada) {
            $builder->add('imagenPortada_eliminar', 'checkbox', array(
                'mapped' => false,
                'label' => 'Eliminar',
                'required' => false,
            ));
        }
        $builder
                ->add('imagenes', 'collection', array(
                    'type' => new ImagenNoticiaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                ))
                ->add('videos', 'collection', array(
                    'type' => new VideoNoticiaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                ))
                ->add('imagenPortada', new ImagenPortadaType($this->manager, $this->tieneImagenPortada))
                ->add('realpaths', 'hidden', array(
                    'required' => false,
                ))
        ;
    }

    /**
     * Set Default.
     *
     * @param OptionsResolverInterface $resolver resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Noticia',
            'cascade_validation' => true,
        ));
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_noticia';
    }

}
