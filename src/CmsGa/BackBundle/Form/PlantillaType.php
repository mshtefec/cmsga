<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * PlantillaType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class PlantillaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nombre')
                ->add('activo', null, array(
                    'required' => false,
                ))
                ->add('descripcion')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Plantilla',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_plantilla';
    }
}
