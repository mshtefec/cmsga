<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * User Seccion Type form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class UserSeccionType extends AbstractType {

    /**
     * Form Builder.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('seccion', 'select2', array(
                    'label' => 'Sección',
                    'class' => 'CmsGa\BackBundle\Entity\Seccion',
                    'url' => 'autocomplete_get_seccion',
                    'required' => true,
                    'configs' => array(
                        'multiple' => false, //es requerido true o false
                        'width' => '100%'
                    ),
                ))
        ;
    }

    /**
     * Set Default Options.
     *
     * @param OptionsResolverInterface $resolver resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(
                array(
                    'data_class' => 'CmsGa\BackBundle\Entity\UserSeccion',
                )
        );
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_userSeccion';
    }

}
