<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use CmsGa\RepositoryFilesBundle\Form\ArchivoType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * SeccionType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class SeccionType extends AbstractType {

    protected $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker) {
        $this->authorizationChecker = $authorizationChecker;
    }
    
    private function getEstilos() {
        return array(
            'cpce' => 'CPCE',
            'sipres' => 'SIPRES',
            'otro' => 'OTRO',
        );
    }

    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder builder 
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {
            $isNew = true;
        } else {
            $isNew = false;
        }
        if (is_null($builder->getData()->getArchivo())) {
            $existArchivo = false;
        } else {
            if (is_null($builder->getData()->getArchivo()->getFilePath())) {
                $existArchivo = false;
            } else {
                $existArchivo = true;
            }
        }
        $builder
                ->add('item', new ItemSeccionType())
                ->add('portada')
                ->add('publicado')
                ->add('permiteNoticias', null, array(
                    'label' => 'Permite Cargar Noticias',
                ))
                ->add('urlExterna')
                ->add('url', null, array(
                    'required' => false,
                ))
                ->add('contenido', 'text', array(
                    'required' => false,
                    'attr' => array(
                        'class' => 'contenido',
                    ),
                ))
                ->add('imagenes', 'collection', array(
                    'type' => new ImagenSeccionType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                ))
                ->add('plantilla', 'entity', array(
                    'class' => 'CmsGaBackBundle:Plantilla',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                                ->where('p.activo=true')
                                ->orderBy('p.descripcion', 'ASC');
                    },
                ))
                ->add('archivo', new ArchivoType($this->authorizationChecker), array(
                    'label' => false,
                    'required' => false,
                ))
                ->add('ordenListado', null, array(
                    'label' => 'Orden de Listado',
                ))
                ->add('colorbtn', 'choice', array(
                    'label' => 'Tipo de Estilo',
                    'choices' => $this->getEstilos(),
                ))
                ->add('iconbtn', null, array(
                    'label' => 'Nombre del Icono',
                ))
        ;
        //es edit
        if (!$isNew && $existArchivo) {
            $builder->add('archivo_eliminar', 'checkbox', array(
                'mapped' => false,
                'label' => 'Eliminar Archivo',
                'required' => false,
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Seccion',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_seccion';
    }

}
