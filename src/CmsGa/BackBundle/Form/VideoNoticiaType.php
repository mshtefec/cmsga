<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Video Noticia Type form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class VideoNoticiaType extends AbstractType
{
    /**
     * Form Builder.
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'codigoIframe', 'textarea', array(
                    'label' => 'Código Iframe',
                    'attr' => array(
                        'class' => 'iframeSrc',
                    ),
                )
            )
            ->add(
                'descripcion', 'textarea', array(
                    'label' => 'Descripción',
                )
            );
    }

    /**
     * Set Default Options.
     *
     * @param OptionsResolverInterface $resolver resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'CmsGa\BackBundle\Entity\VideoNoticia',
            )
        );
    }

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName()
    {
        return 'cmsga_backbundle_videonoticia';
    }
}
