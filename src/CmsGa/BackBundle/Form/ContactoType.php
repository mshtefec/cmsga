<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContactoType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ContactoType extends AbstractType {

    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('correo', null, array(
                    'empty_value' => 'Seleccione destinatario',
                    'label' => 'Destinatario',
                ))
                ->add('nombreYapellido', null, array(
                    'required' => true,
                    'label' => 'Nombre y Apellido',
                ))
                ->add('telefono', null, array(
                    'required' => false,
                    'label' => 'Teléfono',
                ))
                ->add('email', null, array(
                    'required' => true,
                    'label' => 'Correo',
                ))
                ->add('mensaje', "textarea", array(
                    'required' => true,
                    'attr' => array('rows' => '10'),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Contacto',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_contacto';
    }

}
