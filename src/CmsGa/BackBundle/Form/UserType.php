<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use CmsGa\BackBundle\Form\UserSeccionType;
use CmsGa\BackBundle\Form\UserPageFaceType;
use Symfony\Component\Form\FormEvents;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        if (is_null($builder->getData()->getId())) {//New
            $required = true;
        } else {//Edit
            $required = false;
        }
        $builder
            ->add('username', null, array(
                'label' => 'Nombre Usuario',
            ))
            ->add('email', null, array(
                'label' => 'Correo Electrónico',
            ))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Las contraseñas deben conincidir.',
                'options' => array(
                    'attr' => array(
                    ),
                ),
                'required' => $required,
                'first_options' => array(
                    'label' => 'Contraseña',
                ),
                'second_options' => array(
                    'label' => 'Repita Contraseña',
                ),
            ))
            ->add('enabled', null, array(
                'required' => false,
                'label' => 'Habilitado',
            ))
            ->add('roles', 'choice', array(
                'choices' => array(
                    'ROLE_ADMIN'   => 'ADMIN',
                    'ROLE_CONTROL' => 'CONTROL',
                    'ROLE_SECCION' => 'SECCION',
                    'ROLE_EVENTO'  => 'EVENTO',
                ),
                'multiple' => true,
            ))
            ->add('secciones', 'collection', array(
                'label' => false,
                'type' => new UserSeccionType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
            ->add('pages', 'collection', array(
                'label' => false,
                'type' => new UserPageFaceType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\User',
        ));
    }

    public function getName() {
        return 'app_user_registration';
    }
}