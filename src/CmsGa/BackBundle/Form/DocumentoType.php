<?php

namespace CmsGa\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * DocumentoType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class DocumentoType extends AbstractType {

    /**
     * Build Form.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dni', null, array(
                    'required' => true,
                    'label' => 'Ingrese Número de Documento',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\BackBundle\Entity\Documento',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'cmsga_backbundle_documento';
    }

}