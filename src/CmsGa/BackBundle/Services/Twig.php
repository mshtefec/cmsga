<?php

namespace CmsGa\BackBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Twig Extension.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class Twig extends \Twig_Extension {

    protected $container;

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    /* Metodo para llar a Filtro Personalizado
      public function getFilters()
      {

      return array(
      new \Twig_SimpleFilter('img', array($this, 'imgFilter')),
      );
      } */

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('isActive', array($this, 'isActive'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('showCarouselInicio', array($this, 'showCarouselInicio'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('showCarousel', array($this, 'showCarousel'), array('is_safe' => array('html'))),
            // 'showImage' => new \Twig_Function_Method($this, 'showImage', array(
            //     'is_safe' => array('html')
            // )),
            // 'showImageFilter' => new \Twig_Function_Method($this, 'showImageFilter', array(
            //     'is_safe' => array('html')
            // )),
            // 'facebookShareButton' => new \Twig_Function_Method($this, 'facebookShareButton', array(
            //     'is_safe' => array('html')
            // )),
        );
    }

    public function isActive($active, $title_true = null, $title_false = null) {
        if ($active) {
            if ($title_true) {
                $res = '<i class="icon-ok-circle tooltips" rel="tooltip" title="' . $title_true . '"></i>';
            } else {
                $res = '<i class="icon-ok-circle"></i>';
            }
        } else {
            if ($title_false) {
                $res = '<i class="icon-ban-circle tooltips" rel="tooltip" title="' . $title_false . '"></i>';
            } else {
                $res = '<i class="icon-ban-circle"></i>';
            }
        }

        return $res;
    }

    public function showCarousel($img) {
        if ($img->getFilePath()) {
            $path = $img->getWebPath();
            $src = $this->container->get('templating.helper.assets')->getUrl("$path");
            $avalancheService = $this->container->get('imagine.cache.path.resolver');
            $src = $avalancheService->getBrowserPath($src, 'noticias_carousel');

            return '<img src="' . $src . '" />';
        }

        return;
    }

    public function showCarouselInicio($img) {
        if ($img->getFilePath()) {
            $path = $img->getWebPath();
            $src = $this->container->get('templating.helper.assets')->getUrl("$path");
            $avalancheService = $this->container->get('imagine.cache.path.resolver');
            $src = $avalancheService->getBrowserPath($src, 'imagen_alto300');

            return '<img src="' . $src . '" />';
        }

        return;
    }

    public function getTests() {
        return [
            'noticia' => new \Twig_Test_Method($this, 'isNoticia'),
            'seccion' => new \Twig_Test_Method($this, 'isSeccion'),
            'evento' => new \Twig_Test_Method($this, 'isEvento'),
            'curso' => new \Twig_Test_Method($this, 'isCurso'),
            'actividad' => new \Twig_Test_Method($this, 'isActividad'),
        ];
    }

    public function isEvento($evento) {
        return ($evento instanceof \CmsGa\CalendarioBundle\Entity\Evento);
    }

    public function isCurso($curso) {
        return ($curso instanceof \CmsGa\CalendarioBundle\Entity\Curso);
    }

    public function isActividad($actividad) {
        return ($actividad instanceof \CmsGa\CalendarioBundle\Entity\Actividad);
    }

    public function isNoticia($noticia) {
        return ($noticia instanceof \CmsGa\BackBundle\Entity\Noticia);
    }

    public function isSeccion($seccion) {
        return ($seccion instanceof \CmsGa\BackBundle\Entity\Seccion);
    }

    // function showImage($img, $size = null, $options = null) {
    //     if ($img['path']) {
    //         $dir = $img['dir'];
    //         $path = $img['path'];
    //         $src = $this->container->get('templating.helper.assets')->getUrl("$dir/$path");
    //         if ($size) {
    //             $sizes = array(
    //                 "miniatura",
    //                 "alto100", "alto200", "alto300", "alto400",
    //                 "ancho100", "ancho200", "ancho300", "ancho400"
    //             );
    //             if (in_array($size, $sizes)) {
    //                 $avalancheService = $this->container->get('imagine.cache.path.resolver');
    //                 $src = $avalancheService->getBrowserPath($src, $size);
    //             }
    //         }
    //         return '<img src="'.$src.'" '.$options.'/>';
    //     }
    //     return null;
    // }
    // function showImageFilter($img, $height = null, $width = null) {
    //     $dir = $img->getDir();
    //     $path = $img->getPath();
    //     $src = $this->container->get('templating.helper.assets')->getUrl("$dir/$path");
    //     $avalancheService = $this->container->get('imagine.cache.path.resolver');
    //     $cachedImage = $avalancheService->getBrowserPath($src, 'my_thumb');
    //     if ($height) {
    //         $height = 'height:'.$height.'px;';
    //     }
    //     if ($width) {
    //         $width = 'width:'.$width.'px;';
    //     }
    //     return '<img src="'.$cachedImage.'" style="'.$height.$width.'"/>';
    // }
    // function facebookShareButton($pagina, $item, $tema = null, $img = null) {
    //     $src = $this->container->get('templating.helper.assets')->getUrl("bundles/sistemamws/img/compartirfb.png");
    //     //$src = $this->container->get('templating.helper.assets')->getUrl("templates/$tema/assets/images/compartirfb.png");
    //     if ($pagina['servicio']['activo']) {
    //         $res = '<a href="http://www.facebook.com/sharer.php?u=http://www.'.$pagina["dominio"].$pagina["extension"].'/ver/'.$item["id"].'" target="_blank">
    //             <img src="'.$src.'" />
    //         </a>';
    //     } else {
    //         $dominio_base = $this->container->getParameter('dominio');
    //         $res = '<a href="http://www.facebook.com/sharer.php?u=http://www.'.$pagina["subdominio"].'.'.$dominio_base.'/ver/'.$item["id"].'" target="_blank">
    //             <img src="'.$src.'" />
    //         </a>';
    //     }
    //     return $res;
    // }

    /* Llamada a Filtro {{ dir|img(path)}}
      public function imgFilter($dir,$path)
      {
      $assets = $this->container->get('templating.helper.assets')->getUrl("$dir/$path");
      $img = "src=".$assets;

      return $img;
      } */

    public function getName() {
        return 'cmsgaback.extension';
    }

}
