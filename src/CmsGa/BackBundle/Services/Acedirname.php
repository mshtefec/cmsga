<?php

namespace CmsGa\BackBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class Acedirname
{
    protected $container;

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getDirname()
    {
        return $this->container->getParameter('directorio.name');
    }

    public function getName()
    {
        return 'ace.dirname';
    }
}
