// Get the ul that holds the collection of escuelas
var collectionHolder = jQuery('.mplinks');

jQuery(document).ready(function () {
    collectionHolder.data('index', collectionHolder.find(':input').length);

    jQuery('.mplinks').delegate('.btnRemoveMpLink', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.row').remove();
    });

    jQuery('.add_mplink').click(function (e) {

        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        index = addForm(collectionHolder, jQuery('.mplinks'));

    });

});