collectionFormacionHolder = jQuery('.formacion-academica');
collectionFormacionHolder.data('index', collectionFormacionHolder.find(':input').length)
jQuery('.add-formacion-academinca-form').click(function(e) {
    e.preventDefault();
    addForm(collectionFormacionHolder, jQuery(this).parent().find('.formacion-academica'));
    bindFormacionSelect();
})
jQuery('.formacion-academica').delegate('.delete-form','click', function(e) {
    deleteRow(jQuery(this).parent()); 
});
function bindFormacionSelect() {
    jQuery('.nivelFormacionSelector').on('change', function(e){
        isIncompleto = (jQuery(this).val().match(/INCOMPLETO/g) != null);
        if (isIncompleto) {
            jQuery(this).parent().parent().parent().parent().find('.incompleto').show() 
            jQuery(this).parent().parent().parent().parent().find('.completo').hide() 
        } else {
            jQuery(this).parent().parent().parent().parent().find('.incompleto').hide() 
            jQuery(this).parent().parent().parent().parent().find('.completo').show() 
        }
    })
}
