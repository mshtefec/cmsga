/**
 * Funcion ajax on selectd
 *
 *  @param string  url         url
 *  @param string  type        type post, get, etc..
 *  @param string  classSelect class from DOM
 *  @param integer id          id from object
 *
 */
function ajaxOnSelected(url, type, classSelect, id) {
    $.ajax({
        url: url,
        type: type,                                                                
        data: {id : id},
        success:
            function(data) {
                console.log(data);
                jQuery('.'+classSelect+' option').each(function () {
                        id=jQuery(this).val();
                        if (id !='') {
                            
                            jQuery("."+classSelect+" option[value='"+id+"']").remove();
                        }
                    });
                $.each(data, function(i, value) {
                    jQuery('.'+classSelect).append($('<option>', {value: value.id, text: value.nombre}));
                });
            }
    }); 
}
