// Get the ul that holds the collection of escuelas
var collectionPage = jQuery('.pages');
var index;

jQuery(document).ready(function () {

    collectionPage.data('index', collectionPage.find(':input').length);

    jQuery('.pages').delegate('.btnRemovePage', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });

    jQuery('.add_pageFace_link').click(function (e) {

        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionPage, jQuery('.pages'));

    });
});