    $tituloSeccion = jQuery('.tituloSeccion')
    $urlSeccion = jQuery('.urlNombre');
    //$urlSeccion.slugify('.tituloSeccion');
    jQuery($tituloSeccion).focusout(function(){
        validarUrl($urlSeccion);
    });
    jQuery($urlSeccion).focusout(function(){
        $urlSeccion.val($urlSeccion.val().replace(/ /g,"-"))
        validarUrl($urlSeccion);
    });
    function validarUrl($urlSeccion) {
    url = Routing.generate('validar_nombre_url');
        msgContentError = "<div class='alert alert-error'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>La asiganción para la URL</strong> existe</div>";
        msgContentSuccess = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button><strong>Perfecto</strong> URL no existe</div>";
        msgResponse = jQuery($urlSeccion).parent().parent().find('.msg');
        msg = $.ajax({
            url: url,      
            type: 'POST',
            dataType: 'text',
            data: { urlSeccion: $urlSeccion.val() },
            async: false,
            success: function(response) {
                if ('ok'== response) {
                    msg = msgContentSuccess;
                } else {
                    msg = msgContentError;
                }
                    jQuery(msgResponse).html(msg);
                return response;
            }
        });

    }

