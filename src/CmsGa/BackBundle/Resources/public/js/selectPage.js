$(".selectPage").select2({
            placeholder: "Elegir pagina de Facebook...",
            minimumInputLength: 0,
            width: '100%',
            // instead of writing the function to execute the request we use Select2's convenient helper
            ajax: {
                url: Routing.generate('ConfigFace_autocomplete_pages'),
                dataType: "json",
                quietMillis: 250,
                data: function (term, page) {
                    return {
                        // search term
                        q: term
                    };
                },
                results: function (data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                    return {results: data};
                },
                cache: true
            },
            // we do not want to escape markup since we are displaying html in results
            escapeMarkup: function (m) {
                return m;
            }
        });