// Get the ul that holds the collection of escuelas
var collectionPageFace = jQuery('.PageFace');
var index;

jQuery(document).ready(function () {

    collectionPageFace.data('index', collectionPageFace.find(':input').length);

    jQuery('.PageFace').delegate('.btnRemovePageFace', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.row').remove();
    });

    jQuery('.add_PageFace_link').click(function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionPageFace, jQuery('.PageFace'));

    });
});