// Get the ul that holds the collection of escuelas
var collectionSeccion = jQuery('.secciones');
var index;

jQuery(document).ready(function () {

    collectionSeccion.data('index', collectionSeccion.find(':input').length);

    jQuery('.secciones').delegate('.btnRemoveSeccion', 'click', function (e) {
        // prevent the link from creating a "#" on  the URL
        e.preventDefault();
        // remove the li for the tag form
        jQuery(this).closest('.rowremove').remove();
    });

    jQuery('.ribon_dom').delegate('.add_seccion_link', 'click', function (e) {
        // prevent the link from creating a "#" on the URL                
        e.preventDefault();
        // remove the li for the tag form
        addForm(collectionSeccion, jQuery('.secciones'));

    });
});