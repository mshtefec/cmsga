<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video.
 *
 * @ORM\Table(name="video_noticia")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class VideoNoticia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigoIframe", type="text")
     */
    private $codigoIframe;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Noticia", inversedBy="videos")
     * @ORM\JoinColumn(name="noticia_id", referencedColumnName="id")
     */
    private $noticia;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoIframe.
     *
     * @param string $codigoIframe
     *
     * @return Video
     */
    public function setCodigoIframe($codigoIframe)
    {
        $this->codigoIframe = $codigoIframe;

        return $this;
    }

    /**
     * Get codigoIframe.
     *
     * @return string
     */
    public function getCodigoIframe()
    {
        return $this->codigoIframe;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Video
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set noticia.
     *
     * @param \CmsGa\BackBundle\Entity\Noticia $noticia
     *
     * @return VideoNoticia
     */
    public function setNoticia(\CmsGa\BackBundle\Entity\Noticia $noticia = null)
    {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia.
     *
     * @return \CmsGa\BackBundle\Entity\Noticia
     */
    public function getNoticia()
    {
        return $this->noticia;
    }
}
