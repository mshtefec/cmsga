<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Documento.
 *
 * @ORM\Table(name="documento")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\DocumentoRepository")
 */
class Documento {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $dni;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dni.
     *
     * @param string $dni
     *
     * @return Documento
     */
    public function setDni($dni) {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni.
     *
     * @return string
     */
    public function getDni() {
        return $this->dni;
    }

}