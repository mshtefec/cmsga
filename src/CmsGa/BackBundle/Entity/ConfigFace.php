<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ConfigFace
 *
 * @ORM\Table(name="config_face")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\ConfigFaceRepository")
 */
class ConfigFace {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="appId", type="string", length=255)
     */
    private $appId;

    /**
     * @var string
     *
     * @ORM\Column(name="secret", type="string", length=255)
     */
    private $secret;

    /**
     * @ORM\OneToMany(targetEntity="PageFace", mappedBy="configFace", cascade={"all"})
     * */
    private $pages;


    /**
     * Constructor
     */
    public function __construct() {
        $this->pages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ConfigFace
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set appId
     *
     * @param string $appId
     * @return ConfigFace
     */
    public function setAppId($appId) {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId
     *
     * @return string 
     */
    public function getAppId() {
        return $this->appId;
    }

    /**
     * Set secret
     *
     * @param string $secret
     * @return ConfigFace
     */
    public function setSecret($secret) {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret() {
        return $this->secret;
    }

    /**
     * Add pages
     *
     * @param \CmsGa\BackBundle\Entity\PageFace $pages
     * @return ConfigFace
     */
    public function addPage(\CmsGa\BackBundle\Entity\PageFace $pages) {
        $pages->setConfigFace($this);
        $this->pages[] = $pages;

        return $this;
    }

    /**
     * Remove pages
     *
     * @param \CmsGa\BackBundle\Entity\PageFace $pages
     */
    public function removePage(\CmsGa\BackBundle\Entity\PageFace $pages) {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages() {
        return $this->pages;
    }
}