<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CmsGa\toolsBundle\Entity\BaseFile;

/**
 * Imagen.
 *
 * @ORM\Table(name="imagen")
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminador", type="string")
 * @ORM\DiscriminatorMap({
 * "imagen" = "Imagen",
 * "imagen_seccion" = "ImagenSeccion",
 * "imagen_noticia" = "ImagenNoticia",
 * "imagen_evento" = "CmsGa\CalendarioBundle\Entity\ImagenEvento",
 * })
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class Imagen extends BaseFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUploadRootDirBaseFile()
    {
        return $this->getUploadRootDir();
    }
}
