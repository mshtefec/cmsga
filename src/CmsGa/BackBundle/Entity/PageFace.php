<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PageFace
 *
 * @ORM\Table(name="page_face")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\PageFaceRepository")
 */
class PageFace {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="idPage", type="string", length=255)
     */
    private $idPage;

    /**
     * @ORM\ManyToOne(targetEntity="ConfigFace", inversedBy="pages")
     * @ORM\JoinColumn(name="configFace_id", referencedColumnName="id")
     */
    private $configFace;

    /**
     * @ORM\OneToMany(targetEntity="UserPageFace", mappedBy="pageFace")
     */
    private $users;


    /**
     * Constructor
     */
    public function __construct() {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PageFace
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set idPage
     *
     * @param string $idPage
     * @return PageFace
     */
    public function setIdPage($idPage) {
        $this->idPage = $idPage;

        return $this;
    }

    /**
     * Get idPage
     *
     * @return string 
     */
    public function getIdPage() {
        return $this->idPage;
    }

    /**
     * Set configFace
     *
     * @param \CmsGa\BackBundle\Entity\ConfigFace $configFace
     * @return PageFace
     */
    public function setConfigFace(\CmsGa\BackBundle\Entity\ConfigFace $configFace = null) {
        $this->configFace = $configFace;

        return $this;
    }

    /**
     * Get configFace
     *
     * @return \CmsGa\BackBundle\Entity\ConfigFace 
     */
    public function getConfigFace() {
        return $this->configFace;
    }

    /**
     * Add users
     *
     * @param \CmsGa\BackBundle\Entity\UserPageFace $users
     * @return PageFace
     */
    public function addUser(\CmsGa\BackBundle\Entity\UserPageFace $users) {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \CmsGa\BackBundle\Entity\UserPageFace $users
     */
    public function removeUser(\CmsGa\BackBundle\Entity\UserPageFace $users) {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers() {
        return $this->users;
    }
}