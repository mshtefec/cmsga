<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Plantilla.
 *
 * @ORM\Table(name="plantilla")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\PlantillaRepository")
 */
class Plantilla
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Seccion", mappedBy="plantilla")
     **/
    private $secciones;

    public function __toString()
    {
        return $this->getDescripcion();
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->secciones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre.
     *
     * @param string $nombre
     *
     * @return Plantilla
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set activo.
     *
     * @param bool $activo
     *
     * @return Plantilla
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo.
     *
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Plantilla
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add secciones.
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $secciones
     *
     * @return Plantilla
     */
    public function addSeccione(\CmsGa\BackBundle\Entity\Seccion $secciones)
    {
        $secciones->setPlantilla($this);
        $this->secciones[] = $secciones;

        return $this;
    }

    /**
     * Remove secciones.
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $secciones
     */
    public function removeSeccione(\CmsGa\BackBundle\Entity\Seccion $secciones)
    {
        $this->secciones->removeElement($secciones);
    }

    /**
     * Get secciones.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSecciones()
    {
        return $this->secciones;
    }
}
