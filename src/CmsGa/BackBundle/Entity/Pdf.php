<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CmsGa\toolsBundle\Entity\BaseFile;

/**
 * Imagen.
 *
 * @ORM\Table(name="pdf")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class Pdf extends BaseFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUploadDir()
    {

        /* if ($this->dirname) {
          $this->uploadDir = 'uploads/' . $this->dirname . '/publicacion';
          $res = 'uploads/' . $this->dirname . '/publicacion';
          } elseif ($this->id) {
          $res = 'uploads/' . $this->uploadDir . '/publicacion';
          } else {
          $res = 'uploads/publicacion';
          } */

        return 'uploads/'.$this->uploadDir.'/publicacion';
    }
}
