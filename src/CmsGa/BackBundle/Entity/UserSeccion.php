<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Video.
 *
 * @ORM\Table(name="user_seccion")
 * @ORM\Entity
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class UserSeccion implements \CmsGa\BackBundle\Filter\userSeccionInterface{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="secciones")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Seccion", inversedBy="usuarios")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id")
     */
    private $seccion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \CmsGa\BackBundle\Entity\User $user
     * @return UserSeccion
     */
    public function setUser(\CmsGa\BackBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CmsGa\BackBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set seccion
     *
     * @param \CmsGa\BackBundle\Entity\Seccion $seccion
     * @return UserSeccion
     */
    public function setSeccion(\CmsGa\BackBundle\Entity\Seccion $seccion = null)
    {
        $this->seccion = $seccion;

        return $this;
    }

    /**
     * Get seccion
     *
     * @return \CmsGa\BackBundle\Entity\Seccion 
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

   

}
