<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CmsGa\toolsBundle\Entity\BaseFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ImagenPortada.
 *
 * @ORM\Table(name="imagen_portada")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\ImagenPortadaRepository")
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ImagenPortada extends BaseFile {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\OneToOne(targetEntity="Noticia", mappedBy="imagenPortada")
     * */
    private $noticia;

    /**
     * @ORM\OneToOne(targetEntity="CmsGa\CalendarioBundle\Entity\Calendario", mappedBy="imagenPortada")
     * */
    private $calendario;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * @var publicado
     *
     * @ORM\Column(name="publicado", type="boolean", nullable=true)
     */
    private $publicado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finPublicacion", type="datetime", nullable=true)
     */
    private $finPublicacion;

    public function __construct() {
        $this->setFinPublicacion(new \DateTime('now'));
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @Assert\True(message = "Campo orden de Imagen Portada vacio")
     */
    public function isOrdenNotBlank() {
        $res = true;
        if (is_null($this->getFile())) {
            $res = true;
        } elseif (empty($this->orden)) {
            $res = false;
        }

        return $res;
    }

    public function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        /* if ($this->dirname) {
          $this->uploadDir = 'uploads/' . $this->dirname . '/imagen_portada';
          $res = 'uploads/' . $this->dirname . '/imagen_portada';
          } elseif ($this->id) {
          $res = 'uploads/' . $this->uploadDir . '/imagen_portada';
          } else {
          $res = 'uploads/imagen_portada';
          } */

        return 'uploads/' . $this->uploadDir . '/imagen_portada';
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return ImagenPortada
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set noticia.
     *
     * @param \CmsGa\BackBundle\Entity\Noticia $noticia
     *
     * @return ImagenPortada
     */
    public function setNoticia(\CmsGa\BackBundle\Entity\Noticia $noticia = null) {
        $this->noticia = $noticia;

        return $this;
    }

    /**
     * Get noticia.
     *
     * @return \CmsGa\BackBundle\Entity\Noticia
     */
    public function getNoticia() {
        return $this->noticia;
    }

    /**
     * Set calendario.
     *
     * @param \CmsGa\CalendarioBundle\Entity\Calendario $calendario
     *
     * @return ImagenPortada
     */
    public function setCalendario(\CmsGa\CalendarioBundle\Entity\Calendario $calendario = null) {
        $this->calendario = $calendario;

        return $this;
    }

    /**
     * Get calendario.
     *
     * @return \CmsGa\CalendarioBundle\Entity\Calendario
     */
    public function getCalendario() {
        return $this->calendario;
    }

    /**
     * Set orden.
     *
     * @param int $orden
     *
     * @return ImagenPortada
     */
    public function setOrden($orden) {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden.
     *
     * @return int
     */
    public function getOrden() {
        return $this->orden;
    }

    /**
     * Set publicado.
     *
     * @param bool $publicado
     *
     * @return ImagenPortada
     */
    public function setPublicado($publicado) {
        $this->publicado = $publicado;

        return $this;
    }

    /**
     * Get publicado.
     *
     * @return bool
     */
    public function getPublicado() {
        return $this->publicado;
    }

    /**
     * Set finPublicacion
     *
     * @param \DateTime $finPublicacion
     * @return ImagenPortada
     */
    public function setFinPublicacion($finPublicacion) {
        $this->finPublicacion = $finPublicacion;

        return $this;
    }

    /**
     * Get finPublicacion
     *
     * @return \DateTime 
     */
    public function getFinPublicacion() {
        return $this->finPublicacion;
    }

}
