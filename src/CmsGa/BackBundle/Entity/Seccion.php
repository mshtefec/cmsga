<?php

namespace CmsGa\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Seccion.
 *
 * @ORM\Table(name="seccion")
 * @ORM\Entity(repositoryClass="CmsGa\BackBundle\Entity\SeccionRepository")
 * @UniqueEntity("url")
 */
class Seccion {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, unique=true)
     */
    private $url;

    /**
     * @var bool
     *
     * @ORM\Column(name="url_externa", type="boolean", nullable=true)
     */
    private $urlExterna;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text", nullable=true)
     */
    private $contenido;

    /**
     * @ORM\OneToMany(targetEntity="ImagenSeccion", mappedBy="seccion", cascade={"all"}, orphanRemoval=true)
     */
    protected $imagenes;

    /**
     * @ORM\OneToMany(targetEntity="Noticia", mappedBy="seccion", cascade={"all"}, orphanRemoval=true)
     */
    protected $noticias;

    /**
     * @ORM\OneToOne(targetEntity="MWSimple\DynamicMenuBundle\Entity\Item", inversedBy="seccion", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\OneToMany(targetEntity="UserSeccion", mappedBy="seccion")
     * */
    private $usuarios;

    /**
     * @var publicado
     *
     * @ORM\Column(name="publicado", type="boolean", nullable=true)
     */
    private $publicado;

    /**
     * @var portada
     *
     * @ORM\Column(name="portada", type="boolean", nullable=true)
     */
    private $portada;

    /**
     * @var permiteNoticias
     *
     * @ORM\Column(name="permitenoticias", type="boolean", nullable=true)
     */
    private $permiteNoticias;

    /**
     * @ORM\ManyToOne(targetEntity = "Plantilla", inversedBy = "secciones")
     * @ORM\JoinColumn(name = "plantilla_id", referencedColumnName = "id", nullable=false)
     */
    private $plantilla;

    /**
     *  @ORM\OneToOne(targetEntity="CmsGa\RepositoryFilesBundle\Entity\Archivo", cascade={"all"}, orphanRemoval=true)
     *  @ORM\JoinColumn(name="archivo_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $archivo;

    /**
     * @var int
     *
     * @ORM\Column(name="ordenListado", type="integer")
     */
    private $ordenListado;

    /**
     * @ORM\ManyToMany(targetEntity="CmsGa\RepositoryFilesBundle\Entity\Categoria", inversedBy="secciones")
     * @ORM\JoinTable(name="secciones_categorias")
     * */
    private $categorias;

    /**
     * @var string
     *
     * @ORM\Column(name="colorbtn", type="string", length=255, nullable=true)
     */
    private $colorbtn;

    /**
     * @var string
     *
     * @ORM\Column(name="iconbtn", type="string", length=255, nullable=true)
     */
    private $iconbtn;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->imagenes = new ArrayCollection();
        $this->noticias = new ArrayCollection();
        $this->usuarios = new ArrayCollection();
    }

    public function __toString() {
        return $this->getItem()->getName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set publicado.
     *
     * @param bool $publicado
     *
     * @return Seccion
     */
    public function setPublicado($publicado) {
        $this->publicado = $publicado;

        return $this;
    }

    /**
     * Get publicado.
     *
     * @return bool
     */
    public function getPublicado() {
        return $this->publicado;
    }

    /**
     * Set portada.
     *
     * @param bool $portada
     *
     * @return Seccion
     */
    public function setPortada($portada) {
        $this->portada = $portada;

        return $this;
    }

    /**
     * Get portada.
     *
     * @return bool
     */
    public function getPortada() {
        return $this->portada;
    }

    /**
     * Set portada.
     *
     * @param bool $permiteNoticias
     *
     * @return Seccion
     */
    public function setPermiteNoticias($permiteNoticias) {
        $this->permiteNoticias = $permiteNoticias;

        return $this;
    }

    /**
     * Get permiteNoticias.
     *
     * @return bool
     */
    public function getPermiteNoticias() {
        return $this->permiteNoticias;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Seccion
     */
    public function setUrl($url) {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Set urlExterna.
     *
     * @param bool $urlExterna
     *
     * @return Seccion
     */
    public function setUrlExterna($urlExterna) {
        $this->urlExterna = $urlExterna;

        return $this;
    }

    /**
     * Get urlExterna.
     *
     * @return bool
     */
    public function getUrlExterna() {
        return $this->urlExterna;
    }

    /**
     * Set contenido.
     *
     * @param string $contenido
     *
     * @return Seccion
     */
    public function setContenido($contenido) {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido.
     *
     * @return string
     */
    public function getContenido() {
        return $this->contenido;
    }

    /**
     * Add imagenes.
     *
     * @param \CmsGa\BackBundle\Entity\ImagenSeccion $imagenes
     *
     * @return Seccion
     */
    public function addImagene(\CmsGa\BackBundle\Entity\ImagenSeccion $imagenes) {
        $imagenes->setSeccion($this);
        $this->imagenes[] = $imagenes;

        return $this;
    }

    /**
     * Remove imagenes.
     *
     * @param \CmsGa\BackBundle\Entity\ImagenSeccion $imagenes
     */
    public function removeImagene(\CmsGa\BackBundle\Entity\ImagenSeccion $imagenes) {
        $this->imagenes->removeElement($imagenes);
    }

    /**
     * Get imagenes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes() {
        return $this->imagenes;
    }

    /**
     * Add noticias.
     *
     * @param \CmsGa\BackBundle\Entity\Noticia $noticias
     *
     * @return Seccion
     */
    public function addNoticia(\CmsGa\BackBundle\Entity\Noticia $noticias) {
        $this->noticias[] = $noticias;

        return $this;
    }

    /**
     * Remove noticias.
     *
     * @param \CmsGa\BackBundle\Entity\Noticia $noticias
     */
    public function removeNoticia(\CmsGa\BackBundle\Entity\Noticia $noticias) {
        $this->noticias->removeElement($noticias);
    }

    /**
     * Get noticias.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoticias() {
        return $this->noticias;
    }

    /**
     * Set item.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Item $item
     *
     * @return Seccion
     */
    public function setItem(\MWSimple\DynamicMenuBundle\Entity\Item $item = null) {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \MWSimple\DynamicMenuBundle\Entity\Item
     */
    public function getItem() {
        return $this->item;
    }

    /**
     * Set plantilla.
     *
     * @param \CmsGa\BackBundle\Entity\Plantilla $plantilla
     *
     * @return Seccion
     */
    public function setPlantilla(\CmsGa\BackBundle\Entity\Plantilla $plantilla = null) {
        $this->plantilla = $plantilla;

        return $this;
    }

    /**
     * Get plantilla.
     *
     * @return \CmsGa\BackBundle\Entity\Plantilla
     */
    public function getPlantilla() {
        return $this->plantilla;
    }

    /**
     * Set archivo.
     *
     * @param \CmsGa\RepositoryFilesBundle\Entity\Archivo $archivo
     *
     * @return Seccion
     */
    public function setArchivo(\CmsGa\RepositoryFilesBundle\Entity\Archivo $archivo = null) {
        $this->archivo = $archivo;

        return $this;
    }

    /**
     * Get archivo.
     *
     * @return \CmsGa\RepositoryFilesBundle\Entity\Archivo
     */
    public function getArchivo() {
        return $this->archivo;
    }

    /**
     * Set ordenListado.
     *
     * @param int $ordenListado
     *
     * @return Seccion
     */
    public function setOrdenListado($ordenListado) {
        $this->ordenListado = $ordenListado;

        return $this;
    }

    /**
     * Get ordenListado.
     *
     * @return int
     */
    public function getOrdenListado() {
        return $this->ordenListado;
    }

    /**
     * Add usuarios
     *
     * @param \CmsGa\BackBundle\Entity\UserSeccion $usuarios
     * @return Seccion
     */
    public function addUsuario(\CmsGa\BackBundle\Entity\UserSeccion $usuarios) {
        $this->usuarios[] = $usuarios;

        return $this;
    }

    /**
     * Remove usuarios
     *
     * @param \CmsGa\BackBundle\Entity\UserSeccion $usuarios
     */
    public function removeUsuario(\CmsGa\BackBundle\Entity\UserSeccion $usuarios) {
        $this->usuarios->removeElement($usuarios);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuarios() {
        return $this->usuarios;
    }


    /**
     * Add categorias
     *
     * @param \CmsGa\RepositoryFilesBundle\Entity\Categoria $categorias
     * @return Seccion
     */
    public function addCategoria(\CmsGa\RepositoryFilesBundle\Entity\Categoria $categorias)
    {
        $this->categorias[] = $categorias;

        return $this;
    }

    /**
     * Remove categorias
     *
     * @param \CmsGa\RepositoryFilesBundle\Entity\Categoria $categorias
     */
    public function removeCategoria(\CmsGa\RepositoryFilesBundle\Entity\Categoria $categorias)
    {
        $this->categorias->removeElement($categorias);
    }

    /**
     * Get categorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Set colorbtn.
     *
     * @param string $colorbtn
     *
     * @return Seccion
     */
    public function setColorbtn($colorbtn) {
        $this->colorbtn = $colorbtn;

        return $this;
    }

    /**
     * Get colorbtn.
     *
     * @return string
     */
    public function getColorbtn() {
        return $this->colorbtn;
    }

    /**
     * Set iconbtn.
     *
     * @param string $iconbtn
     *
     * @return Seccion
     */
    public function setIconbtn($iconbtn) {
        $this->iconbtn = $iconbtn;

        return $this;
    }

    /**
     * Get iconbtn.
     *
     * @return string
     */
    public function getIconbtn() {
        return $this->iconbtn;
    }
}
