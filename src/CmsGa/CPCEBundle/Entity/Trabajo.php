<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trabajo
 *
 * @ORM\Table(name="trabajo", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_FDD6B80AA061F7B2", columns={"tra_nroasi"}), @ORM\UniqueConstraint(name="UNIQ_FDD6B80A5F018FF7", columns={"tra_nrolegali"}), @ORM\UniqueConstraint(name="UNIQ_FDD6B80AAC21B1F1", columns={"tra_nroasi_liquidacion"})}, indexes={@ORM\Index(name="IDX_FDD6B80A2ADCBD5E", columns={"tra_user_id"}), @ORM\Index(name="IDX_FDD6B80A616076B7", columns={"tra_tarea_id"}), @ORM\Index(name="IDX_FDD6B80A9C3EDFFC", columns={"tra_estado"})})
 * @ORM\Entity
 */
class Trabajo
{
    /**
     * @var string
     *
     * @ORM\Column(name="tra_clientecomitente", type="string", length=100, nullable=false)
     */
    private $traClientecomitente;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_comitentecuit", type="string", length=15, nullable=false)
     */
    private $traComitentecuit;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_comitentedomicilio", type="string", length=100, nullable=false)
     */
    private $traComitentedomicilio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tra_fecha_informe", type="date", nullable=false)
     */
    private $traFechaInforme;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_profesional", type="string", length=100, nullable=false)
     */
    private $traProfesional;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_matricula", type="string", length=10, nullable=false)
     */
    private $traMatricula;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_domicilio", type="string", length=50, nullable=false)
     */
    private $traDomicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_telefono", type="string", length=50, nullable=false)
     */
    private $traTelefono;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_celular", type="string", length=50, nullable=false)
     */
    private $traCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_correo", type="string", length=50, nullable=false)
     */
    private $traCorreo;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_importe1", type="float", precision=10, scale=0, nullable=false)
     */
    private $traImporte1;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_importe2", type="float", precision=10, scale=0, nullable=false)
     */
    private $traImporte2;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_honorariominimo", type="float", precision=10, scale=0, nullable=false)
     */
    private $traHonorariominimo;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_monto", type="float", precision=10, scale=0, nullable=false)
     */
    private $traMonto;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_condicioniva", type="string", length=2, nullable=false)
     */
    private $traCondicioniva;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_cuit", type="string", length=15, nullable=false)
     */
    private $traCuit;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_cantidad", type="integer", nullable=false)
     */
    private $traCantidad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tra_fecha_cierre", type="date", nullable=false)
     */
    private $traFechaCierre;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_auditoria_tipo", type="string", length=3, nullable=true)
     */
    private $traAuditoriaTipo;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_monto_iva", type="float", precision=10, scale=0, nullable=true)
     */
    private $traMontoIva;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_monto_aporte", type="float", precision=10, scale=0, nullable=true)
     */
    private $traMontoAporte;

    /**
     * @var float
     *
     * @ORM\Column(name="tra_monto_deposito", type="float", precision=10, scale=0, nullable=false)
     */
    private $traMontoDeposito;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_nroasi", type="bigint", nullable=true)
     */
    private $traNroasi;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_nrolegali", type="integer", nullable=true)
     */
    private $traNrolegali;

    /**
     * @var integer
     *
     * @ORM\Column(name="tra_nroasi_liquidacion", type="bigint", nullable=true)
     */
    private $traNroasiLiquidacion;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_certificado", type="string", length=1, nullable=true)
     */
    private $traCertificado;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_esauditor", type="string", length=2, nullable=true)
     */
    private $traEsauditor;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_meses", type="string", length=2, nullable=true)
     */
    private $traMeses;

    /**
     * @var string
     *
     * @ORM\Column(name="tra_porcentaje_sindico", type="decimal", precision=5, scale=2, nullable=true)
     */
    private $traPorcentajeSindico = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="tra_importe_periodo", type="float", precision=10, scale=0, nullable=true)
     */
    private $traImportePeriodo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CmsGa\CPCEBundle\Entity\TrabajoEstado
     *
     * @ORM\ManyToOne(targetEntity="CmsGa\CPCEBundle\Entity\TrabajoEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tra_estado", referencedColumnName="id")
     * })
     */
    private $traEstado;

    /**
     * @var \CmsGa\CPCEBundle\Entity\Tareas
     *
     * @ORM\ManyToOne(targetEntity="CmsGa\CPCEBundle\Entity\Tareas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tra_tarea_id", referencedColumnName="tar_codigo")
     * })
     */
    private $traTarea;

    /**
     * @var \CmsGa\CPCEBundle\Entity\FosUser
     *
     * @ORM\ManyToOne(targetEntity="CmsGa\CPCEBundle\Entity\FosUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tra_user_id", referencedColumnName="id")
     * })
     */
    private $traUser;



    /**
     * Set traClientecomitente
     *
     * @param string $traClientecomitente
     *
     * @return Trabajo
     */
    public function setTraClientecomitente($traClientecomitente)
    {
        $this->traClientecomitente = $traClientecomitente;

        return $this;
    }

    /**
     * Get traClientecomitente
     *
     * @return string
     */
    public function getTraClientecomitente()
    {
        return $this->traClientecomitente;
    }

    /**
     * Set traComitentecuit
     *
     * @param string $traComitentecuit
     *
     * @return Trabajo
     */
    public function setTraComitentecuit($traComitentecuit)
    {
        $this->traComitentecuit = $traComitentecuit;

        return $this;
    }

    /**
     * Get traComitentecuit
     *
     * @return string
     */
    public function getTraComitentecuit()
    {
        return $this->traComitentecuit;
    }

    /**
     * Set traComitentedomicilio
     *
     * @param string $traComitentedomicilio
     *
     * @return Trabajo
     */
    public function setTraComitentedomicilio($traComitentedomicilio)
    {
        $this->traComitentedomicilio = $traComitentedomicilio;

        return $this;
    }

    /**
     * Get traComitentedomicilio
     *
     * @return string
     */
    public function getTraComitentedomicilio()
    {
        return $this->traComitentedomicilio;
    }

    /**
     * Set traFechaInforme
     *
     * @param \DateTime $traFechaInforme
     *
     * @return Trabajo
     */
    public function setTraFechaInforme($traFechaInforme)
    {
        $this->traFechaInforme = $traFechaInforme;

        return $this;
    }

    /**
     * Get traFechaInforme
     *
     * @return \DateTime
     */
    public function getTraFechaInforme()
    {
        return $this->traFechaInforme;
    }

    /**
     * Set traProfesional
     *
     * @param string $traProfesional
     *
     * @return Trabajo
     */
    public function setTraProfesional($traProfesional)
    {
        $this->traProfesional = $traProfesional;

        return $this;
    }

    /**
     * Get traProfesional
     *
     * @return string
     */
    public function getTraProfesional()
    {
        return $this->traProfesional;
    }

    /**
     * Set traMatricula
     *
     * @param string $traMatricula
     *
     * @return Trabajo
     */
    public function setTraMatricula($traMatricula)
    {
        $this->traMatricula = $traMatricula;

        return $this;
    }

    /**
     * Get traMatricula
     *
     * @return string
     */
    public function getTraMatricula()
    {
        return $this->traMatricula;
    }

    /**
     * Set traDomicilio
     *
     * @param string $traDomicilio
     *
     * @return Trabajo
     */
    public function setTraDomicilio($traDomicilio)
    {
        $this->traDomicilio = $traDomicilio;

        return $this;
    }

    /**
     * Get traDomicilio
     *
     * @return string
     */
    public function getTraDomicilio()
    {
        return $this->traDomicilio;
    }

    /**
     * Set traTelefono
     *
     * @param string $traTelefono
     *
     * @return Trabajo
     */
    public function setTraTelefono($traTelefono)
    {
        $this->traTelefono = $traTelefono;

        return $this;
    }

    /**
     * Get traTelefono
     *
     * @return string
     */
    public function getTraTelefono()
    {
        return $this->traTelefono;
    }

    /**
     * Set traCelular
     *
     * @param string $traCelular
     *
     * @return Trabajo
     */
    public function setTraCelular($traCelular)
    {
        $this->traCelular = $traCelular;

        return $this;
    }

    /**
     * Get traCelular
     *
     * @return string
     */
    public function getTraCelular()
    {
        return $this->traCelular;
    }

    /**
     * Set traCorreo
     *
     * @param string $traCorreo
     *
     * @return Trabajo
     */
    public function setTraCorreo($traCorreo)
    {
        $this->traCorreo = $traCorreo;

        return $this;
    }

    /**
     * Get traCorreo
     *
     * @return string
     */
    public function getTraCorreo()
    {
        return $this->traCorreo;
    }

    /**
     * Set traImporte1
     *
     * @param float $traImporte1
     *
     * @return Trabajo
     */
    public function setTraImporte1($traImporte1)
    {
        $this->traImporte1 = $traImporte1;

        return $this;
    }

    /**
     * Get traImporte1
     *
     * @return float
     */
    public function getTraImporte1()
    {
        return $this->traImporte1;
    }

    /**
     * Set traImporte2
     *
     * @param float $traImporte2
     *
     * @return Trabajo
     */
    public function setTraImporte2($traImporte2)
    {
        $this->traImporte2 = $traImporte2;

        return $this;
    }

    /**
     * Get traImporte2
     *
     * @return float
     */
    public function getTraImporte2()
    {
        return $this->traImporte2;
    }

    /**
     * Set traHonorariominimo
     *
     * @param float $traHonorariominimo
     *
     * @return Trabajo
     */
    public function setTraHonorariominimo($traHonorariominimo)
    {
        $this->traHonorariominimo = $traHonorariominimo;

        return $this;
    }

    /**
     * Get traHonorariominimo
     *
     * @return float
     */
    public function getTraHonorariominimo()
    {
        return $this->traHonorariominimo;
    }

    /**
     * Set traMonto
     *
     * @param float $traMonto
     *
     * @return Trabajo
     */
    public function setTraMonto($traMonto)
    {
        $this->traMonto = $traMonto;

        return $this;
    }

    /**
     * Get traMonto
     *
     * @return float
     */
    public function getTraMonto()
    {
        return $this->traMonto;
    }

    /**
     * Set traCondicioniva
     *
     * @param string $traCondicioniva
     *
     * @return Trabajo
     */
    public function setTraCondicioniva($traCondicioniva)
    {
        $this->traCondicioniva = $traCondicioniva;

        return $this;
    }

    /**
     * Get traCondicioniva
     *
     * @return string
     */
    public function getTraCondicioniva()
    {
        return $this->traCondicioniva;
    }

    /**
     * Set traCuit
     *
     * @param string $traCuit
     *
     * @return Trabajo
     */
    public function setTraCuit($traCuit)
    {
        $this->traCuit = $traCuit;

        return $this;
    }

    /**
     * Get traCuit
     *
     * @return string
     */
    public function getTraCuit()
    {
        return $this->traCuit;
    }

    /**
     * Set traCantidad
     *
     * @param integer $traCantidad
     *
     * @return Trabajo
     */
    public function setTraCantidad($traCantidad)
    {
        $this->traCantidad = $traCantidad;

        return $this;
    }

    /**
     * Get traCantidad
     *
     * @return integer
     */
    public function getTraCantidad()
    {
        return $this->traCantidad;
    }

    /**
     * Set traFechaCierre
     *
     * @param \DateTime $traFechaCierre
     *
     * @return Trabajo
     */
    public function setTraFechaCierre($traFechaCierre)
    {
        $this->traFechaCierre = $traFechaCierre;

        return $this;
    }

    /**
     * Get traFechaCierre
     *
     * @return \DateTime
     */
    public function getTraFechaCierre()
    {
        return $this->traFechaCierre;
    }

    /**
     * Set traAuditoriaTipo
     *
     * @param string $traAuditoriaTipo
     *
     * @return Trabajo
     */
    public function setTraAuditoriaTipo($traAuditoriaTipo)
    {
        $this->traAuditoriaTipo = $traAuditoriaTipo;

        return $this;
    }

    /**
     * Get traAuditoriaTipo
     *
     * @return string
     */
    public function getTraAuditoriaTipo()
    {
        return $this->traAuditoriaTipo;
    }

    /**
     * Set traMontoIva
     *
     * @param float $traMontoIva
     *
     * @return Trabajo
     */
    public function setTraMontoIva($traMontoIva)
    {
        $this->traMontoIva = $traMontoIva;

        return $this;
    }

    /**
     * Get traMontoIva
     *
     * @return float
     */
    public function getTraMontoIva()
    {
        return $this->traMontoIva;
    }

    /**
     * Set traMontoAporte
     *
     * @param float $traMontoAporte
     *
     * @return Trabajo
     */
    public function setTraMontoAporte($traMontoAporte)
    {
        $this->traMontoAporte = $traMontoAporte;

        return $this;
    }

    /**
     * Get traMontoAporte
     *
     * @return float
     */
    public function getTraMontoAporte()
    {
        return $this->traMontoAporte;
    }

    /**
     * Set traMontoDeposito
     *
     * @param float $traMontoDeposito
     *
     * @return Trabajo
     */
    public function setTraMontoDeposito($traMontoDeposito)
    {
        $this->traMontoDeposito = $traMontoDeposito;

        return $this;
    }

    /**
     * Get traMontoDeposito
     *
     * @return float
     */
    public function getTraMontoDeposito()
    {
        return $this->traMontoDeposito;
    }

    /**
     * Set traNroasi
     *
     * @param integer $traNroasi
     *
     * @return Trabajo
     */
    public function setTraNroasi($traNroasi)
    {
        $this->traNroasi = $traNroasi;

        return $this;
    }

    /**
     * Get traNroasi
     *
     * @return integer
     */
    public function getTraNroasi()
    {
        return $this->traNroasi;
    }

    /**
     * Set traNrolegali
     *
     * @param integer $traNrolegali
     *
     * @return Trabajo
     */
    public function setTraNrolegali($traNrolegali)
    {
        $this->traNrolegali = $traNrolegali;

        return $this;
    }

    /**
     * Get traNrolegali
     *
     * @return integer
     */
    public function getTraNrolegali()
    {
        return $this->traNrolegali;
    }

    /**
     * Set traNroasiLiquidacion
     *
     * @param integer $traNroasiLiquidacion
     *
     * @return Trabajo
     */
    public function setTraNroasiLiquidacion($traNroasiLiquidacion)
    {
        $this->traNroasiLiquidacion = $traNroasiLiquidacion;

        return $this;
    }

    /**
     * Get traNroasiLiquidacion
     *
     * @return integer
     */
    public function getTraNroasiLiquidacion()
    {
        return $this->traNroasiLiquidacion;
    }

    /**
     * Set traCertificado
     *
     * @param string $traCertificado
     *
     * @return Trabajo
     */
    public function setTraCertificado($traCertificado)
    {
        $this->traCertificado = $traCertificado;

        return $this;
    }

    /**
     * Get traCertificado
     *
     * @return string
     */
    public function getTraCertificado()
    {
        return $this->traCertificado;
    }

    /**
     * Set traEsauditor
     *
     * @param string $traEsauditor
     *
     * @return Trabajo
     */
    public function setTraEsauditor($traEsauditor)
    {
        $this->traEsauditor = $traEsauditor;

        return $this;
    }

    /**
     * Get traEsauditor
     *
     * @return string
     */
    public function getTraEsauditor()
    {
        return $this->traEsauditor;
    }

    /**
     * Set traMeses
     *
     * @param string $traMeses
     *
     * @return Trabajo
     */
    public function setTraMeses($traMeses)
    {
        $this->traMeses = $traMeses;

        return $this;
    }

    /**
     * Get traMeses
     *
     * @return string
     */
    public function getTraMeses()
    {
        return $this->traMeses;
    }

    /**
     * Set traPorcentajeSindico
     *
     * @param string $traPorcentajeSindico
     *
     * @return Trabajo
     */
    public function setTraPorcentajeSindico($traPorcentajeSindico)
    {
        $this->traPorcentajeSindico = $traPorcentajeSindico;

        return $this;
    }

    /**
     * Get traPorcentajeSindico
     *
     * @return string
     */
    public function getTraPorcentajeSindico()
    {
        return $this->traPorcentajeSindico;
    }

    /**
     * Set traImportePeriodo
     *
     * @param float $traImportePeriodo
     *
     * @return Trabajo
     */
    public function setTraImportePeriodo($traImportePeriodo)
    {
        $this->traImportePeriodo = $traImportePeriodo;

        return $this;
    }

    /**
     * Get traImportePeriodo
     *
     * @return float
     */
    public function getTraImportePeriodo()
    {
        return $this->traImportePeriodo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set traEstado
     *
     * @param \CmsGa\CPCEBundle\Entity\TrabajoEstado $traEstado
     *
     * @return Trabajo
     */
    public function setTraEstado(\CmsGa\CPCEBundle\Entity\TrabajoEstado $traEstado = null)
    {
        $this->traEstado = $traEstado;

        return $this;
    }

    /**
     * Get traEstado
     *
     * @return \CmsGa\CPCEBundle\Entity\TrabajoEstado
     */
    public function getTraEstado()
    {
        return $this->traEstado;
    }

    /**
     * Set traTarea
     *
     * @param \CmsGa\CPCEBundle\Entity\Tareas $traTarea
     *
     * @return Trabajo
     */
    public function setTraTarea(\CmsGa\CPCEBundle\Entity\Tareas $traTarea = null)
    {
        $this->traTarea = $traTarea;

        return $this;
    }

    /**
     * Get traTarea
     *
     * @return \CmsGa\CPCEBundle\Entity\Tareas
     */
    public function getTraTarea()
    {
        return $this->traTarea;
    }

    /**
     * Set traUser
     *
     * @param \CmsGa\CPCEBundle\Entity\FosUser $traUser
     *
     * @return Trabajo
     */
    public function setTraUser(\CmsGa\CPCEBundle\Entity\FosUser $traUser = null)
    {
        $this->traUser = $traUser;

        return $this;
    }

    /**
     * Get traUser
     *
     * @return \CmsGa\CPCEBundle\Entity\FosUser
     */
    public function getTraUser()
    {
        return $this->traUser;
    }
}
