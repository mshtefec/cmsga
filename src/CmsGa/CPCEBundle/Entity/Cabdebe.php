<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cabdebe
 *
 * @ORM\Table(name="cabdebe")
 * @ORM\Entity
 */
class Cabdebe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="INSTITUT", type="string", length=2, nullable=true)
     */
    private $institut;

    /**
     * @var float
     *
     * @ORM\Column(name="ASIENTO", type="float", precision=53, scale=0, nullable=true)
     */
    private $asiento;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIP", type="string", length=60, nullable=true)
     */
    private $descrip;

    /**
     * @var string
     *
     * @ORM\Column(name="DESTINO", type="string", length=60, nullable=true)
     */
    private $destino;

    /**
     * @var string
     *
     * @ORM\Column(name="CPTO1", type="string", length=70, nullable=true)
     */
    private $cpto1;

    /**
     * @var string
     *
     * @ORM\Column(name="CPTO2", type="string", length=70, nullable=true)
     */
    private $cpto2;

    /**
     * @var string
     *
     * @ORM\Column(name="CPTO3", type="string", length=70, nullable=true)
     */
    private $cpto3;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="ESTADO", type="string", length=1, nullable=true)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="CAMPO1", type="string", length=1, nullable=true)
     */
    private $campo1;

    /**
     * @var string
     *
     * @ORM\Column(name="LEYENDA", type="string", length=20, nullable=true)
     */
    private $leyenda;

    /**
     * @var float
     *
     * @ORM\Column(name="REF_AA", type="float", precision=53, scale=0, nullable=true)
     */
    private $refAa;

    /**
     * @var float
     *
     * @ORM\Column(name="REF_MM", type="float", precision=53, scale=0, nullable=true)
     */
    private $refMm;



    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Cabdebe
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set institut
     *
     * @param string $institut
     *
     * @return Cabdebe
     */
    public function setInstitut($institut)
    {
        $this->institut = $institut;

        return $this;
    }

    /**
     * Get institut
     *
     * @return string
     */
    public function getInstitut()
    {
        return $this->institut;
    }

    /**
     * Set asiento
     *
     * @param float $asiento
     *
     * @return Cabdebe
     */
    public function setAsiento($asiento)
    {
        $this->asiento = $asiento;

        return $this;
    }

    /**
     * Get asiento
     *
     * @return float
     */
    public function getAsiento()
    {
        return $this->asiento;
    }

    /**
     * Set descrip
     *
     * @param string $descrip
     *
     * @return Cabdebe
     */
    public function setDescrip($descrip)
    {
        $this->descrip = $descrip;

        return $this;
    }

    /**
     * Get descrip
     *
     * @return string
     */
    public function getDescrip()
    {
        return $this->descrip;
    }

    /**
     * Set destino
     *
     * @param string $destino
     *
     * @return Cabdebe
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino
     *
     * @return string
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set cpto1
     *
     * @param string $cpto1
     *
     * @return Cabdebe
     */
    public function setCpto1($cpto1)
    {
        $this->cpto1 = $cpto1;

        return $this;
    }

    /**
     * Get cpto1
     *
     * @return string
     */
    public function getCpto1()
    {
        return $this->cpto1;
    }

    /**
     * Set cpto2
     *
     * @param string $cpto2
     *
     * @return Cabdebe
     */
    public function setCpto2($cpto2)
    {
        $this->cpto2 = $cpto2;

        return $this;
    }

    /**
     * Get cpto2
     *
     * @return string
     */
    public function getCpto2()
    {
        return $this->cpto2;
    }

    /**
     * Set cpto3
     *
     * @param string $cpto3
     *
     * @return Cabdebe
     */
    public function setCpto3($cpto3)
    {
        $this->cpto3 = $cpto3;

        return $this;
    }

    /**
     * Get cpto3
     *
     * @return string
     */
    public function getCpto3()
    {
        return $this->cpto3;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Cabdebe
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Cabdebe
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set campo1
     *
     * @param string $campo1
     *
     * @return Cabdebe
     */
    public function setCampo1($campo1)
    {
        $this->campo1 = $campo1;

        return $this;
    }

    /**
     * Get campo1
     *
     * @return string
     */
    public function getCampo1()
    {
        return $this->campo1;
    }

    /**
     * Set leyenda
     *
     * @param string $leyenda
     *
     * @return Cabdebe
     */
    public function setLeyenda($leyenda)
    {
        $this->leyenda = $leyenda;

        return $this;
    }

    /**
     * Get leyenda
     *
     * @return string
     */
    public function getLeyenda()
    {
        return $this->leyenda;
    }

    /**
     * Set refAa
     *
     * @param float $refAa
     *
     * @return Cabdebe
     */
    public function setRefAa($refAa)
    {
        $this->refAa = $refAa;

        return $this;
    }

    /**
     * Get refAa
     *
     * @return float
     */
    public function getRefAa()
    {
        return $this->refAa;
    }

    /**
     * Set refMm
     *
     * @param float $refMm
     *
     * @return Cabdebe
     */
    public function setRefMm($refMm)
    {
        $this->refMm = $refMm;

        return $this;
    }

    /**
     * Get refMm
     *
     * @return float
     */
    public function getRefMm()
    {
        return $this->refMm;
    }
}
