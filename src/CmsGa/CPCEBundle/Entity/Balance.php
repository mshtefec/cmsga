<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Balance
 *
 * @ORM\Table(name="balance")
 * @ORM\Entity
 */
class Balance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pla_nrocta", type="string", length=8, nullable=false)
     */
    private $plaNrocta = '';

    /**
     * @var string
     *
     * @ORM\Column(name="pla_nombre", type="string", length=50, nullable=true)
     */
    private $plaNombre = '';

    /**
     * @var float
     *
     * @ORM\Column(name="pla_debe", type="float", precision=12, scale=2, nullable=true)
     */
    private $plaDebe = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="pla_haber", type="float", precision=12, scale=2, nullable=true)
     */
    private $plaHaber;

    /**
     * @var float
     *
     * @ORM\Column(name="DEBE_ANT", type="float", precision=53, scale=0, nullable=true)
     */
    private $debeAnt;

    /**
     * @var float
     *
     * @ORM\Column(name="HABER_ANT", type="float", precision=53, scale=0, nullable=true)
     */
    private $haberAnt;

    /**
     * @var string
     *
     * @ORM\Column(name="ACUMULA", type="string", length=8, nullable=true)
     */
    private $acumula;

    /**
     * @var string
     *
     * @ORM\Column(name="IMPUTABLE", type="string", length=1, nullable=true)
     */
    private $imputable;

    /**
     * @var string
     *
     * @ORM\Column(name="LETRA", type="string", length=1, nullable=true)
     */
    private $letra;

    /**
     * @var float
     *
     * @ORM\Column(name="SALTO_ANT", type="float", precision=53, scale=0, nullable=true)
     */
    private $saltoAnt;

    /**
     * @var float
     *
     * @ORM\Column(name="SALTO_POS", type="float", precision=53, scale=0, nullable=true)
     */
    private $saltoPos;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ASENTADO", type="boolean", nullable=false)
     */
    private $asentado;

    /**
     * @var string
     *
     * @ORM\Column(name="CIERRE", type="string", length=1, nullable=true)
     */
    private $cierre;

    /**
     * @var string
     *
     * @ORM\Column(name="APERTURA", type="string", length=1, nullable=true)
     */
    private $apertura;

    /**
     * @var float
     *
     * @ORM\Column(name="D_0001", type="float", precision=53, scale=0, nullable=true)
     */
    private $d0001;

    /**
     * @var float
     *
     * @ORM\Column(name="H_0001", type="float", precision=53, scale=0, nullable=true)
     */
    private $h0001;

    /**
     * @var float
     *
     * @ORM\Column(name="D_0002", type="float", precision=53, scale=0, nullable=true)
     */
    private $d0002;

    /**
     * @var float
     *
     * @ORM\Column(name="H_0002", type="float", precision=53, scale=0, nullable=true)
     */
    private $h0002;

    /**
     * @var float
     *
     * @ORM\Column(name="D_0003", type="float", precision=53, scale=0, nullable=true)
     */
    private $d0003;

    /**
     * @var float
     *
     * @ORM\Column(name="H_0003", type="float", precision=53, scale=0, nullable=true)
     */
    private $h0003;

    /**
     * @var float
     *
     * @ORM\Column(name="D_0004", type="float", precision=53, scale=0, nullable=true)
     */
    private $d0004;

    /**
     * @var float
     *
     * @ORM\Column(name="H_0004", type="float", precision=53, scale=0, nullable=true)
     */
    private $h0004;

    /**
     * @var float
     *
     * @ORM\Column(name="D_OTRO", type="float", precision=53, scale=0, nullable=true)
     */
    private $dOtro;

    /**
     * @var float
     *
     * @ORM\Column(name="H_OTRO", type="float", precision=53, scale=0, nullable=true)
     */
    private $hOtro;



    /**
     * Set plaNrocta
     *
     * @param string $plaNrocta
     *
     * @return Balance
     */
    public function setPlaNrocta($plaNrocta)
    {
        $this->plaNrocta = $plaNrocta;

        return $this;
    }

    /**
     * Get plaNrocta
     *
     * @return string
     */
    public function getPlaNrocta()
    {
        return $this->plaNrocta;
    }

    /**
     * Set plaNombre
     *
     * @param string $plaNombre
     *
     * @return Balance
     */
    public function setPlaNombre($plaNombre)
    {
        $this->plaNombre = $plaNombre;

        return $this;
    }

    /**
     * Get plaNombre
     *
     * @return string
     */
    public function getPlaNombre()
    {
        return $this->plaNombre;
    }

    /**
     * Set plaDebe
     *
     * @param float $plaDebe
     *
     * @return Balance
     */
    public function setPlaDebe($plaDebe)
    {
        $this->plaDebe = $plaDebe;

        return $this;
    }

    /**
     * Get plaDebe
     *
     * @return float
     */
    public function getPlaDebe()
    {
        return $this->plaDebe;
    }

    /**
     * Set plaHaber
     *
     * @param float $plaHaber
     *
     * @return Balance
     */
    public function setPlaHaber($plaHaber)
    {
        $this->plaHaber = $plaHaber;

        return $this;
    }

    /**
     * Get plaHaber
     *
     * @return float
     */
    public function getPlaHaber()
    {
        return $this->plaHaber;
    }

    /**
     * Set debeAnt
     *
     * @param float $debeAnt
     *
     * @return Balance
     */
    public function setDebeAnt($debeAnt)
    {
        $this->debeAnt = $debeAnt;

        return $this;
    }

    /**
     * Get debeAnt
     *
     * @return float
     */
    public function getDebeAnt()
    {
        return $this->debeAnt;
    }

    /**
     * Set haberAnt
     *
     * @param float $haberAnt
     *
     * @return Balance
     */
    public function setHaberAnt($haberAnt)
    {
        $this->haberAnt = $haberAnt;

        return $this;
    }

    /**
     * Get haberAnt
     *
     * @return float
     */
    public function getHaberAnt()
    {
        return $this->haberAnt;
    }

    /**
     * Set acumula
     *
     * @param string $acumula
     *
     * @return Balance
     */
    public function setAcumula($acumula)
    {
        $this->acumula = $acumula;

        return $this;
    }

    /**
     * Get acumula
     *
     * @return string
     */
    public function getAcumula()
    {
        return $this->acumula;
    }

    /**
     * Set imputable
     *
     * @param string $imputable
     *
     * @return Balance
     */
    public function setImputable($imputable)
    {
        $this->imputable = $imputable;

        return $this;
    }

    /**
     * Get imputable
     *
     * @return string
     */
    public function getImputable()
    {
        return $this->imputable;
    }

    /**
     * Set letra
     *
     * @param string $letra
     *
     * @return Balance
     */
    public function setLetra($letra)
    {
        $this->letra = $letra;

        return $this;
    }

    /**
     * Get letra
     *
     * @return string
     */
    public function getLetra()
    {
        return $this->letra;
    }

    /**
     * Set saltoAnt
     *
     * @param float $saltoAnt
     *
     * @return Balance
     */
    public function setSaltoAnt($saltoAnt)
    {
        $this->saltoAnt = $saltoAnt;

        return $this;
    }

    /**
     * Get saltoAnt
     *
     * @return float
     */
    public function getSaltoAnt()
    {
        return $this->saltoAnt;
    }

    /**
     * Set saltoPos
     *
     * @param float $saltoPos
     *
     * @return Balance
     */
    public function setSaltoPos($saltoPos)
    {
        $this->saltoPos = $saltoPos;

        return $this;
    }

    /**
     * Get saltoPos
     *
     * @return float
     */
    public function getSaltoPos()
    {
        return $this->saltoPos;
    }

    /**
     * Set asentado
     *
     * @param boolean $asentado
     *
     * @return Balance
     */
    public function setAsentado($asentado)
    {
        $this->asentado = $asentado;

        return $this;
    }

    /**
     * Get asentado
     *
     * @return boolean
     */
    public function getAsentado()
    {
        return $this->asentado;
    }

    /**
     * Set cierre
     *
     * @param string $cierre
     *
     * @return Balance
     */
    public function setCierre($cierre)
    {
        $this->cierre = $cierre;

        return $this;
    }

    /**
     * Get cierre
     *
     * @return string
     */
    public function getCierre()
    {
        return $this->cierre;
    }

    /**
     * Set apertura
     *
     * @param string $apertura
     *
     * @return Balance
     */
    public function setApertura($apertura)
    {
        $this->apertura = $apertura;

        return $this;
    }

    /**
     * Get apertura
     *
     * @return string
     */
    public function getApertura()
    {
        return $this->apertura;
    }

    /**
     * Set d0001
     *
     * @param float $d0001
     *
     * @return Balance
     */
    public function setD0001($d0001)
    {
        $this->d0001 = $d0001;

        return $this;
    }

    /**
     * Get d0001
     *
     * @return float
     */
    public function getD0001()
    {
        return $this->d0001;
    }

    /**
     * Set h0001
     *
     * @param float $h0001
     *
     * @return Balance
     */
    public function setH0001($h0001)
    {
        $this->h0001 = $h0001;

        return $this;
    }

    /**
     * Get h0001
     *
     * @return float
     */
    public function getH0001()
    {
        return $this->h0001;
    }

    /**
     * Set d0002
     *
     * @param float $d0002
     *
     * @return Balance
     */
    public function setD0002($d0002)
    {
        $this->d0002 = $d0002;

        return $this;
    }

    /**
     * Get d0002
     *
     * @return float
     */
    public function getD0002()
    {
        return $this->d0002;
    }

    /**
     * Set h0002
     *
     * @param float $h0002
     *
     * @return Balance
     */
    public function setH0002($h0002)
    {
        $this->h0002 = $h0002;

        return $this;
    }

    /**
     * Get h0002
     *
     * @return float
     */
    public function getH0002()
    {
        return $this->h0002;
    }

    /**
     * Set d0003
     *
     * @param float $d0003
     *
     * @return Balance
     */
    public function setD0003($d0003)
    {
        $this->d0003 = $d0003;

        return $this;
    }

    /**
     * Get d0003
     *
     * @return float
     */
    public function getD0003()
    {
        return $this->d0003;
    }

    /**
     * Set h0003
     *
     * @param float $h0003
     *
     * @return Balance
     */
    public function setH0003($h0003)
    {
        $this->h0003 = $h0003;

        return $this;
    }

    /**
     * Get h0003
     *
     * @return float
     */
    public function getH0003()
    {
        return $this->h0003;
    }

    /**
     * Set d0004
     *
     * @param float $d0004
     *
     * @return Balance
     */
    public function setD0004($d0004)
    {
        $this->d0004 = $d0004;

        return $this;
    }

    /**
     * Get d0004
     *
     * @return float
     */
    public function getD0004()
    {
        return $this->d0004;
    }

    /**
     * Set h0004
     *
     * @param float $h0004
     *
     * @return Balance
     */
    public function setH0004($h0004)
    {
        $this->h0004 = $h0004;

        return $this;
    }

    /**
     * Get h0004
     *
     * @return float
     */
    public function getH0004()
    {
        return $this->h0004;
    }

    /**
     * Set dOtro
     *
     * @param float $dOtro
     *
     * @return Balance
     */
    public function setDOtro($dOtro)
    {
        $this->dOtro = $dOtro;

        return $this;
    }

    /**
     * Get dOtro
     *
     * @return float
     */
    public function getDOtro()
    {
        return $this->dOtro;
    }

    /**
     * Set hOtro
     *
     * @param float $hOtro
     *
     * @return Balance
     */
    public function setHOtro($hOtro)
    {
        $this->hOtro = $hOtro;

        return $this;
    }

    /**
     * Get hOtro
     *
     * @return float
     */
    public function getHOtro()
    {
        return $this->hOtro;
    }
}
