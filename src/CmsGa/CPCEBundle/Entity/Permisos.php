<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permisos
 *
 * @ORM\Table(name="permisos")
 * @ORM\Entity
 */
class Permisos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="per_nrocli", type="integer", nullable=false)
     */
    private $perNrocli = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="per_operador", type="integer", nullable=false)
     */
    private $perOperador = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="per_acceso", type="integer", nullable=false)
     */
    private $perAcceso = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="per_permitido", type="string", nullable=false)
     */
    private $perPermitido = 'NO';



    /**
     * Set perNrocli
     *
     * @param integer $perNrocli
     *
     * @return Permisos
     */
    public function setPerNrocli($perNrocli)
    {
        $this->perNrocli = $perNrocli;

        return $this;
    }

    /**
     * Get perNrocli
     *
     * @return integer
     */
    public function getPerNrocli()
    {
        return $this->perNrocli;
    }

    /**
     * Set perOperador
     *
     * @param integer $perOperador
     *
     * @return Permisos
     */
    public function setPerOperador($perOperador)
    {
        $this->perOperador = $perOperador;

        return $this;
    }

    /**
     * Get perOperador
     *
     * @return integer
     */
    public function getPerOperador()
    {
        return $this->perOperador;
    }

    /**
     * Set perAcceso
     *
     * @param integer $perAcceso
     *
     * @return Permisos
     */
    public function setPerAcceso($perAcceso)
    {
        $this->perAcceso = $perAcceso;

        return $this;
    }

    /**
     * Get perAcceso
     *
     * @return integer
     */
    public function getPerAcceso()
    {
        return $this->perAcceso;
    }

    /**
     * Set perPermitido
     *
     * @param string $perPermitido
     *
     * @return Permisos
     */
    public function setPerPermitido($perPermitido)
    {
        $this->perPermitido = $perPermitido;

        return $this;
    }

    /**
     * Get perPermitido
     *
     * @return string
     */
    public function getPerPermitido()
    {
        return $this->perPermitido;
    }
}
