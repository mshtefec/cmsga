<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comitente
 *
 * @ORM\Table(name="comitente", indexes={@ORM\Index(name="SkMatricula", columns={"afi_titulo", "afi_matricula"}), @ORM\Index(name="afi_tipo_nombre", columns={"afi_tipo", "afi_nombre"})})
 * @ORM\Entity
 */
class Comitente
{
    /**
     * @var string
     *
     * @ORM\Column(name="afi_nombre", type="string", length=100, nullable=false)
     */
    private $afiNombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="afi_fecnac", type="date", nullable=false)
     */
    private $afiFecnac;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_tipo", type="string", length=1, nullable=false)
     */
    private $afiTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_tipdoc", type="string", length=3, nullable=false)
     */
    private $afiTipdoc;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_titulo", type="string", length=2, nullable=false)
     */
    private $afiTitulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="afi_matricula", type="integer", nullable=false)
     */
    private $afiMatricula;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_direccion", type="string", length=100, nullable=false)
     */
    private $afiDireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_localidad", type="string", length=20, nullable=false)
     */
    private $afiLocalidad;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_provincia", type="string", length=50, nullable=false)
     */
    private $afiProvincia;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_zona", type="string", length=4, nullable=false)
     */
    private $afiZona;

    /**
     * @var integer
     *
     * @ORM\Column(name="afi_codpos", type="integer", nullable=false)
     */
    private $afiCodpos;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_telefono1", type="string", length=50, nullable=false)
     */
    private $afiTelefono1;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_telefono2", type="string", length=50, nullable=false)
     */
    private $afiTelefono2;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_mail", type="string", length=100, nullable=false)
     */
    private $afiMail;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_tipoiva", type="string", length=2, nullable=false)
     */
    private $afiTipoiva;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_cuit", type="string", length=15, nullable=false)
     */
    private $afiCuit;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_dgr", type="string", length=15, nullable=false)
     */
    private $afiDgr;

    /**
     * @var float
     *
     * @ORM\Column(name="afi_lote", type="float", precision=10, scale=0, nullable=false)
     */
    private $afiLote;

    /**
     * @var integer
     *
     * @ORM\Column(name="afi_nrodoc", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $afiNrodoc;



    /**
     * Set afiNombre
     *
     * @param string $afiNombre
     *
     * @return Comitente
     */
    public function setAfiNombre($afiNombre)
    {
        $this->afiNombre = $afiNombre;

        return $this;
    }

    /**
     * Get afiNombre
     *
     * @return string
     */
    public function getAfiNombre()
    {
        return $this->afiNombre;
    }

    /**
     * Set afiFecnac
     *
     * @param \DateTime $afiFecnac
     *
     * @return Comitente
     */
    public function setAfiFecnac($afiFecnac)
    {
        $this->afiFecnac = $afiFecnac;

        return $this;
    }

    /**
     * Get afiFecnac
     *
     * @return \DateTime
     */
    public function getAfiFecnac()
    {
        return $this->afiFecnac;
    }

    /**
     * Set afiTipo
     *
     * @param string $afiTipo
     *
     * @return Comitente
     */
    public function setAfiTipo($afiTipo)
    {
        $this->afiTipo = $afiTipo;

        return $this;
    }

    /**
     * Get afiTipo
     *
     * @return string
     */
    public function getAfiTipo()
    {
        return $this->afiTipo;
    }

    /**
     * Set afiTipdoc
     *
     * @param string $afiTipdoc
     *
     * @return Comitente
     */
    public function setAfiTipdoc($afiTipdoc)
    {
        $this->afiTipdoc = $afiTipdoc;

        return $this;
    }

    /**
     * Get afiTipdoc
     *
     * @return string
     */
    public function getAfiTipdoc()
    {
        return $this->afiTipdoc;
    }

    /**
     * Set afiTitulo
     *
     * @param string $afiTitulo
     *
     * @return Comitente
     */
    public function setAfiTitulo($afiTitulo)
    {
        $this->afiTitulo = $afiTitulo;

        return $this;
    }

    /**
     * Get afiTitulo
     *
     * @return string
     */
    public function getAfiTitulo()
    {
        return $this->afiTitulo;
    }

    /**
     * Set afiMatricula
     *
     * @param integer $afiMatricula
     *
     * @return Comitente
     */
    public function setAfiMatricula($afiMatricula)
    {
        $this->afiMatricula = $afiMatricula;

        return $this;
    }

    /**
     * Get afiMatricula
     *
     * @return integer
     */
    public function getAfiMatricula()
    {
        return $this->afiMatricula;
    }

    /**
     * Set afiDireccion
     *
     * @param string $afiDireccion
     *
     * @return Comitente
     */
    public function setAfiDireccion($afiDireccion)
    {
        $this->afiDireccion = $afiDireccion;

        return $this;
    }

    /**
     * Get afiDireccion
     *
     * @return string
     */
    public function getAfiDireccion()
    {
        return $this->afiDireccion;
    }

    /**
     * Set afiLocalidad
     *
     * @param string $afiLocalidad
     *
     * @return Comitente
     */
    public function setAfiLocalidad($afiLocalidad)
    {
        $this->afiLocalidad = $afiLocalidad;

        return $this;
    }

    /**
     * Get afiLocalidad
     *
     * @return string
     */
    public function getAfiLocalidad()
    {
        return $this->afiLocalidad;
    }

    /**
     * Set afiProvincia
     *
     * @param string $afiProvincia
     *
     * @return Comitente
     */
    public function setAfiProvincia($afiProvincia)
    {
        $this->afiProvincia = $afiProvincia;

        return $this;
    }

    /**
     * Get afiProvincia
     *
     * @return string
     */
    public function getAfiProvincia()
    {
        return $this->afiProvincia;
    }

    /**
     * Set afiZona
     *
     * @param string $afiZona
     *
     * @return Comitente
     */
    public function setAfiZona($afiZona)
    {
        $this->afiZona = $afiZona;

        return $this;
    }

    /**
     * Get afiZona
     *
     * @return string
     */
    public function getAfiZona()
    {
        return $this->afiZona;
    }

    /**
     * Set afiCodpos
     *
     * @param integer $afiCodpos
     *
     * @return Comitente
     */
    public function setAfiCodpos($afiCodpos)
    {
        $this->afiCodpos = $afiCodpos;

        return $this;
    }

    /**
     * Get afiCodpos
     *
     * @return integer
     */
    public function getAfiCodpos()
    {
        return $this->afiCodpos;
    }

    /**
     * Set afiTelefono1
     *
     * @param string $afiTelefono1
     *
     * @return Comitente
     */
    public function setAfiTelefono1($afiTelefono1)
    {
        $this->afiTelefono1 = $afiTelefono1;

        return $this;
    }

    /**
     * Get afiTelefono1
     *
     * @return string
     */
    public function getAfiTelefono1()
    {
        return $this->afiTelefono1;
    }

    /**
     * Set afiTelefono2
     *
     * @param string $afiTelefono2
     *
     * @return Comitente
     */
    public function setAfiTelefono2($afiTelefono2)
    {
        $this->afiTelefono2 = $afiTelefono2;

        return $this;
    }

    /**
     * Get afiTelefono2
     *
     * @return string
     */
    public function getAfiTelefono2()
    {
        return $this->afiTelefono2;
    }

    /**
     * Set afiMail
     *
     * @param string $afiMail
     *
     * @return Comitente
     */
    public function setAfiMail($afiMail)
    {
        $this->afiMail = $afiMail;

        return $this;
    }

    /**
     * Get afiMail
     *
     * @return string
     */
    public function getAfiMail()
    {
        return $this->afiMail;
    }

    /**
     * Set afiTipoiva
     *
     * @param string $afiTipoiva
     *
     * @return Comitente
     */
    public function setAfiTipoiva($afiTipoiva)
    {
        $this->afiTipoiva = $afiTipoiva;

        return $this;
    }

    /**
     * Get afiTipoiva
     *
     * @return string
     */
    public function getAfiTipoiva()
    {
        return $this->afiTipoiva;
    }

    /**
     * Set afiCuit
     *
     * @param string $afiCuit
     *
     * @return Comitente
     */
    public function setAfiCuit($afiCuit)
    {
        $this->afiCuit = $afiCuit;

        return $this;
    }

    /**
     * Get afiCuit
     *
     * @return string
     */
    public function getAfiCuit()
    {
        return $this->afiCuit;
    }

    /**
     * Set afiDgr
     *
     * @param string $afiDgr
     *
     * @return Comitente
     */
    public function setAfiDgr($afiDgr)
    {
        $this->afiDgr = $afiDgr;

        return $this;
    }

    /**
     * Get afiDgr
     *
     * @return string
     */
    public function getAfiDgr()
    {
        return $this->afiDgr;
    }

    /**
     * Set afiLote
     *
     * @param float $afiLote
     *
     * @return Comitente
     */
    public function setAfiLote($afiLote)
    {
        $this->afiLote = $afiLote;

        return $this;
    }

    /**
     * Get afiLote
     *
     * @return float
     */
    public function getAfiLote()
    {
        return $this->afiLote;
    }

    /**
     * Get afiNrodoc
     *
     * @return integer
     */
    public function getAfiNrodoc()
    {
        return $this->afiNrodoc;
    }
}
