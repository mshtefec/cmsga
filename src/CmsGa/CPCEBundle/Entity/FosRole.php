<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FosRole
 *
 * @ORM\Table(name="fos_role")
 * @ORM\Entity
 */
class FosRole
{
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="acceso", type="boolean", nullable=true)
     */
    private $acceso;

    /**
     * @var string
     *
     * @ORM\Column(name="role_nombre", type="string", length=255, nullable=false)
     */
    private $roleNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="CmsGa\CPCEBundle\Entity\FosUser", mappedBy="role")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return FosRole
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return FosRole
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set acceso
     *
     * @param boolean $acceso
     *
     * @return FosRole
     */
    public function setAcceso($acceso)
    {
        $this->acceso = $acceso;

        return $this;
    }

    /**
     * Get acceso
     *
     * @return boolean
     */
    public function getAcceso()
    {
        return $this->acceso;
    }

    /**
     * Set roleNombre
     *
     * @param string $roleNombre
     *
     * @return FosRole
     */
    public function setRoleNombre($roleNombre)
    {
        $this->roleNombre = $roleNombre;

        return $this;
    }

    /**
     * Get roleNombre
     *
     * @return string
     */
    public function getRoleNombre()
    {
        return $this->roleNombre;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add user
     *
     * @param \CmsGa\CPCEBundle\Entity\FosUser $user
     *
     * @return FosRole
     */
    public function addUser(\CmsGa\CPCEBundle\Entity\FosUser $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \CmsGa\CPCEBundle\Entity\FosUser $user
     */
    public function removeUser(\CmsGa\CPCEBundle\Entity\FosUser $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
