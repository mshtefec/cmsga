<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cateservi
 *
 * @ORM\Table(name="cateservi")
 * @ORM\Entity
 */
class Cateservi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="cas_idcategoria", type="integer", nullable=false)
     */
    private $casIdcategoria = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="cas_idservicio", type="string", length=10, nullable=false)
     */
    private $casIdservicio = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="cas_fecalt", type="datetime", nullable=false)
     */
    private $casFecalt = '0000-00-00 00:00:00';



    /**
     * Set casIdcategoria
     *
     * @param integer $casIdcategoria
     *
     * @return Cateservi
     */
    public function setCasIdcategoria($casIdcategoria)
    {
        $this->casIdcategoria = $casIdcategoria;

        return $this;
    }

    /**
     * Get casIdcategoria
     *
     * @return integer
     */
    public function getCasIdcategoria()
    {
        return $this->casIdcategoria;
    }

    /**
     * Set casIdservicio
     *
     * @param string $casIdservicio
     *
     * @return Cateservi
     */
    public function setCasIdservicio($casIdservicio)
    {
        $this->casIdservicio = $casIdservicio;

        return $this;
    }

    /**
     * Get casIdservicio
     *
     * @return string
     */
    public function getCasIdservicio()
    {
        return $this->casIdservicio;
    }

    /**
     * Set casFecalt
     *
     * @param \DateTime $casFecalt
     *
     * @return Cateservi
     */
    public function setCasFecalt($casFecalt)
    {
        $this->casFecalt = $casFecalt;

        return $this;
    }

    /**
     * Get casFecalt
     *
     * @return \DateTime
     */
    public function getCasFecalt()
    {
        return $this->casFecalt;
    }
}
