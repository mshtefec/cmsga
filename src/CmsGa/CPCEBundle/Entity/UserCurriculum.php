<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserCurriculum
 *
 * @ORM\Table(name="user_curriculum", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_DC85612FE8402530", columns={"usercv_id"})})
 * @ORM\Entity
 */
class UserCurriculum
{
    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255, nullable=true)
     */
    private $filePath;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CmsGa\CPCEBundle\Entity\FosUser
     *
     * @ORM\ManyToOne(targetEntity="CmsGa\CPCEBundle\Entity\FosUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usercv_id", referencedColumnName="id")
     * })
     */
    private $usercv;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="CmsGa\CPCEBundle\Entity\OfertaLaboral", mappedBy="userCurriculum")
     */
    private $ofertaLaboral;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ofertaLaboral = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return UserCurriculum
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UserCurriculum
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return UserCurriculum
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usercv
     *
     * @param \CmsGa\CPCEBundle\Entity\FosUser $usercv
     *
     * @return UserCurriculum
     */
    public function setUsercv(\CmsGa\CPCEBundle\Entity\FosUser $usercv = null)
    {
        $this->usercv = $usercv;

        return $this;
    }

    /**
     * Get usercv
     *
     * @return \CmsGa\CPCEBundle\Entity\FosUser
     */
    public function getUsercv()
    {
        return $this->usercv;
    }

    /**
     * Add ofertaLaboral
     *
     * @param \CmsGa\CPCEBundle\Entity\OfertaLaboral $ofertaLaboral
     *
     * @return UserCurriculum
     */
    public function addOfertaLaboral(\CmsGa\CPCEBundle\Entity\OfertaLaboral $ofertaLaboral)
    {
        $this->ofertaLaboral[] = $ofertaLaboral;

        return $this;
    }

    /**
     * Remove ofertaLaboral
     *
     * @param \CmsGa\CPCEBundle\Entity\OfertaLaboral $ofertaLaboral
     */
    public function removeOfertaLaboral(\CmsGa\CPCEBundle\Entity\OfertaLaboral $ofertaLaboral)
    {
        $this->ofertaLaboral->removeElement($ofertaLaboral);
    }

    /**
     * Get ofertaLaboral
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOfertaLaboral()
    {
        return $this->ofertaLaboral;
    }
}
