<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Thcalcac2014
 *
 * @ORM\Table(name="thcalcac2014")
 * @ORM\Entity
 */
class Thcalcac2014
{
    /**
     * @var float
     *
     * @ORM\Column(name="thc_fijo", type="float", precision=15, scale=2, nullable=false)
     */
    private $thcFijo = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="thc_adicporc", type="float", precision=6, scale=4, nullable=false)
     */
    private $thcAdicporc = '0.0000';

    /**
     * @var float
     *
     * @ORM\Column(name="thc_excedente", type="float", precision=15, scale=2, nullable=false)
     */
    private $thcExcedente = '0.00';

    /**
     * @var float
     *
     * @ORM\Column(name="thc_mondes", type="float")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $thcMondes;

    /**
     * @var float
     *
     * @ORM\Column(name="thc_monhas", type="float")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $thcMonhas;



    /**
     * Set thcFijo
     *
     * @param float $thcFijo
     *
     * @return Thcalcac2014
     */
    public function setThcFijo($thcFijo)
    {
        $this->thcFijo = $thcFijo;

        return $this;
    }

    /**
     * Get thcFijo
     *
     * @return float
     */
    public function getThcFijo()
    {
        return $this->thcFijo;
    }

    /**
     * Set thcAdicporc
     *
     * @param float $thcAdicporc
     *
     * @return Thcalcac2014
     */
    public function setThcAdicporc($thcAdicporc)
    {
        $this->thcAdicporc = $thcAdicporc;

        return $this;
    }

    /**
     * Get thcAdicporc
     *
     * @return float
     */
    public function getThcAdicporc()
    {
        return $this->thcAdicporc;
    }

    /**
     * Set thcExcedente
     *
     * @param float $thcExcedente
     *
     * @return Thcalcac2014
     */
    public function setThcExcedente($thcExcedente)
    {
        $this->thcExcedente = $thcExcedente;

        return $this;
    }

    /**
     * Get thcExcedente
     *
     * @return float
     */
    public function getThcExcedente()
    {
        return $this->thcExcedente;
    }

    /**
     * Set thcMondes
     *
     * @param float $thcMondes
     *
     * @return Thcalcac2014
     */
    public function setThcMondes($thcMondes)
    {
        $this->thcMondes = $thcMondes;

        return $this;
    }

    /**
     * Get thcMondes
     *
     * @return float
     */
    public function getThcMondes()
    {
        return $this->thcMondes;
    }

    /**
     * Set thcMonhas
     *
     * @param float $thcMonhas
     *
     * @return Thcalcac2014
     */
    public function setThcMonhas($thcMonhas)
    {
        $this->thcMonhas = $thcMonhas;

        return $this;
    }

    /**
     * Get thcMonhas
     *
     * @return float
     */
    public function getThcMonhas()
    {
        return $this->thcMonhas;
    }
}
