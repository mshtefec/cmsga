<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cieraper
 *
 * @ORM\Table(name="cieraper")
 * @ORM\Entity
 */
class Cieraper
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="INSTIT", type="string", length=2, nullable=true)
     */
    private $instit;

    /**
     * @var float
     *
     * @ORM\Column(name="AA_CIERRE", type="float", precision=53, scale=0, nullable=true)
     */
    private $aaCierre;

    /**
     * @var float
     *
     * @ORM\Column(name="MM_CIERRE", type="float", precision=53, scale=0, nullable=true)
     */
    private $mmCierre;

    /**
     * @var float
     *
     * @ORM\Column(name="FUNDICION", type="float", precision=53, scale=0, nullable=true)
     */
    private $fundicion;

    /**
     * @var float
     *
     * @ORM\Column(name="RESULTADOS", type="float", precision=53, scale=0, nullable=true)
     */
    private $resultados;

    /**
     * @var float
     *
     * @ORM\Column(name="CIERRE", type="float", precision=53, scale=0, nullable=true)
     */
    private $cierre;

    /**
     * @var float
     *
     * @ORM\Column(name="APERTURA", type="float", precision=53, scale=0, nullable=true)
     */
    private $apertura;



    /**
     * Set instit
     *
     * @param string $instit
     *
     * @return Cieraper
     */
    public function setInstit($instit)
    {
        $this->instit = $instit;

        return $this;
    }

    /**
     * Get instit
     *
     * @return string
     */
    public function getInstit()
    {
        return $this->instit;
    }

    /**
     * Set aaCierre
     *
     * @param float $aaCierre
     *
     * @return Cieraper
     */
    public function setAaCierre($aaCierre)
    {
        $this->aaCierre = $aaCierre;

        return $this;
    }

    /**
     * Get aaCierre
     *
     * @return float
     */
    public function getAaCierre()
    {
        return $this->aaCierre;
    }

    /**
     * Set mmCierre
     *
     * @param float $mmCierre
     *
     * @return Cieraper
     */
    public function setMmCierre($mmCierre)
    {
        $this->mmCierre = $mmCierre;

        return $this;
    }

    /**
     * Get mmCierre
     *
     * @return float
     */
    public function getMmCierre()
    {
        return $this->mmCierre;
    }

    /**
     * Set fundicion
     *
     * @param float $fundicion
     *
     * @return Cieraper
     */
    public function setFundicion($fundicion)
    {
        $this->fundicion = $fundicion;

        return $this;
    }

    /**
     * Get fundicion
     *
     * @return float
     */
    public function getFundicion()
    {
        return $this->fundicion;
    }

    /**
     * Set resultados
     *
     * @param float $resultados
     *
     * @return Cieraper
     */
    public function setResultados($resultados)
    {
        $this->resultados = $resultados;

        return $this;
    }

    /**
     * Get resultados
     *
     * @return float
     */
    public function getResultados()
    {
        return $this->resultados;
    }

    /**
     * Set cierre
     *
     * @param float $cierre
     *
     * @return Cieraper
     */
    public function setCierre($cierre)
    {
        $this->cierre = $cierre;

        return $this;
    }

    /**
     * Get cierre
     *
     * @return float
     */
    public function getCierre()
    {
        return $this->cierre;
    }

    /**
     * Set apertura
     *
     * @param float $apertura
     *
     * @return Cieraper
     */
    public function setApertura($apertura)
    {
        $this->apertura = $apertura;

        return $this;
    }

    /**
     * Get apertura
     *
     * @return float
     */
    public function getApertura()
    {
        return $this->apertura;
    }
}
