<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accesos
 *
 * @ORM\Table(name="accesos")
 * @ORM\Entity
 */
class Accesos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="acc_nroacc", type="integer", nullable=false)
     */
    private $accNroacc = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_descri", type="string", length=30, nullable=false)
     */
    private $accDescri = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_funcio", type="string", length=10, nullable=false)
     */
    private $accFuncio = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_nada", type="string", length=5, nullable=false)
     */
    private $accNada = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_fuente", type="string", length=8, nullable=false)
     */
    private $accFuente = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_accenc", type="string", length=1, nullable=false)
     */
    private $accAccenc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_accven", type="string", length=1, nullable=false)
     */
    private $accAccven = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_acccaj", type="string", length=1, nullable=false)
     */
    private $accAcccaj = '';

    /**
     * @var string
     *
     * @ORM\Column(name="acc_accbas", type="string", length=1, nullable=false)
     */
    private $accAccbas = '';



    /**
     * Set accNroacc
     *
     * @param integer $accNroacc
     *
     * @return Accesos
     */
    public function setAccNroacc($accNroacc)
    {
        $this->accNroacc = $accNroacc;

        return $this;
    }

    /**
     * Get accNroacc
     *
     * @return integer
     */
    public function getAccNroacc()
    {
        return $this->accNroacc;
    }

    /**
     * Set accDescri
     *
     * @param string $accDescri
     *
     * @return Accesos
     */
    public function setAccDescri($accDescri)
    {
        $this->accDescri = $accDescri;

        return $this;
    }

    /**
     * Get accDescri
     *
     * @return string
     */
    public function getAccDescri()
    {
        return $this->accDescri;
    }

    /**
     * Set accFuncio
     *
     * @param string $accFuncio
     *
     * @return Accesos
     */
    public function setAccFuncio($accFuncio)
    {
        $this->accFuncio = $accFuncio;

        return $this;
    }

    /**
     * Get accFuncio
     *
     * @return string
     */
    public function getAccFuncio()
    {
        return $this->accFuncio;
    }

    /**
     * Set accNada
     *
     * @param string $accNada
     *
     * @return Accesos
     */
    public function setAccNada($accNada)
    {
        $this->accNada = $accNada;

        return $this;
    }

    /**
     * Get accNada
     *
     * @return string
     */
    public function getAccNada()
    {
        return $this->accNada;
    }

    /**
     * Set accFuente
     *
     * @param string $accFuente
     *
     * @return Accesos
     */
    public function setAccFuente($accFuente)
    {
        $this->accFuente = $accFuente;

        return $this;
    }

    /**
     * Get accFuente
     *
     * @return string
     */
    public function getAccFuente()
    {
        return $this->accFuente;
    }

    /**
     * Set accAccenc
     *
     * @param string $accAccenc
     *
     * @return Accesos
     */
    public function setAccAccenc($accAccenc)
    {
        $this->accAccenc = $accAccenc;

        return $this;
    }

    /**
     * Get accAccenc
     *
     * @return string
     */
    public function getAccAccenc()
    {
        return $this->accAccenc;
    }

    /**
     * Set accAccven
     *
     * @param string $accAccven
     *
     * @return Accesos
     */
    public function setAccAccven($accAccven)
    {
        $this->accAccven = $accAccven;

        return $this;
    }

    /**
     * Get accAccven
     *
     * @return string
     */
    public function getAccAccven()
    {
        return $this->accAccven;
    }

    /**
     * Set accAcccaj
     *
     * @param string $accAcccaj
     *
     * @return Accesos
     */
    public function setAccAcccaj($accAcccaj)
    {
        $this->accAcccaj = $accAcccaj;

        return $this;
    }

    /**
     * Get accAcccaj
     *
     * @return string
     */
    public function getAccAcccaj()
    {
        return $this->accAcccaj;
    }

    /**
     * Set accAccbas
     *
     * @param string $accAccbas
     *
     * @return Accesos
     */
    public function setAccAccbas($accAccbas)
    {
        $this->accAccbas = $accAccbas;

        return $this;
    }

    /**
     * Get accAccbas
     *
     * @return string
     */
    public function getAccAccbas()
    {
        return $this->accAccbas;
    }
}
