<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Asdet
 *
 * @ORM\Table(name="asdet", indexes={@ORM\Index(name="skasiento", columns={"ASIENTO", "FECHA"})})
 * @ORM\Entity
 */
class Asdet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA", type="datetime", nullable=true)
     */
    private $fecha = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="INSTITUT", type="string", length=2, nullable=true)
     */
    private $institut;

    /**
     * @var string
     *
     * @ORM\Column(name="PROCESO", type="string", length=6, nullable=true)
     */
    private $proceso;

    /**
     * @var float
     *
     * @ORM\Column(name="COMPROB", type="float", precision=10, scale=0, nullable=true)
     */
    private $comprob = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="ASIENTO", type="float", precision=10, scale=0, nullable=true)
     */
    private $asiento = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="CUENTA", type="string", length=8, nullable=true)
     */
    private $cuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="MATRICULA", type="string", length=5, nullable=true)
     */
    private $matricula;

    /**
     * @var string
     *
     * @ORM\Column(name="ZONA", type="string", length=4, nullable=true)
     */
    private $zona;

    /**
     * @var float
     *
     * @ORM\Column(name="IMPORTE", type="float", precision=14, scale=2, nullable=true)
     */
    private $importe = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="COLUMNA", type="boolean", nullable=false)
     */
    private $columna;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="ESTADO", type="string", length=1, nullable=true)
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="MADRE", type="string", length=8, nullable=true)
     */
    private $madre;

    /**
     * @var string
     *
     * @ORM\Column(name="CAMPO1", type="string", length=1, nullable=true)
     */
    private $campo1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FCONCILIA", type="datetime", nullable=true)
     */
    private $fconcilia;



    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Asdet
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set institut
     *
     * @param string $institut
     *
     * @return Asdet
     */
    public function setInstitut($institut)
    {
        $this->institut = $institut;

        return $this;
    }

    /**
     * Get institut
     *
     * @return string
     */
    public function getInstitut()
    {
        return $this->institut;
    }

    /**
     * Set proceso
     *
     * @param string $proceso
     *
     * @return Asdet
     */
    public function setProceso($proceso)
    {
        $this->proceso = $proceso;

        return $this;
    }

    /**
     * Get proceso
     *
     * @return string
     */
    public function getProceso()
    {
        return $this->proceso;
    }

    /**
     * Set comprob
     *
     * @param float $comprob
     *
     * @return Asdet
     */
    public function setComprob($comprob)
    {
        $this->comprob = $comprob;

        return $this;
    }

    /**
     * Get comprob
     *
     * @return float
     */
    public function getComprob()
    {
        return $this->comprob;
    }

    /**
     * Set asiento
     *
     * @param float $asiento
     *
     * @return Asdet
     */
    public function setAsiento($asiento)
    {
        $this->asiento = $asiento;

        return $this;
    }

    /**
     * Get asiento
     *
     * @return float
     */
    public function getAsiento()
    {
        return $this->asiento;
    }

    /**
     * Set cuenta
     *
     * @param string $cuenta
     *
     * @return Asdet
     */
    public function setCuenta($cuenta)
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    /**
     * Get cuenta
     *
     * @return string
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * Set matricula
     *
     * @param string $matricula
     *
     * @return Asdet
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula
     *
     * @return string
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set zona
     *
     * @param string $zona
     *
     * @return Asdet
     */
    public function setZona($zona)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Get zona
     *
     * @return string
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set importe
     *
     * @param float $importe
     *
     * @return Asdet
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set columna
     *
     * @param boolean $columna
     *
     * @return Asdet
     */
    public function setColumna($columna)
    {
        $this->columna = $columna;

        return $this;
    }

    /**
     * Get columna
     *
     * @return boolean
     */
    public function getColumna()
    {
        return $this->columna;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Asdet
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Asdet
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set madre
     *
     * @param string $madre
     *
     * @return Asdet
     */
    public function setMadre($madre)
    {
        $this->madre = $madre;

        return $this;
    }

    /**
     * Get madre
     *
     * @return string
     */
    public function getMadre()
    {
        return $this->madre;
    }

    /**
     * Set campo1
     *
     * @param string $campo1
     *
     * @return Asdet
     */
    public function setCampo1($campo1)
    {
        $this->campo1 = $campo1;

        return $this;
    }

    /**
     * Get campo1
     *
     * @return string
     */
    public function getCampo1()
    {
        return $this->campo1;
    }

    /**
     * Set fconcilia
     *
     * @param \DateTime $fconcilia
     *
     * @return Asdet
     */
    public function setFconcilia($fconcilia)
    {
        $this->fconcilia = $fconcilia;

        return $this;
    }

    /**
     * Get fconcilia
     *
     * @return \DateTime
     */
    public function getFconcilia()
    {
        return $this->fconcilia;
    }
}
