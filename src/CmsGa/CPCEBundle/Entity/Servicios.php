<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Servicios
 *
 * @ORM\Table(name="servicios")
 * @ORM\Entity
 */
class Servicios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ser_corser", type="string", length=6, nullable=false)
     */
    private $serCorser = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ser_descripcion", type="string", length=50, nullable=false)
     */
    private $serDescripcion = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ser_fecha", type="datetime", nullable=false)
     */
    private $serFecha = '0000-00-00 00:00:00';

    /**
     * @var string
     *
     * @ORM\Column(name="ser_importe", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $serImporte = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="ser_nrocta", type="string", length=9, nullable=false)
     */
    private $serNrocta = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ser_proceso", type="string", length=6, nullable=false)
     */
    private $serProceso = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ser_devenga", type="string", length=1, nullable=false)
     */
    private $serDevenga = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="ser_lote", type="bigint", nullable=false)
     */
    private $serLote = '0';



    /**
     * Set serCorser
     *
     * @param string $serCorser
     *
     * @return Servicios
     */
    public function setSerCorser($serCorser)
    {
        $this->serCorser = $serCorser;

        return $this;
    }

    /**
     * Get serCorser
     *
     * @return string
     */
    public function getSerCorser()
    {
        return $this->serCorser;
    }

    /**
     * Set serDescripcion
     *
     * @param string $serDescripcion
     *
     * @return Servicios
     */
    public function setSerDescripcion($serDescripcion)
    {
        $this->serDescripcion = $serDescripcion;

        return $this;
    }

    /**
     * Get serDescripcion
     *
     * @return string
     */
    public function getSerDescripcion()
    {
        return $this->serDescripcion;
    }

    /**
     * Set serFecha
     *
     * @param \DateTime $serFecha
     *
     * @return Servicios
     */
    public function setSerFecha($serFecha)
    {
        $this->serFecha = $serFecha;

        return $this;
    }

    /**
     * Get serFecha
     *
     * @return \DateTime
     */
    public function getSerFecha()
    {
        return $this->serFecha;
    }

    /**
     * Set serImporte
     *
     * @param string $serImporte
     *
     * @return Servicios
     */
    public function setSerImporte($serImporte)
    {
        $this->serImporte = $serImporte;

        return $this;
    }

    /**
     * Get serImporte
     *
     * @return string
     */
    public function getSerImporte()
    {
        return $this->serImporte;
    }

    /**
     * Set serNrocta
     *
     * @param string $serNrocta
     *
     * @return Servicios
     */
    public function setSerNrocta($serNrocta)
    {
        $this->serNrocta = $serNrocta;

        return $this;
    }

    /**
     * Get serNrocta
     *
     * @return string
     */
    public function getSerNrocta()
    {
        return $this->serNrocta;
    }

    /**
     * Set serProceso
     *
     * @param string $serProceso
     *
     * @return Servicios
     */
    public function setSerProceso($serProceso)
    {
        $this->serProceso = $serProceso;

        return $this;
    }

    /**
     * Get serProceso
     *
     * @return string
     */
    public function getSerProceso()
    {
        return $this->serProceso;
    }

    /**
     * Set serDevenga
     *
     * @param string $serDevenga
     *
     * @return Servicios
     */
    public function setSerDevenga($serDevenga)
    {
        $this->serDevenga = $serDevenga;

        return $this;
    }

    /**
     * Get serDevenga
     *
     * @return string
     */
    public function getSerDevenga()
    {
        return $this->serDevenga;
    }

    /**
     * Set serLote
     *
     * @param integer $serLote
     *
     * @return Servicios
     */
    public function setSerLote($serLote)
    {
        $this->serLote = $serLote;

        return $this;
    }

    /**
     * Get serLote
     *
     * @return integer
     */
    public function getSerLote()
    {
        return $this->serLote;
    }
}
