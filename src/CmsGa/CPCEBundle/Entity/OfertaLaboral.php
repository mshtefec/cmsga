<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OfertaLaboral
 *
 * @ORM\Table(name="oferta_laboral")
 * @ORM\Entity
 */
class OfertaLaboral
{
    /**
     * @var string
     *
     * @ORM\Column(name="solicitante", type="string", length=255, nullable=false)
     */
    private $solicitante;

    /**
     * @var string
     *
     * @ORM\Column(name="requiere", type="string", length=255, nullable=false)
     */
    private $requiere;

    /**
     * @var string
     *
     * @ORM\Column(name="puesto", type="string", length=255, nullable=false)
     */
    private $puesto;

    /**
     * @var string
     *
     * @ORM\Column(name="requisito", type="text", nullable=false)
     */
    private $requisito;

    /**
     * @var string
     *
     * @ORM\Column(name="tarea", type="text", nullable=false)
     */
    private $tarea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="date", nullable=false)
     */
    private $fechafin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @var integer
     *
     * @ORM\Column(name="delegacion", type="integer", nullable=false)
     */
    private $delegacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="CmsGa\CPCEBundle\Entity\UserCurriculum", inversedBy="ofertaLaboral")
     * @ORM\JoinTable(name="ofertalaborals_cvpostulantes",
     *   joinColumns={
     *     @ORM\JoinColumn(name="oferta_laboral_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_curriculum_id", referencedColumnName="id")
     *   }
     * )
     */
    private $userCurriculum;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userCurriculum = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set solicitante
     *
     * @param string $solicitante
     *
     * @return OfertaLaboral
     */
    public function setSolicitante($solicitante)
    {
        $this->solicitante = $solicitante;

        return $this;
    }

    /**
     * Get solicitante
     *
     * @return string
     */
    public function getSolicitante()
    {
        return $this->solicitante;
    }

    /**
     * Set requiere
     *
     * @param string $requiere
     *
     * @return OfertaLaboral
     */
    public function setRequiere($requiere)
    {
        $this->requiere = $requiere;

        return $this;
    }

    /**
     * Get requiere
     *
     * @return string
     */
    public function getRequiere()
    {
        return $this->requiere;
    }

    /**
     * Set puesto
     *
     * @param string $puesto
     *
     * @return OfertaLaboral
     */
    public function setPuesto($puesto)
    {
        $this->puesto = $puesto;

        return $this;
    }

    /**
     * Get puesto
     *
     * @return string
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Set requisito
     *
     * @param string $requisito
     *
     * @return OfertaLaboral
     */
    public function setRequisito($requisito)
    {
        $this->requisito = $requisito;

        return $this;
    }

    /**
     * Get requisito
     *
     * @return string
     */
    public function getRequisito()
    {
        return $this->requisito;
    }

    /**
     * Set tarea
     *
     * @param string $tarea
     *
     * @return OfertaLaboral
     */
    public function setTarea($tarea)
    {
        $this->tarea = $tarea;

        return $this;
    }

    /**
     * Get tarea
     *
     * @return string
     */
    public function getTarea()
    {
        return $this->tarea;
    }

    /**
     * Set fechafin
     *
     * @param \DateTime $fechafin
     *
     * @return OfertaLaboral
     */
    public function setFechafin($fechafin)
    {
        $this->fechafin = $fechafin;

        return $this;
    }

    /**
     * Get fechafin
     *
     * @return \DateTime
     */
    public function getFechafin()
    {
        return $this->fechafin;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return OfertaLaboral
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set delegacion
     *
     * @param integer $delegacion
     *
     * @return OfertaLaboral
     */
    public function setDelegacion($delegacion)
    {
        $this->delegacion = $delegacion;

        return $this;
    }

    /**
     * Get delegacion
     *
     * @return integer
     */
    public function getDelegacion()
    {
        return $this->delegacion;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return OfertaLaboral
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return OfertaLaboral
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return OfertaLaboral
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return OfertaLaboral
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return OfertaLaboral
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add userCurriculum
     *
     * @param \CmsGa\CPCEBundle\Entity\UserCurriculum $userCurriculum
     *
     * @return OfertaLaboral
     */
    public function addUserCurriculum(\CmsGa\CPCEBundle\Entity\UserCurriculum $userCurriculum)
    {
        $this->userCurriculum[] = $userCurriculum;

        return $this;
    }

    /**
     * Remove userCurriculum
     *
     * @param \CmsGa\CPCEBundle\Entity\UserCurriculum $userCurriculum
     */
    public function removeUserCurriculum(\CmsGa\CPCEBundle\Entity\UserCurriculum $userCurriculum)
    {
        $this->userCurriculum->removeElement($userCurriculum);
    }

    /**
     * Get userCurriculum
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCurriculum()
    {
        return $this->userCurriculum;
    }
}
