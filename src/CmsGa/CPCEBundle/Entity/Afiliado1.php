<?php

namespace CmsGa\CPCEBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Afiliado1
 *
 * @ORM\Table(name="afiliado1")
 * @ORM\Entity
 */
class Afiliado1
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_matricula", type="string", length=5, nullable=true)
     */
    private $afiMatricula = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_nombre", type="string", length=30, nullable=true)
     */
    private $afiNombre = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="afi_fecnac", type="date", nullable=true)
     */
    private $afiFecnac = '0000-00-00';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_tipdoc", type="string", length=3, nullable=true)
     */
    private $afiTipdoc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_nrodoc", type="string", length=8, nullable=true)
     */
    private $afiNrodoc = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_direccion", type="string", length=50, nullable=true)
     */
    private $afiDireccion = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_localidad", type="string", length=20, nullable=true)
     */
    private $afiLocalidad = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_provincia", type="string", length=4, nullable=true)
     */
    private $afiProvincia = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_zona", type="string", length=4, nullable=true)
     */
    private $afiZona = '';

    /**
     * @var integer
     *
     * @ORM\Column(name="afi_codpos", type="integer", nullable=true)
     */
    private $afiCodpos = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_telefono1", type="string", length=50, nullable=true)
     */
    private $afiTelefono1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_telefono2", type="string", length=50, nullable=true)
     */
    private $afiTelefono2 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_mail", type="string", length=50, nullable=true)
     */
    private $afiMail = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_civil", type="string", length=2, nullable=true)
     */
    private $afiCivil = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_titulo", type="string", length=3, nullable=true)
     */
    private $afiTitulo = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="afi_fecgraduacion", type="date", nullable=true)
     */
    private $afiFecgraduacion = '0000-00-00';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_cuit", type="string", length=15, nullable=true)
     */
    private $afiCuit = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_dgr", type="string", length=15, nullable=true)
     */
    private $afiDgr = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_categoria", type="string", length=6, nullable=true)
     */
    private $afiCategoria = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_ficha", type="string", length=1, nullable=true)
     */
    private $afiFicha = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="afi_fecficha", type="date", nullable=true)
     */
    private $afiFecficha = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="afi_fecdiploma", type="date", nullable=true)
     */
    private $afiFecdiploma = '0000-00-00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="afi_fecmatricula", type="date", nullable=true)
     */
    private $afiFecmatricula = '0000-00-00';

    /**
     * @var float
     *
     * @ORM\Column(name="EJPROF", type="float", precision=53, scale=0, nullable=true)
     */
    private $ejprof;

    /**
     * @var float
     *
     * @ORM\Column(name="ASOC", type="float", precision=53, scale=0, nullable=true)
     */
    private $asoc;

    /**
     * @var float
     *
     * @ORM\Column(name="MONTO", type="float", precision=53, scale=0, nullable=true)
     */
    private $monto;

    /**
     * @var string
     *
     * @ORM\Column(name="afi_sexo", type="string", length=1, nullable=true)
     */
    private $afiSexo = '';

    /**
     * @var string
     *
     * @ORM\Column(name="afi_garante", type="string", length=60, nullable=true)
     */
    private $afiGarante = '';

    /**
     * @var string
     *
     * @ORM\Column(name="DOC1", type="string", length=8, nullable=true)
     */
    private $doc1;

    /**
     * @var string
     *
     * @ORM\Column(name="AUT1", type="string", length=35, nullable=true)
     */
    private $aut1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NAC1", type="datetime", nullable=true)
     */
    private $nac1;

    /**
     * @var string
     *
     * @ORM\Column(name="SEX1", type="string", length=1, nullable=true)
     */
    private $sex1;

    /**
     * @var string
     *
     * @ORM\Column(name="FIL1", type="string", length=1, nullable=true)
     */
    private $fil1;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC2", type="string", length=8, nullable=true)
     */
    private $doc2;

    /**
     * @var string
     *
     * @ORM\Column(name="AUT2", type="string", length=35, nullable=true)
     */
    private $aut2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NAC2", type="datetime", nullable=true)
     */
    private $nac2;

    /**
     * @var string
     *
     * @ORM\Column(name="SEX2", type="string", length=1, nullable=true)
     */
    private $sex2;

    /**
     * @var string
     *
     * @ORM\Column(name="FIL2", type="string", length=1, nullable=true)
     */
    private $fil2;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC3", type="string", length=8, nullable=true)
     */
    private $doc3;

    /**
     * @var string
     *
     * @ORM\Column(name="AUT3", type="string", length=35, nullable=true)
     */
    private $aut3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NAC3", type="datetime", nullable=true)
     */
    private $nac3;

    /**
     * @var string
     *
     * @ORM\Column(name="SEX3", type="string", length=1, nullable=true)
     */
    private $sex3;

    /**
     * @var string
     *
     * @ORM\Column(name="FIL3", type="string", length=1, nullable=true)
     */
    private $fil3;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC4", type="string", length=8, nullable=true)
     */
    private $doc4;

    /**
     * @var string
     *
     * @ORM\Column(name="AUT4", type="string", length=35, nullable=true)
     */
    private $aut4;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NAC4", type="datetime", nullable=true)
     */
    private $nac4;

    /**
     * @var string
     *
     * @ORM\Column(name="SEX4", type="string", length=1, nullable=true)
     */
    private $sex4;

    /**
     * @var string
     *
     * @ORM\Column(name="FIL4", type="string", length=1, nullable=true)
     */
    private $fil4;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC5", type="string", length=8, nullable=true)
     */
    private $doc5;

    /**
     * @var string
     *
     * @ORM\Column(name="AUT5", type="string", length=35, nullable=true)
     */
    private $aut5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NAC5", type="datetime", nullable=true)
     */
    private $nac5;

    /**
     * @var string
     *
     * @ORM\Column(name="SEX5", type="string", length=1, nullable=true)
     */
    private $sex5;

    /**
     * @var string
     *
     * @ORM\Column(name="FIL5", type="string", length=1, nullable=true)
     */
    private $fil5;

    /**
     * @var string
     *
     * @ORM\Column(name="DOC6", type="string", length=8, nullable=true)
     */
    private $doc6;

    /**
     * @var string
     *
     * @ORM\Column(name="AUT6", type="string", length=35, nullable=true)
     */
    private $aut6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="NAC6", type="datetime", nullable=true)
     */
    private $nac6;

    /**
     * @var string
     *
     * @ORM\Column(name="SEX6", type="string", length=1, nullable=true)
     */
    private $sex6;

    /**
     * @var string
     *
     * @ORM\Column(name="FIL6", type="string", length=1, nullable=true)
     */
    private $fil6;

    /**
     * @var float
     *
     * @ORM\Column(name="DGRALIC", type="float", precision=53, scale=0, nullable=true)
     */
    private $dgralic;

    /**
     * @var float
     *
     * @ORM\Column(name="CUOTA", type="float", precision=53, scale=0, nullable=true)
     */
    private $cuota;

    /**
     * @var float
     *
     * @ORM\Column(name="DEBITOS", type="float", precision=53, scale=0, nullable=true)
     */
    private $debitos;

    /**
     * @var float
     *
     * @ORM\Column(name="CREDITOS", type="float", precision=53, scale=0, nullable=true)
     */
    private $creditos;

    /**
     * @var string
     *
     * @ORM\Column(name="GANANCIAS", type="string", length=1, nullable=true)
     */
    private $ganancias;

    /**
     * @var string
     *
     * @ORM\Column(name="IVA", type="string", length=1, nullable=true)
     */
    private $iva;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DGREXCEP", type="datetime", nullable=true)
     */
    private $dgrexcep;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DGIEXCEP", type="datetime", nullable=true)
     */
    private $dgiexcep;



    /**
     * Set afiMatricula
     *
     * @param string $afiMatricula
     *
     * @return Afiliado1
     */
    public function setAfiMatricula($afiMatricula)
    {
        $this->afiMatricula = $afiMatricula;

        return $this;
    }

    /**
     * Get afiMatricula
     *
     * @return string
     */
    public function getAfiMatricula()
    {
        return $this->afiMatricula;
    }

    /**
     * Set afiNombre
     *
     * @param string $afiNombre
     *
     * @return Afiliado1
     */
    public function setAfiNombre($afiNombre)
    {
        $this->afiNombre = $afiNombre;

        return $this;
    }

    /**
     * Get afiNombre
     *
     * @return string
     */
    public function getAfiNombre()
    {
        return $this->afiNombre;
    }

    /**
     * Set afiFecnac
     *
     * @param \DateTime $afiFecnac
     *
     * @return Afiliado1
     */
    public function setAfiFecnac($afiFecnac)
    {
        $this->afiFecnac = $afiFecnac;

        return $this;
    }

    /**
     * Get afiFecnac
     *
     * @return \DateTime
     */
    public function getAfiFecnac()
    {
        return $this->afiFecnac;
    }

    /**
     * Set afiTipdoc
     *
     * @param string $afiTipdoc
     *
     * @return Afiliado1
     */
    public function setAfiTipdoc($afiTipdoc)
    {
        $this->afiTipdoc = $afiTipdoc;

        return $this;
    }

    /**
     * Get afiTipdoc
     *
     * @return string
     */
    public function getAfiTipdoc()
    {
        return $this->afiTipdoc;
    }

    /**
     * Set afiNrodoc
     *
     * @param string $afiNrodoc
     *
     * @return Afiliado1
     */
    public function setAfiNrodoc($afiNrodoc)
    {
        $this->afiNrodoc = $afiNrodoc;

        return $this;
    }

    /**
     * Get afiNrodoc
     *
     * @return string
     */
    public function getAfiNrodoc()
    {
        return $this->afiNrodoc;
    }

    /**
     * Set afiDireccion
     *
     * @param string $afiDireccion
     *
     * @return Afiliado1
     */
    public function setAfiDireccion($afiDireccion)
    {
        $this->afiDireccion = $afiDireccion;

        return $this;
    }

    /**
     * Get afiDireccion
     *
     * @return string
     */
    public function getAfiDireccion()
    {
        return $this->afiDireccion;
    }

    /**
     * Set afiLocalidad
     *
     * @param string $afiLocalidad
     *
     * @return Afiliado1
     */
    public function setAfiLocalidad($afiLocalidad)
    {
        $this->afiLocalidad = $afiLocalidad;

        return $this;
    }

    /**
     * Get afiLocalidad
     *
     * @return string
     */
    public function getAfiLocalidad()
    {
        return $this->afiLocalidad;
    }

    /**
     * Set afiProvincia
     *
     * @param string $afiProvincia
     *
     * @return Afiliado1
     */
    public function setAfiProvincia($afiProvincia)
    {
        $this->afiProvincia = $afiProvincia;

        return $this;
    }

    /**
     * Get afiProvincia
     *
     * @return string
     */
    public function getAfiProvincia()
    {
        return $this->afiProvincia;
    }

    /**
     * Set afiZona
     *
     * @param string $afiZona
     *
     * @return Afiliado1
     */
    public function setAfiZona($afiZona)
    {
        $this->afiZona = $afiZona;

        return $this;
    }

    /**
     * Get afiZona
     *
     * @return string
     */
    public function getAfiZona()
    {
        return $this->afiZona;
    }

    /**
     * Set afiCodpos
     *
     * @param integer $afiCodpos
     *
     * @return Afiliado1
     */
    public function setAfiCodpos($afiCodpos)
    {
        $this->afiCodpos = $afiCodpos;

        return $this;
    }

    /**
     * Get afiCodpos
     *
     * @return integer
     */
    public function getAfiCodpos()
    {
        return $this->afiCodpos;
    }

    /**
     * Set afiTelefono1
     *
     * @param string $afiTelefono1
     *
     * @return Afiliado1
     */
    public function setAfiTelefono1($afiTelefono1)
    {
        $this->afiTelefono1 = $afiTelefono1;

        return $this;
    }

    /**
     * Get afiTelefono1
     *
     * @return string
     */
    public function getAfiTelefono1()
    {
        return $this->afiTelefono1;
    }

    /**
     * Set afiTelefono2
     *
     * @param string $afiTelefono2
     *
     * @return Afiliado1
     */
    public function setAfiTelefono2($afiTelefono2)
    {
        $this->afiTelefono2 = $afiTelefono2;

        return $this;
    }

    /**
     * Get afiTelefono2
     *
     * @return string
     */
    public function getAfiTelefono2()
    {
        return $this->afiTelefono2;
    }

    /**
     * Set afiMail
     *
     * @param string $afiMail
     *
     * @return Afiliado1
     */
    public function setAfiMail($afiMail)
    {
        $this->afiMail = $afiMail;

        return $this;
    }

    /**
     * Get afiMail
     *
     * @return string
     */
    public function getAfiMail()
    {
        return $this->afiMail;
    }

    /**
     * Set afiCivil
     *
     * @param string $afiCivil
     *
     * @return Afiliado1
     */
    public function setAfiCivil($afiCivil)
    {
        $this->afiCivil = $afiCivil;

        return $this;
    }

    /**
     * Get afiCivil
     *
     * @return string
     */
    public function getAfiCivil()
    {
        return $this->afiCivil;
    }

    /**
     * Set afiTitulo
     *
     * @param string $afiTitulo
     *
     * @return Afiliado1
     */
    public function setAfiTitulo($afiTitulo)
    {
        $this->afiTitulo = $afiTitulo;

        return $this;
    }

    /**
     * Get afiTitulo
     *
     * @return string
     */
    public function getAfiTitulo()
    {
        return $this->afiTitulo;
    }

    /**
     * Set afiFecgraduacion
     *
     * @param \DateTime $afiFecgraduacion
     *
     * @return Afiliado1
     */
    public function setAfiFecgraduacion($afiFecgraduacion)
    {
        $this->afiFecgraduacion = $afiFecgraduacion;

        return $this;
    }

    /**
     * Get afiFecgraduacion
     *
     * @return \DateTime
     */
    public function getAfiFecgraduacion()
    {
        return $this->afiFecgraduacion;
    }

    /**
     * Set afiCuit
     *
     * @param string $afiCuit
     *
     * @return Afiliado1
     */
    public function setAfiCuit($afiCuit)
    {
        $this->afiCuit = $afiCuit;

        return $this;
    }

    /**
     * Get afiCuit
     *
     * @return string
     */
    public function getAfiCuit()
    {
        return $this->afiCuit;
    }

    /**
     * Set afiDgr
     *
     * @param string $afiDgr
     *
     * @return Afiliado1
     */
    public function setAfiDgr($afiDgr)
    {
        $this->afiDgr = $afiDgr;

        return $this;
    }

    /**
     * Get afiDgr
     *
     * @return string
     */
    public function getAfiDgr()
    {
        return $this->afiDgr;
    }

    /**
     * Set afiCategoria
     *
     * @param string $afiCategoria
     *
     * @return Afiliado1
     */
    public function setAfiCategoria($afiCategoria)
    {
        $this->afiCategoria = $afiCategoria;

        return $this;
    }

    /**
     * Get afiCategoria
     *
     * @return string
     */
    public function getAfiCategoria()
    {
        return $this->afiCategoria;
    }

    /**
     * Set afiFicha
     *
     * @param string $afiFicha
     *
     * @return Afiliado1
     */
    public function setAfiFicha($afiFicha)
    {
        $this->afiFicha = $afiFicha;

        return $this;
    }

    /**
     * Get afiFicha
     *
     * @return string
     */
    public function getAfiFicha()
    {
        return $this->afiFicha;
    }

    /**
     * Set afiFecficha
     *
     * @param \DateTime $afiFecficha
     *
     * @return Afiliado1
     */
    public function setAfiFecficha($afiFecficha)
    {
        $this->afiFecficha = $afiFecficha;

        return $this;
    }

    /**
     * Get afiFecficha
     *
     * @return \DateTime
     */
    public function getAfiFecficha()
    {
        return $this->afiFecficha;
    }

    /**
     * Set afiFecdiploma
     *
     * @param \DateTime $afiFecdiploma
     *
     * @return Afiliado1
     */
    public function setAfiFecdiploma($afiFecdiploma)
    {
        $this->afiFecdiploma = $afiFecdiploma;

        return $this;
    }

    /**
     * Get afiFecdiploma
     *
     * @return \DateTime
     */
    public function getAfiFecdiploma()
    {
        return $this->afiFecdiploma;
    }

    /**
     * Set afiFecmatricula
     *
     * @param \DateTime $afiFecmatricula
     *
     * @return Afiliado1
     */
    public function setAfiFecmatricula($afiFecmatricula)
    {
        $this->afiFecmatricula = $afiFecmatricula;

        return $this;
    }

    /**
     * Get afiFecmatricula
     *
     * @return \DateTime
     */
    public function getAfiFecmatricula()
    {
        return $this->afiFecmatricula;
    }

    /**
     * Set ejprof
     *
     * @param float $ejprof
     *
     * @return Afiliado1
     */
    public function setEjprof($ejprof)
    {
        $this->ejprof = $ejprof;

        return $this;
    }

    /**
     * Get ejprof
     *
     * @return float
     */
    public function getEjprof()
    {
        return $this->ejprof;
    }

    /**
     * Set asoc
     *
     * @param float $asoc
     *
     * @return Afiliado1
     */
    public function setAsoc($asoc)
    {
        $this->asoc = $asoc;

        return $this;
    }

    /**
     * Get asoc
     *
     * @return float
     */
    public function getAsoc()
    {
        return $this->asoc;
    }

    /**
     * Set monto
     *
     * @param float $monto
     *
     * @return Afiliado1
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set afiSexo
     *
     * @param string $afiSexo
     *
     * @return Afiliado1
     */
    public function setAfiSexo($afiSexo)
    {
        $this->afiSexo = $afiSexo;

        return $this;
    }

    /**
     * Get afiSexo
     *
     * @return string
     */
    public function getAfiSexo()
    {
        return $this->afiSexo;
    }

    /**
     * Set afiGarante
     *
     * @param string $afiGarante
     *
     * @return Afiliado1
     */
    public function setAfiGarante($afiGarante)
    {
        $this->afiGarante = $afiGarante;

        return $this;
    }

    /**
     * Get afiGarante
     *
     * @return string
     */
    public function getAfiGarante()
    {
        return $this->afiGarante;
    }

    /**
     * Set doc1
     *
     * @param string $doc1
     *
     * @return Afiliado1
     */
    public function setDoc1($doc1)
    {
        $this->doc1 = $doc1;

        return $this;
    }

    /**
     * Get doc1
     *
     * @return string
     */
    public function getDoc1()
    {
        return $this->doc1;
    }

    /**
     * Set aut1
     *
     * @param string $aut1
     *
     * @return Afiliado1
     */
    public function setAut1($aut1)
    {
        $this->aut1 = $aut1;

        return $this;
    }

    /**
     * Get aut1
     *
     * @return string
     */
    public function getAut1()
    {
        return $this->aut1;
    }

    /**
     * Set nac1
     *
     * @param \DateTime $nac1
     *
     * @return Afiliado1
     */
    public function setNac1($nac1)
    {
        $this->nac1 = $nac1;

        return $this;
    }

    /**
     * Get nac1
     *
     * @return \DateTime
     */
    public function getNac1()
    {
        return $this->nac1;
    }

    /**
     * Set sex1
     *
     * @param string $sex1
     *
     * @return Afiliado1
     */
    public function setSex1($sex1)
    {
        $this->sex1 = $sex1;

        return $this;
    }

    /**
     * Get sex1
     *
     * @return string
     */
    public function getSex1()
    {
        return $this->sex1;
    }

    /**
     * Set fil1
     *
     * @param string $fil1
     *
     * @return Afiliado1
     */
    public function setFil1($fil1)
    {
        $this->fil1 = $fil1;

        return $this;
    }

    /**
     * Get fil1
     *
     * @return string
     */
    public function getFil1()
    {
        return $this->fil1;
    }

    /**
     * Set doc2
     *
     * @param string $doc2
     *
     * @return Afiliado1
     */
    public function setDoc2($doc2)
    {
        $this->doc2 = $doc2;

        return $this;
    }

    /**
     * Get doc2
     *
     * @return string
     */
    public function getDoc2()
    {
        return $this->doc2;
    }

    /**
     * Set aut2
     *
     * @param string $aut2
     *
     * @return Afiliado1
     */
    public function setAut2($aut2)
    {
        $this->aut2 = $aut2;

        return $this;
    }

    /**
     * Get aut2
     *
     * @return string
     */
    public function getAut2()
    {
        return $this->aut2;
    }

    /**
     * Set nac2
     *
     * @param \DateTime $nac2
     *
     * @return Afiliado1
     */
    public function setNac2($nac2)
    {
        $this->nac2 = $nac2;

        return $this;
    }

    /**
     * Get nac2
     *
     * @return \DateTime
     */
    public function getNac2()
    {
        return $this->nac2;
    }

    /**
     * Set sex2
     *
     * @param string $sex2
     *
     * @return Afiliado1
     */
    public function setSex2($sex2)
    {
        $this->sex2 = $sex2;

        return $this;
    }

    /**
     * Get sex2
     *
     * @return string
     */
    public function getSex2()
    {
        return $this->sex2;
    }

    /**
     * Set fil2
     *
     * @param string $fil2
     *
     * @return Afiliado1
     */
    public function setFil2($fil2)
    {
        $this->fil2 = $fil2;

        return $this;
    }

    /**
     * Get fil2
     *
     * @return string
     */
    public function getFil2()
    {
        return $this->fil2;
    }

    /**
     * Set doc3
     *
     * @param string $doc3
     *
     * @return Afiliado1
     */
    public function setDoc3($doc3)
    {
        $this->doc3 = $doc3;

        return $this;
    }

    /**
     * Get doc3
     *
     * @return string
     */
    public function getDoc3()
    {
        return $this->doc3;
    }

    /**
     * Set aut3
     *
     * @param string $aut3
     *
     * @return Afiliado1
     */
    public function setAut3($aut3)
    {
        $this->aut3 = $aut3;

        return $this;
    }

    /**
     * Get aut3
     *
     * @return string
     */
    public function getAut3()
    {
        return $this->aut3;
    }

    /**
     * Set nac3
     *
     * @param \DateTime $nac3
     *
     * @return Afiliado1
     */
    public function setNac3($nac3)
    {
        $this->nac3 = $nac3;

        return $this;
    }

    /**
     * Get nac3
     *
     * @return \DateTime
     */
    public function getNac3()
    {
        return $this->nac3;
    }

    /**
     * Set sex3
     *
     * @param string $sex3
     *
     * @return Afiliado1
     */
    public function setSex3($sex3)
    {
        $this->sex3 = $sex3;

        return $this;
    }

    /**
     * Get sex3
     *
     * @return string
     */
    public function getSex3()
    {
        return $this->sex3;
    }

    /**
     * Set fil3
     *
     * @param string $fil3
     *
     * @return Afiliado1
     */
    public function setFil3($fil3)
    {
        $this->fil3 = $fil3;

        return $this;
    }

    /**
     * Get fil3
     *
     * @return string
     */
    public function getFil3()
    {
        return $this->fil3;
    }

    /**
     * Set doc4
     *
     * @param string $doc4
     *
     * @return Afiliado1
     */
    public function setDoc4($doc4)
    {
        $this->doc4 = $doc4;

        return $this;
    }

    /**
     * Get doc4
     *
     * @return string
     */
    public function getDoc4()
    {
        return $this->doc4;
    }

    /**
     * Set aut4
     *
     * @param string $aut4
     *
     * @return Afiliado1
     */
    public function setAut4($aut4)
    {
        $this->aut4 = $aut4;

        return $this;
    }

    /**
     * Get aut4
     *
     * @return string
     */
    public function getAut4()
    {
        return $this->aut4;
    }

    /**
     * Set nac4
     *
     * @param \DateTime $nac4
     *
     * @return Afiliado1
     */
    public function setNac4($nac4)
    {
        $this->nac4 = $nac4;

        return $this;
    }

    /**
     * Get nac4
     *
     * @return \DateTime
     */
    public function getNac4()
    {
        return $this->nac4;
    }

    /**
     * Set sex4
     *
     * @param string $sex4
     *
     * @return Afiliado1
     */
    public function setSex4($sex4)
    {
        $this->sex4 = $sex4;

        return $this;
    }

    /**
     * Get sex4
     *
     * @return string
     */
    public function getSex4()
    {
        return $this->sex4;
    }

    /**
     * Set fil4
     *
     * @param string $fil4
     *
     * @return Afiliado1
     */
    public function setFil4($fil4)
    {
        $this->fil4 = $fil4;

        return $this;
    }

    /**
     * Get fil4
     *
     * @return string
     */
    public function getFil4()
    {
        return $this->fil4;
    }

    /**
     * Set doc5
     *
     * @param string $doc5
     *
     * @return Afiliado1
     */
    public function setDoc5($doc5)
    {
        $this->doc5 = $doc5;

        return $this;
    }

    /**
     * Get doc5
     *
     * @return string
     */
    public function getDoc5()
    {
        return $this->doc5;
    }

    /**
     * Set aut5
     *
     * @param string $aut5
     *
     * @return Afiliado1
     */
    public function setAut5($aut5)
    {
        $this->aut5 = $aut5;

        return $this;
    }

    /**
     * Get aut5
     *
     * @return string
     */
    public function getAut5()
    {
        return $this->aut5;
    }

    /**
     * Set nac5
     *
     * @param \DateTime $nac5
     *
     * @return Afiliado1
     */
    public function setNac5($nac5)
    {
        $this->nac5 = $nac5;

        return $this;
    }

    /**
     * Get nac5
     *
     * @return \DateTime
     */
    public function getNac5()
    {
        return $this->nac5;
    }

    /**
     * Set sex5
     *
     * @param string $sex5
     *
     * @return Afiliado1
     */
    public function setSex5($sex5)
    {
        $this->sex5 = $sex5;

        return $this;
    }

    /**
     * Get sex5
     *
     * @return string
     */
    public function getSex5()
    {
        return $this->sex5;
    }

    /**
     * Set fil5
     *
     * @param string $fil5
     *
     * @return Afiliado1
     */
    public function setFil5($fil5)
    {
        $this->fil5 = $fil5;

        return $this;
    }

    /**
     * Get fil5
     *
     * @return string
     */
    public function getFil5()
    {
        return $this->fil5;
    }

    /**
     * Set doc6
     *
     * @param string $doc6
     *
     * @return Afiliado1
     */
    public function setDoc6($doc6)
    {
        $this->doc6 = $doc6;

        return $this;
    }

    /**
     * Get doc6
     *
     * @return string
     */
    public function getDoc6()
    {
        return $this->doc6;
    }

    /**
     * Set aut6
     *
     * @param string $aut6
     *
     * @return Afiliado1
     */
    public function setAut6($aut6)
    {
        $this->aut6 = $aut6;

        return $this;
    }

    /**
     * Get aut6
     *
     * @return string
     */
    public function getAut6()
    {
        return $this->aut6;
    }

    /**
     * Set nac6
     *
     * @param \DateTime $nac6
     *
     * @return Afiliado1
     */
    public function setNac6($nac6)
    {
        $this->nac6 = $nac6;

        return $this;
    }

    /**
     * Get nac6
     *
     * @return \DateTime
     */
    public function getNac6()
    {
        return $this->nac6;
    }

    /**
     * Set sex6
     *
     * @param string $sex6
     *
     * @return Afiliado1
     */
    public function setSex6($sex6)
    {
        $this->sex6 = $sex6;

        return $this;
    }

    /**
     * Get sex6
     *
     * @return string
     */
    public function getSex6()
    {
        return $this->sex6;
    }

    /**
     * Set fil6
     *
     * @param string $fil6
     *
     * @return Afiliado1
     */
    public function setFil6($fil6)
    {
        $this->fil6 = $fil6;

        return $this;
    }

    /**
     * Get fil6
     *
     * @return string
     */
    public function getFil6()
    {
        return $this->fil6;
    }

    /**
     * Set dgralic
     *
     * @param float $dgralic
     *
     * @return Afiliado1
     */
    public function setDgralic($dgralic)
    {
        $this->dgralic = $dgralic;

        return $this;
    }

    /**
     * Get dgralic
     *
     * @return float
     */
    public function getDgralic()
    {
        return $this->dgralic;
    }

    /**
     * Set cuota
     *
     * @param float $cuota
     *
     * @return Afiliado1
     */
    public function setCuota($cuota)
    {
        $this->cuota = $cuota;

        return $this;
    }

    /**
     * Get cuota
     *
     * @return float
     */
    public function getCuota()
    {
        return $this->cuota;
    }

    /**
     * Set debitos
     *
     * @param float $debitos
     *
     * @return Afiliado1
     */
    public function setDebitos($debitos)
    {
        $this->debitos = $debitos;

        return $this;
    }

    /**
     * Get debitos
     *
     * @return float
     */
    public function getDebitos()
    {
        return $this->debitos;
    }

    /**
     * Set creditos
     *
     * @param float $creditos
     *
     * @return Afiliado1
     */
    public function setCreditos($creditos)
    {
        $this->creditos = $creditos;

        return $this;
    }

    /**
     * Get creditos
     *
     * @return float
     */
    public function getCreditos()
    {
        return $this->creditos;
    }

    /**
     * Set ganancias
     *
     * @param string $ganancias
     *
     * @return Afiliado1
     */
    public function setGanancias($ganancias)
    {
        $this->ganancias = $ganancias;

        return $this;
    }

    /**
     * Get ganancias
     *
     * @return string
     */
    public function getGanancias()
    {
        return $this->ganancias;
    }

    /**
     * Set iva
     *
     * @param string $iva
     *
     * @return Afiliado1
     */
    public function setIva($iva)
    {
        $this->iva = $iva;

        return $this;
    }

    /**
     * Get iva
     *
     * @return string
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * Set dgrexcep
     *
     * @param \DateTime $dgrexcep
     *
     * @return Afiliado1
     */
    public function setDgrexcep($dgrexcep)
    {
        $this->dgrexcep = $dgrexcep;

        return $this;
    }

    /**
     * Get dgrexcep
     *
     * @return \DateTime
     */
    public function getDgrexcep()
    {
        return $this->dgrexcep;
    }

    /**
     * Set dgiexcep
     *
     * @param \DateTime $dgiexcep
     *
     * @return Afiliado1
     */
    public function setDgiexcep($dgiexcep)
    {
        $this->dgiexcep = $dgiexcep;

        return $this;
    }

    /**
     * Get dgiexcep
     *
     * @return \DateTime
     */
    public function getDgiexcep()
    {
        return $this->dgiexcep;
    }
}
