<?php

namespace CmsGa\CPCEBundle\Repository;

/**
 * Procesos1Repository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Procesos1Repository extends \Doctrine\ORM\EntityRepository {

    public function getCuentasByProceso($proceso) {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('p')
            ->from('CmsGaCPCEBundle:Procesos1', 'p')
            ->where('p.proCodigo = :proceso')
            ->setParameter('proceso', $proceso)
        ;
        return $qb
            ->getQuery()
            ->getOneOrNullResult();
        ;
    }
}