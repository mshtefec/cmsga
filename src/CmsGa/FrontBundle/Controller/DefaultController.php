<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use CmsGa\BackBundle\Entity\Contacto;
use CmsGa\BackBundle\Form\ContactoType;

class DefaultController extends Controller {

    /**
     * @Route("/", name="inicio")
     * @Template("@dirname/Default/inicio.html.twig")
     */
    public function inicioAction() {
        $mwsCV = $this->container->get('mws_count_visits');
        $mwsCV->countVisits();
        $em = $this->getDoctrine()->getManager();
        $secciones = $em->getRepository('CmsGaBackBundle:Seccion')
            ->findAllSeccionPortadaOrderByNoticias();
        $noticias = $em->getRepository('CmsGaBackBundle:Noticia')
            ->findAllNoticiasInPortada();
        $destacadas = $em->getRepository('CmsGaBackBundle:Noticia')
            ->findNoticiasDestacadas(10);
        
        return array(
            'secciones'  => $secciones,
            'noticias'   => $noticias, 
            'destacadas' => $destacadas,
        );
    }

    /**
     * @Route("/carousel", name="carousel")
     * @Template("@dirname/Default/carousel.html.twig")
     */
    public function carouselAction() {
        $em = $this->getDoctrine()->getManager();
        $carousel = $em->getRepository('CmsGaBackBundle:ImagenPortada')
            ->findPortadasOrderByFecha();

        return array(
            'carousel' => $carousel,
        );
    }

    /**
     * @Route("/menu/{slug}/{level}/{view}", name="menu_view")
     */
    public function menuViewAction($slug, $level, $view) {
        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('MWSimpleDynamicMenuBundle:Menu')
            ->findMenuAndItems($slug, $level);

        $template = '@dirname/Menu/' . $view . '.html.twig';

        return $this->render($template, array(
            'menu' => $menu,
        ));
    }

    /**
     * @Route("/menu/{slug}/{level}", name="menu")
     */
    public function menuAction($slug, $level) {
        $em = $this->getDoctrine()->getManager();
        $menu = $em->getRepository('MWSimpleDynamicMenuBundle:Menu')
            ->findMenuAndItems($slug, $level);

        $template = '@dirname/Default/' . $slug . '.html.twig';

        return $this->render($template, array(
            'menu' => $menu,
        ));
    }

    /**
     * @Route("/seccion/ver/{sec}/{view}", name="seccion_front_view")
     */
    public function seccionViewAction($sec, $view) {
        $em = $this->getDoctrine()->getManager();
        $seccion = $em->getRepository('CmsGaBackBundle:Seccion')
            ->findSeccionAndItem($sec);

        $template = '@dirname/Seccion/' . $view . '.html.twig';

        return $this->render($template, array(
            'seccion' => $seccion,
        ));
    }

    /**
     * @Route("/seccion/{sec}", name="seccion_front")
     */
    public function seccionAction($sec) {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        $seccion = $em->getRepository('CmsGaBackBundle:Seccion')
            ->findSeccionAndItem($sec);

        $page = $request->query->getInt('page', 1);
        $view = $seccion->getPlantilla()->getNombre();

        $template = '@dirname/Plantilla/' . $view . '.html.twig';

        return $this->render($template, array(
            'seccion' => $seccion,
            'page' => $page,
        ));
    }

    /**
     * @Route("/contacto", name="contacto_cmsga")
     * @Template("@dirname/Default/contacto.html.twig")
     */
    public function contactoAction(Request $request) {
        $entity = new Contacto();
        $form = $this->createForm(new ContactoType(), $entity, array(
            'action' => $this->generateUrl('contacto_cmsga'),
            'method' => 'POST',
            'attr' => array(
                'id' => 'formCorreo'
            )
        ));
        $em = $this->getDoctrine()->getManager();
        $correos = $em->getRepository('CmsGaBackBundle:Correo')->findAll();
        $form->handleRequest($request);

        $fueEnviado = false;
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()
                        ->add('success', 'El mensaje se ha enviado correctamente');
                $this->enviarMail($entity);

                $fueEnviado = true;
            } else {
                $this->get('session')->getFlashBag()
                        ->add('danger', 'Error al enviar el mensaje');
            }
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'fueEnviado' => $fueEnviado,
            'correos' => $correos,
        );
    }

    /**
     * Envio de mail.
     *
     * @param Contacto $entity Contacto
     */
    public function enviarMail($entity) {
        $message = \Swift_Message::newInstance()
                ->setSubject('Aviso de correo')
                ->setFrom($entity->getEmail())
                ->setTo($entity->getCorreo()->getCorreo())
                ->setBody(
                $this->renderView(
                        'CmsGaFrontBundle:Default:contacto.email.html.twig', array('entity' => $entity)
                ), 'text/html'
                )
        ;
        $this->get('mailer')->send($message);
    }

    /**
     * Buscar contenido en las secciones o noticias.
     * La variable $view sirve para mostrar otra vista del buscador.
     *
     * @Route("/buscador/{prefer}/{view}", name="buscador_contenido", defaults={"prefer" = null, "view" = null})
     * @Method({"GET", "POST"})
     */
    public function buscadorContenidoAction($prefer = null, $view = null) {
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST' || is_null($request->get('_route'))) {
            $findForm = $this->createFindForm($prefer);
            $findForm->handleRequest($request);

            if ($findForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $dataForm = $findForm->getData();

                // $dataForm['texto'] = htmlentities($dataForm['texto']);
                if ($dataForm['tipo'] == 'noticias') {
                    $entities = $em->getRepository('CmsGaBackBundle:Noticia')
                        ->findLikeNoticias($dataForm['texto']);
                } elseif ($dataForm['tipo'] == 'secciones') {
                    $entities = $em->getRepository('CmsGaBackBundle:Seccion')
                        ->findLikeSecciones($dataForm['texto']);
                } elseif ($dataForm['tipo'] == 'calendario') {
                    $entities = $em->getRepository('CmsGaCalendarioBundle:Calendario')
                        ->findLikeCalendario($dataForm['texto']);
                }

                if (!$entities) {
                    $this->get('session')->getFlashBag()->add('info', 'No se encontraron resultados.');

                    $content = $this->renderView(
                        '@dirname/Default/buscadorResultado.html.twig', array(
                            'resultados' => $entities
                        )
                    );
                }

                $content = $this->renderView(
                    '@dirname/Default/buscadorResultado.html.twig', array(
                        'resultados' => $entities
                    )
                );
            } else {
                if (is_null($view)) {
                    $view = 'buscadorContenido';
                }
                $content = $this->renderView(
                    '@dirname/Default/' . $view . '.html.twig', array(
                        'find_form' => $findForm->createView()
                    )
                );
            }

            return new Response($content);
        }

        return $this->redirect($this->generateUrl('inicio'));
    }

    /**
     * Create find form.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function createFindForm($prefer) {
        if (is_null($prefer)) {
            $prefer = 'noticias';
        }

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('buscador_contenido'))
            ->setMethod('POST')
            ->add('texto', 'text', array(
                'label' => false,
                'attr' => array(
                    'class' => 'form-control inputBuscar',
                    'placeholder' => 'Buscar...',
                ),
            ))
            ->add('tipo', 'choice', array(
                'label' => false,
                'choices' => array(
                    'noticias' => 'Noticias',
                    'secciones' => 'Secciones',
                    'calendario' => 'Actividades',
                ),
                //'preferred_choices' => array($prefer),
                'attr' => array(
                    'class' => 'form-control',
                ),
            ))
            ->getForm()
        ;
    }

    /**
     * @Route("/bloque/cmsga/{nombre}", name="bloque_front")
     * @Template()
     */
    public function bloqueAction($nombre) {
        $em = $this->getDoctrine()->getManager();
        $bloque = $em->getRepository('CmsGaBackBundle:Bloque')->findOneByNombre($nombre);

        return array('bloque' => $bloque);
    }

    /**
     * @Route("/lateral", name="lateral")
     * @Template("@dirname/Default/lateral.html.twig")
     */
    public function lateralAction() {
        $em = $this->getDoctrine()->getManager();
        $actuales = $em->getRepository('CmsGaCalendarioBundle:Calendario')->findActuales();
        $proximas = $em->getRepository('CmsGaCalendarioBundle:Calendario')->findProximas();

        return array(
            'actuales' => $actuales,
            'proximas' => $proximas,
        );
    }

    /**
     * @Route("/calendario", name="calendario")
     * @Template("@dirname/Default/calendario.html.twig")
     */
    public function calendarioAction() {
        return array();
    }

}
