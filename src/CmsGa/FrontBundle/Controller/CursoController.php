<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CursoController extends Controller {

    /**
     * Detalle Noticia.
     *
     * @param int $id id de noticia
     *
     * @Route("/detalle-curso/{id}", name="detalle_curso", defaults={"id" = 1})
     * @Template("@dirname/Actividad/detalle.html.twig")
     */
    public function detalleCursoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository('CmsGaCalendarioBundle:Curso')->find($id);

        return array(
            'entity' => $curso,
            'titulo' => 'Curso',
        );
    }

    /**
     * @Route("/curso-listar/", name="listar_cursos")
     * @Template("@dirname/Actividad/listado.html.twig")
     */
    public function cursolistaAction() {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('CmsGaCalendarioBundle:Curso')
            ->findAllByActivosMenorAlDiaActual();
        $request = $this->getRequest();
        $pagina = $request->query->get('page', 1);

        $query = $em->createQuery($qb->getDQL());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $pagina, //page number
            7 // limit per page
        );
        $proximos = $em->getRepository('CmsGaCalendarioBundle:Curso')
            ->findAllByActivosMayorIgualAlDiaActual();
        // parameters to template
        return array(
            'pagination' => $pagination,
            'titulo' => 'Cursos',
            'path_detalle' => 'detalle_curso',
            'proximos' => $proximos,
        );
    }

    /**
     * @Route("/curso-recent-listar/", name="listar_cursos_recientes")
     * @Template("@dirname/Actividad/recentList.html.twig")
     */
    public function recentCursosAction($max = 3) {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CmsGaCalendarioBundle:Curso')->findCusoByMax($max);

        return array(
            'entity' => $entities,
            'titulo' => 'Cursos',
            'subTituloAgenda' => 'Curso',
            'path_detalle' => 'detalle_curso',
        );
    }

    /**
     * @Route("/curso/{url}", name="detalle_cursobyurl", defaults={"url" = 0})
     * @Template("@dirname/Actividad/detalle.html.twig")
     */
    public function detalleByUrlAction($url) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CmsGaCalendarioBundle:Calendario')->findSegunUrl('curso/' . $url);

        return array(
            'entity' => current($entity),
            'titulo' => 'Cursos',
        );
    }

}
