<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CmsGa\BackBundle\Form\NoticiaFilterType;

class NoticiaController extends Controller {

    /**
     * Detalle Noticia.
     *
     * @param int $id id de noticia
     *
     * @Route("/detalle-noticia/{id}", name="detalle_noticia", defaults={"id" = 1})
     * @Template("@dirname/Noticia/detalle.html.twig")
     */
    public function detalleAction($id) {
        $em = $this->getDoctrine()->getManager();
        $noticia = $em->getRepository('CmsGaBackBundle:Noticia')->findNoticiaPublicada($id);
        
        if (!$noticia) {
            throw $this->createNotFoundException('Unable to find Noticia entity.');
        }
        return array(
            'noticia' => $noticia,
            'seccion' => $noticia->getSeccion(),
        );
    }

    /**
     * @Route("/noticia-listar/{sec}/{view}", name="listar_noticia")
     */
    public function noticiaslistaAction($sec, $view, $pagina) {
        $em = $this->getDoctrine()->getManager();

        //  return array('seccion' => $seccion);

        /* $dql = "SELECT  n FROM CmsGa\BackBundle\Entity\Noticia n"
          .' JOIN n.seccion s WITH n.publicado = true '
          ." WHERE s.url = '".$sec."' ORDER BY n.fecha DESC"; */
        $dql = $em->getRepository('CmsGaBackBundle:Noticia')->queryAllOrderByFechaDesc($sec);

        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $pagina/* page number */, 4/* limit per page */
        );

        $template = '@dirname/Noticia/' . $view . '.html.twig';

        return $this->render($template, array(
                    'pagination' => $pagination,
        ));
    }

    // /**
    //  * Noticias
    //  *
    //  * @Route("/noticias", name="noticias_front")
    //  * @Template()
    //  *
    //  * @return array vars
    //  */
    // public function noticiasAction()
    // {
    //     $title = 'Noticias';
    //     $em = $this->getDoctrine()->getManager();
    //     $queryBuilder = $em->getRepository('CmsGaBackBundle:Noticia')->queryAllOrderedByCategoriaAndFecha();
    //     list($filterForm, $queryBuilder) = $this->filter($queryBuilder);
    //     $page = $this->get('request')->query->get('page', 1);
    //     $paginator  = $this->get('knp_paginator');
    //     $pagination = $paginator->paginate(
    //         $queryBuilder,
    //         $page,
    //         6
    //     );
    //     if ($page > 1) {
    //         $session = $this->get('session');
    //         $key_control = $session->get('key_control');
    //         $session->set('key_control', $key_control + $pagination->getItemNumberPerPage());
    //         $template = 'CmsGaIpapFrontBundle:Noticia:listNoticias.html.twig';
    //     } else {
    //         $session = $this->get('session');
    //         $session->set('key_control', 0);
    //         $template = 'CmsGaIpapFrontBundle:Noticia:noticias.html.twig';
    //     }
    //     return $this->render($template, array(
    //         'title'      => $title,
    //         'noticias'   => $pagination,
    //         'filterForm' => $filterForm->createView(),
    //     ));
    // }
    // /**
    // * Process filter request.
    // *
    // * @return array
    // */
    // protected function filter($queryBuilder)
    // {
    //     $request = $this->getRequest();
    //     $session = $request->getSession();
    //     $filterForm = $this->createFilterForm();
    //     // Bind values from the request
    //     $filterForm->handleRequest($request);
    //     // Reset filter
    //     if ($filterForm->get('reset')->isClicked()) {
    //         $session->remove('NoticiaFrontControllerFilter');
    //         $filterForm = $this->createFilterForm();
    //     }
    //     // Filter action
    //     if ($filterForm->get('filter')->isClicked()) {
    //         if ($filterForm->isValid()) {
    //             // Build the query from the given form object
    //             $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
    //             // Save filter to session
    //             $filterData = $filterForm->getData();
    //             $session->set('NoticiaFrontControllerFilter', $filterData);
    //         }
    //     } else {
    //         // Get filter from session
    //         if ($session->has('NoticiaFrontControllerFilter')) {
    //             $filterData = $session->get('NoticiaFrontControllerFilter');
    //             $filterForm = $this->createFilterForm($filterData);
    //             $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
    //         }
    //     }
    //     return array($filterForm, $queryBuilder);
    // }
    // /**
    // * Create filter form.
    // *
    // * @return \Symfony\Component\Form\Form The form
    // */
    // private function createFilterForm($filterData = null)
    // {
    //     $form = $this->createForm(new NoticiaFilterType(), $filterData, array(
    //         'action' => $this->generateUrl('noticias_front'),
    //         'method' => 'GET',
    //     ));
    //     $form
    //         ->add('filter', 'submit', array(
    //             'translation_domain' => 'MWSimpleAdminCrudBundle',
    //             'label'              => 'views.index.filter',
    //             'attr'               => array('class' => 'btn btn-success'),
    //         ))
    //         ->add('reset', 'submit', array(
    //             'translation_domain' => 'MWSimpleAdminCrudBundle',
    //             'label'              => 'views.index.reset',
    //             'attr'               => array('class' => 'btn btn-danger'),
    //         ))
    //     ;
    //     return $form;
    // }
    // /**
    //  * Becas
    //  *
    //  * @Route("/becas", name="becas_front")
    //  * @Template("CmsGaIpapFrontBundle:Noticia:noticias.html.twig")
    //  *
    //  * @return array vars
    //  */
    // public function becasAction()
    // {
    //     $title = 'Becas';
    //     $em = $this->getDoctrine()->getManager();
    //     $queryBuilder = $em->getRepository('CmsGaBackBundle:Noticia')->queryAllBecasOrderedByFecha();
    //     list($filterForm, $queryBuilder) = $this->filter($queryBuilder);
    //     $page = $this->get('request')->query->get('page', 1);
    //     $paginator  = $this->get('knp_paginator');
    //     $pagination = $paginator->paginate(
    //         $queryBuilder,
    //         $page,
    //         6
    //     );
    //     if ($page > 1) {
    //         $session = $this->get('session');
    //         $key_control = $session->get('key_control');
    //         $session->set('key_control', $key_control + $pagination->getItemNumberPerPage());
    //         $template = 'CmsGaIpapFrontBundle:Noticia:listNoticias.html.twig';
    //     } else {
    //         $session = $this->get('session');
    //         $session->set('key_control', 0);
    //         $template = 'CmsGaIpapFrontBundle:Noticia:noticias.html.twig';
    //     }
    //     return $this->render($template, array(
    //         'title'      => $title,
    //         'noticias'   => $pagination,
    //         'filterForm' => $filterForm->createView(),
    //     ));
    // }
}
