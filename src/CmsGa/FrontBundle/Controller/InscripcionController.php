<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use CmsGa\CalendarioBundle\Entity\Calendario;

use CmsGa\BackBundle\Entity\Documento;
use CmsGa\BackBundle\Form\DocumentoType;

use CmsGa\CalendarioBundle\Entity\Inscripcion;
use CmsGa\FrontBundle\Form\InscripcionType;
//use CmsGa\CalendarioBundle\Form\InscripcionType;

/**
 * Inscripcion controller.
 * @author Shtefec Max <max.shtefec@gmail.com>
 *
 */
class InscripcionController extends Controller {

    /**
     * Ingreso Inscripcion Curso.
     *
     * @param int $id id de curso
     *
     * @Route("/inscripcion/curso/{id}", name="inscripcion_curso", defaults={"id" = 0})
     * @Template("@dirname/Inscripcion/inscripcionCurso.html.twig")
     */
    public function inscripcionCursoAction($id, Request $request) {
        $formInscripcionView    = null;
        $noDebe                 = 0;
        
        $em = $this->getDoctrine()->getManager();
        $cpceEm = $this->get('doctrine')->getManager('cpce');

        $calendario = $em->getRepository('CmsGaCalendarioBundle:Calendario')->find($id);

        if ($calendario && $calendario->getPermiteInscripcion()) {
            
            $entityDocumento    = new Documento();
            $entityInscripcion  = new Inscripcion();

            $entityInscripcion->setCalendario($calendario);

            $formDocumento      = $this->createForm(new DocumentoType(), $entityDocumento);
            $formInscripcion    = $this->createForm(new InscripcionType(), $entityInscripcion);

            $formDocumentoView = $formDocumento->createView();

            $formDocumento->handleRequest($request);
            $formInscripcion->handleRequest($request);

            if (!$calendario->getSinControl()) {
                $formDocumentoView   = null;
                $formInscripcionView = $formInscripcion->createView();
            } else {
                if ($formDocumento->isSubmitted()) { 

                    if ($formDocumento->isValid()) {

                        $afiliado = $cpceEm->getRepository('CmsGaCPCEBundle:Afiliado')->findAfiliadoByDocFilter($entityDocumento->getDni());
                        
                        if ($afiliado) {

                            // Busco cuenta activa para ver la categoria a la que pertenece.
                            $afiCategoria = $afiliado->getAfiCategoria();
                            $afiCategoria = $afiCategoria ? substr($afiCategoria, 0, 2) : "-";
                            $habilitado = true;
                            // Controlo si pertenece a alguna cuenta que se bloqueo.
                            foreach ($calendario->getCategorias() as $key => $categoria) {
                                $habilitado = $afiCategoria == $categoria->getControlCategoria()->getCodigo() ? false : true;
                            }
                            if (!$habilitado) {
                                $this->get('session')->getFlashBag()->add('danger', 'Su condición de matricula esta en revisión, consulte con la Delegación.');
                            } else {
                                
                                $nodebe = 0;

                                foreach ($calendario->getCuentas() as $key => $cuenta) {
                                    $saldo = $cpceEm->getRepository('CmsGaCPCEBundle:Totales')->findSaldoTotalByAfiliadoCuenta($afiliado->getAfiTipdoc(), $afiliado->getAfiNrodoc(), $cuenta->getControlCuenta()->getCodigo());
                                    $saldo = is_null($saldo['saldo']) ? 0 : $saldo['saldo'];
                                    $saldo = $saldo <= $cuenta->getImporte() ? 0 : $saldo;
                                    $nodebe = $nodebe + $saldo;
                                }
                                
                                if ($nodebe <= 0) {

                                    $options['attr']['readonly'] = true;
                                    //$options =  $formInscripcion->get('nrodoc')->getConfig()->getOptions();

                                    $formInscripcion->add('nombre', null, $options);
                                    $formInscripcion->get('nombre')->setData($afiliado->getAfiNombre());

                                    $formInscripcion->get('tipdoc')->setData($afiliado->getAfiTipdoc());

                                    $formInscripcion->add('nrodoc', null, $options);
                                    $formInscripcion->get('nrodoc')->setData($afiliado->getAfiNrodoc());

                                    $formInscripcion->get('titulo')->setData($afiliado->getAfiTitulo());

                                    $formInscripcion->add('matricula', null, $options);
                                    $formInscripcion->get('matricula')->setData($afiliado->getAfiMatricula());

                                    $formInscripcion->get('correo')->setData($afiliado->getAfiMail());

                                    $formDocumentoView   = null;
                                    $formInscripcionView = $formInscripcion->createView();
                                } else {
                                    $this->get('session')->getFlashBag()->add('danger', 'Puede no estar al día con su cuenta personal, consulte con la Delegación.');
                                }

                            }

                        } else {
                            $this->get('session')->getFlashBag()->add('danger', 'Puede existir un error en sus datos, intente de nuevo.');
                        }

                    } else {
                        $this->get('session')->getFlashBag()->add('danger', 'No se puede validar el Número de Documento, intente de nuevo.');
                    }

                }
            }

            if ($formInscripcion->isSubmitted()) { 

                if ($formInscripcion->isValid()) {

                    $em->persist($entityInscripcion);

                    $inscripto = $em->getRepository('CmsGaCalendarioBundle:Inscripcion')->findByIdAndDni($calendario->getId(), $entityInscripcion->getNrodoc());

                    if (count($inscripto) > 0) {
                        $this->get('session')->getFlashBag()->add('danger', 'Usted ya se ha pre-inscripto a este curso con ese número de documento.');
                    } else {

                        // otra vez controlo si existe matricula
                        if (!$calendario->getSinControl()) {
                            $em->flush();                            

                            $this->get('session')->getFlashBag()->add('success', 'La pre-inscripción se realizo correctamente!');
                            $this->enviarCorreoAction($entityInscripcion, $id);
                        } else {

                            $afiliado = $cpceEm->getRepository('CmsGaCPCEBundle:Afiliado')->findAfiliadoByDocFilter($entityInscripcion->getNrodoc());
                            
                            if ($afiliado) {
                                
                                // Si es "empleado" no hago control de saldos.
                                if ($afiliado->getAfiTipo() == 'E') {
                                    $em->flush();

                                    $this->get('session')->getFlashBag()->add('success', 'La pre-inscripción se realizo correctamente!');    
                                    $this->enviarCorreoAction($entityInscripcion, $id);
                                } else {
                                    // Busco cuenta activa para ver la categoria a la que pertenece.
                                    $afiCategoria = $afiliado->getAfiCategoria();
                                    $afiCategoria = $afiCategoria ? substr($afiCategoria, 0, 2) : "-";
                                    $habilitado = true;
                                    // Controlo si pertenece a alguna cuenta que se bloqueo.
                                    foreach ($calendario->getCategorias() as $key => $categoria) {
                                        $habilitado = $afiCategoria == $categoria->getControlCategoria()->getCodigo() ? false : true;
                                    }
                                    if (!$habilitado) {
                                        $this->get('session')->getFlashBag()->add('danger', 'Su condición de matricula esta en revisión, consulte con la Delegación.');
                                    } else {
                                        $nodebe = 0;
                                        
                                        // Vuelvo a controlar saldos
                                        foreach ($calendario->getCuentas() as $key => $cuenta) {
                                            $saldo = $cpceEm->getRepository('CmsGaCPCEBundle:Totales')->findSaldoTotalByAfiliadoCuenta($afiliado->getAfiTipdoc(), $afiliado->getAfiNrodoc(), $cuenta->getControlCuenta()->getCodigo());
                                            $saldo = is_null($saldo['saldo']) ? 0 : $saldo['saldo'];
                                            $saldo = $saldo <= $cuenta->getImporte() ? 0 : $saldo;
                                            $nodebe = $nodebe + $saldo;
                                        }

                                        if ($nodebe <= 0) {

                                            if ($afiliado->getAfiTitulo() == 'ET') {
                                                $fecha_vencimiento = $afiliado->getAfiFechavencimientoet();
                                                $now = new \DateTime('now');
                                                if ($fecha_vencimiento >= $now) {
                                                    $this->get('session')->getFlashBag()->add('danger', 'Usted no cumple con los requisitos de inscripción al curso, consulte con la Delegación.');
                                                } else {
                                                    $em->flush();

                                                    $this->get('session')->getFlashBag()->add('success', 'La pre-inscripción se realizo correctamente!');    
                                                    $this->enviarCorreoAction($entityInscripcion, $id);    
                                                }
                                            } else {
                                                $em->flush();

                                                $this->get('session')->getFlashBag()->add('success', 'La pre-inscripción se realizo correctamente!');    
                                                $this->enviarCorreoAction($entityInscripcion, $id);
                                            }
                                        } else {
                                            $this->get('session')->getFlashBag()->add('danger', 'Puede no estar al día en su cuenta personal con los requisitos del curso, consulte con la Delegación.');
                                        }
                                    }
                                }

                            } else {
                                $this->get('session')->getFlashBag()->add('danger', 'No se puede realizar esta pre-inscripción, puede haber un problema en sus datos.');            
                            }
                            
                        }

                        
                    }

                    return $this->redirect($this->generateUrl('calendario'));
                } else {
                    $this->get('session')->getFlashBag()->add('danger', 'No se puede realizar esta pre-inscripción, intente de nuevo.');
                }
                

            }

            return array(
                'curso'             => $calendario,
                'titulo'            => 'Pre Inscripcion',
                'formDocumento'     => $formDocumentoView,
                'formInscripcion'   => $formInscripcionView,
            );


        } else {
            $this->get('session')->getFlashBag()->add('danger', 'No se puede encontrar ese curso o no permite pre-inscripcion.');

            return $this->redirect($this->generateUrl('calendario'));
        }
    }

    public function enviarCorreoAction($entityInscripcion, $id) {

        $from = "noresponder@cpcechaco.org.ar";
        $to = $entityInscripcion->getCorreo();
        $nombre = $entityInscripcion->getNombre();
        
        $message = \Swift_Message::newInstance()
            ->setSubject('CPCE Pre inscripción a Curso')
            ->setFrom($from)
            ->setTo($to)
            ->setBody(
                $this->renderView(
                        'CmsGaFrontBundle:cpceweb:Inscripcion/mail.html.twig',
                        array(
                            'id' => $id,
                            'nombre' => $nombre
                        )
                ), 'text/html'
            )
        ;
        $this->get('mailer')->send($message);
           
    }
}