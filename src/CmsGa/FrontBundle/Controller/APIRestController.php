<?php

namespace CmsGa\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class APIRestController extends Controller {

    /**
     * Detalle JSON.
     *
     * @param int $id id de curso
     *
     * @Route("/apirest/agenda/{id}", name="detalle_agenda_json")
     */
    public function detalleCursoJSONAction($id) {

        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository('CmsGaCalendarioBundle:Curso')->find($id);
        
        $fecha = $curso->getStartDatetime();
        if ($curso->getDisertante()) {
            $concepto1 = $curso->getTitle() . " _Disertante: " . $curso->getDisertante() . " _Fecha: " . $fecha->format('d-m-Y');    
        } else {
            $concepto1 = $curso->getTitle() . " _Fecha: " . $fecha->format('d-m-Y');
        }
        $concepto2 = "IMPORTE: ";
        $concepto3 = "-";

        $arrayJson = array(
            'id' => $curso->getId(),
            'concepto_1' => $concepto1,
            'concepto_2' => $concepto2,
            'concepto_3' => $concepto3,
        );

        return new JsonResponse($arrayJson);
    }

}
