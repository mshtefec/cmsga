/**
 *  Borra la imgen form 
 *  
 *  @param Button $deleteButton DOM Borrar    
 * 
 *  @return void
 */
function deleteRow($deleteButton) { 
    var $divContent = jQuery($deleteButton).parent().parent();                                    
    $divContent.remove();      
}
jQuery('.selectEsEmpleado').on('change', function(){
    var isEmpleado = jQuery(this).val();
    if (isEmpleado == '1') {
        var content = jQuery('.contentEsEmpleado')
                        .data('prototype');
        jQuery('.contentEsEmpleado').html(content);
        jQuery('.areaSelect2').parent().find('.select2-container').select2('enable', false);
    } else {
        jQuery('.contentEsEmpleado').empty();
    }
})
var isEmpleado = jQuery('.selectEsEmpleado').val();
if (isEmpleado == '1') {
    var content = jQuery('.contentEsEmpleado')
                    .data('prototype');
    jQuery('.contentEsEmpleado').html(content);
}
jQuery('.inputUpper').keyup(function(){
    this.value = this.value.toUpperCase();
});
var option = {
    success: updateSelect2
};
jQuery('.modal-body form').ajaxForm(option);
function updateSelect2(responseText, statusText, xhr, $form)  { 
    jQuery('.areaSelect2').parent().find('.select2-container').select2('data', responseText);
    jQuery('#area-form').modal('hide');
    jQuery('#agregar-area').hide();    
}
