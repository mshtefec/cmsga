 /*
 * Validate
 *        
 */       
configValid = {                
    errorClass: "label label-danger",
    highlight: function(element, errorClass) { 
        console.log(element.id);
        jQuery(element).parent()
            .addClass('has-error');
        tabNotificacion()
          },
    success: function(element) {
        element
            .parent()
            .removeClass('has-error')
            .addClass('has-success');
        element
            .parent()
            .find('label').last().remove();
        tabNotificacion();
    },      
    ignore: ".ignore, .select2-offscreen, .select2-input"                             
    };
$form = jQuery('form');
$form.validate(configValid);
