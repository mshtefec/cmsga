//domicilios
collectionDomiciliosHolder = jQuery('.domicilios');
collectionDomiciliosHolder.data('index', collectionDomiciliosHolder.find(':input').length);
var c = 0;
jQuery('.domicilios').children().each(function(i){
    console.log(this);
    //jQuery(this)
    bindPais();
    bindProvincia();
    bindEsDomicilioSelect();
    decrementCount();
    c++;
})
/* No se podrán agregar más de 3 formularios de domicilio */
var countForm = 3 - c;
jQuery('.input-append').find('.add-domicilio-form').click(function(e) {
    e.preventDefault();
    console.log($(this).attr('class'));
    addForm(jQuery(this).parent().parent().find('.domicilios'), jQuery(this).parent().parent().find('.domicilios'));
    bindPais();
    bindProvincia();
    bindEsDomicilioSelect();
    decrementCount();
    $last = jQuery('.domicilios').children().last();
    selectTipoDomicilipo($last);
})
jQuery('.domicilios').delegate('.delete-form','click', function(e) {
    e.preventDefault();
    $domicilioForm = jQuery(this).parent().parent().parent();
    removeSelect($domicilioForm);
    deleteRow(jQuery(this).parent()); 
    incrementCount();
});
/**
 * Bind País la dunción change para el ajax de provincias
 *
 */
function bindPais() {
    jQuery('.paisContent').on('change', function(e){
        var id = jQuery(this).val();
        var url= Routing.generate('ajax_front_provincia', { 'id': id});
        var type= 'POST';
        var $classSelect = jQuery(this).parent().parent().parent().parent().find('.provinciaContent');
        /* Está función se encuentra en ajaxElementOnSelected.js */
        ajaxElementOnSelected(url, type, $classSelect, id);
    })
}
/**
 * Bind Provincia la función change para el ajax de localidades
 *
 */
function bindProvincia() {
    jQuery('.provinciaContent').on('change', function(e){
        var id = jQuery(this).val();
        var url= Routing.generate('ajax_front_localidad', { 'id': id});
        var type= 'POST';
        var $classSelect = jQuery(this).parent().parent().parent().parent().find('.localidadContent');
        /* Está función se encuentra en ajaxElementOnSelected.js */
        ajaxElementOnSelected(url, type, $classSelect, id);
    })
}
function decrementCount() {
    countForm--;
    if(countForm < 1) {
        jQuery('.add-domicilio-form').hide();
        jQuery('.tipoChoicerSelect').hide();
    }
}
function incrementCount() {
    if(countForm == 0) {
        jQuery('.add-domicilio-form').show();
        jQuery('.tipoChoicerSelect').show();
    }
    countForm++;
}
/**
 * Cada formulario solo pude ser de un tipo
 *
 * @param DOM $last formulario domicilio
 *
 * @return void
 */
function selectTipoDomicilipo($last) {
    var $selected = jQuery('.tipoChoicerSelect :selected');
    var tipoVal = $selected.val();
    $last.find('.tipoSelect').val(tipoVal);
    $selected.remove();
}
/**
 * Cada formulario solo pude ser de un tipo
 *
 * @param DOM $domicilioForm formulario domicilio a eliminar
 *
 * @return void
 */
function removeSelect($domicilioForm){
    var tipoVal = $domicilioForm.find('.tipoSelect :selected').val();
    var tipoText = $domicilioForm.find('.tipoSelect :selected').text();
    jQuery('.tipoChoicerSelect').append(
        jQuery('<option>', {
            value: tipoVal,        
            text: tipoText
        })
   );
}
function bindEsDomicilioSelect() {
    jQuery('.esEdificioSelect').on('change', function(e){
       var tipo = jQuery(this).val();
       if (tipo == 'edificio') {
           jQuery('.edificio').show(); 
       } else {
           jQuery('.edificio').hide(); 
       }
    })
}
