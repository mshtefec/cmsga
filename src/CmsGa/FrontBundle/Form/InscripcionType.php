<?php

namespace CmsGa\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * InscripcionType form.
 * @author Nombre Apellido <name@gmail.com>
 */
class InscripcionType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sinControl = $builder->getData()->getCalendario()->getSinControl();

        if ($sinControl) {
            $builder
                ->add('titulo', 'choice', array(
                    'label' => 'Tipo Matricula',
                    'choices'  => array(
                        'CP' => 'CP',
                        'LA' => 'LA',
                        'LE' => 'LE',
                        'PE' => 'PE',
                        '-' => 'S/M',
                    ),
                    'attr' => array(
                        'col' => 'col-lg-3 col-md-3 col-sm-12',
                    )
                ))
            ;
        } else {
            $builder
                ->add('titulo', 'choice', array(
                    'label' => 'Tipo Matricula',
                    'choices'  => array(
                        'CP' => 'CP',
                        'LA' => 'LA',
                        'LE' => 'LE',
                        'PE' => 'PE',
                    ),
                    'attr' => array(
                        'col' => 'col-lg-3 col-md-3 col-sm-12',
                    )
                ))
            ;
        }

        $builder
            ->add('tipdoc', 'choice', array(
                'label' => 'Tipo Documento',
                'choices' => array(
                    1 => 'DNI',
                    2 => 'LC',
                    3 => 'LE',
                ),
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('nrodoc', null, array(
                'label' => 'Nro Documento',
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('nombre', null, array(
                'label' => 'Apellido y Nombre',
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                )
            ))
            ->add('matricula', null, array(
                'label' => 'Nro Matricula',
                'attr' => array(
                    'col' => 'col-lg-3 col-md-3 col-sm-12',
                )
            ))
            ->add('correo', null, array(
                'attr' => array(
                    'col' => 'col-lg-6 col-md-6 col-sm-12',
                )
            ))
            ->add('calendario', 'select2', array(
                'class' => 'CmsGa\CalendarioBundle\Entity\Calendario',
                'url'   => 'Inscripcion_autocomplete_calendario',
                'configs' => array(
                    'multiple' => false, //es requerido true o false
                    'width' => '100%'
                ),
                'label_attr' => array(
                    'col' => "col-lg-12 col-md-12 col-sm-12",
                    'class' => "col-lg-12 col-md-12 col-sm-12",
                ),
                'attr' => array(
                    'col' => "col-lg-12 col-md-12 col-sm-12",
                    'class' => "col-lg-12 col-md-12 col-sm-12",
                )
            ))
            ->add('estado', 'choice', array(
                'label' => 'Estado',
                'choices' => array(
                    'En Proceso' => 'En Proceso',
                    'Inscripto' => 'Inscripto',
                    'Abonado' => 'Abonado',
                ),
                'attr' => array(
                    'col' => 'col-lg-12 col-md-12 col-sm-12',
                )
            ))
        ;

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CmsGa\CalendarioBundle\Entity\Inscripcion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cmsga_frontbundle_inscripcion';
    }
}

