<?php

namespace MWSimple\Bundle\CountVisitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/mws/countvisits")
     * @Template()
     */
    public function indexAction()
    {
        $mwsCV = $this->container->get('mws_count_visits');
        $countvisits = $mwsCV->getCountVisits();

        return array('countvisits' => $countvisits);
    }
}
