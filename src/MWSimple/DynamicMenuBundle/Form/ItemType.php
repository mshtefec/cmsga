<?php

namespace MWSimple\DynamicMenuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ItemType form.
 *
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class ItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name')
                ->add('title')
                ->add('level')
                ->add('orden')
                ->add('menu')
                ->add('parent')
                ->add('seccion')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MWSimple\DynamicMenuBundle\Entity\Item',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mwsimple_dynamicmenubundle_item';
    }
}
