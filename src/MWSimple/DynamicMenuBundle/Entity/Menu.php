<?php

namespace MWSimple\DynamicMenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menu.
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity(repositoryClass="MWSimple\DynamicMenuBundle\Entity\MenuRepository")
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_slug", type="string", length=255)
     */
    private $nameSlug;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="menu", cascade={"persist"}, orphanRemoval=true)
     */
    private $items;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameSlug.
     *
     * @param string $nameSlug
     *
     * @return Menu
     */
    public function setNameSlug($nameSlug)
    {
        $this->nameSlug = $nameSlug;

        return $this;
    }

    /**
     * Get nameSlug.
     *
     * @return string
     */
    public function getNameSlug()
    {
        return $this->nameSlug;
    }

    /**
     * Add items.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Item $items
     *
     * @return Menu
     */
    public function addItem(\MWSimple\DynamicMenuBundle\Entity\Item $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items.
     *
     * @param \MWSimple\DynamicMenuBundle\Entity\Item $items
     */
    public function removeItem(\MWSimple\DynamicMenuBundle\Entity\Item $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }
}
